$(function () {
	var _token  = '{{csrf_token()}}';	//Utilizado no ajax
	var product_type_id  = null;	
    var product_id = null;
    var duplicate = false;
	
	//Inicializa Data Table
	$("#tbl", '#div-main').DataTable({
		"aaSorting": [],	
		"columnDefs": [
			{ "orderable": false, "targets": [8] }
		],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ]
	});

	var html = ''+
	'<div class="row">'+
		'<div id="div-tbl-btn" class="col-md-6">'+
		'</div>'+
		'<div id="div-tbl-filter" class="col-md-6">'+
		'</div>'+
	'</div>';
			
	$('#tbl_wrapper').prepend(html);

	$('.dt-buttons').appendTo('#div-tbl-btn');
	$('#tbl_filter').appendTo('#div-tbl-filter');
	$('.dt-button').addClass('btn btn-default');
	
	//Set da linguagem do select2
	$.fn.select2.defaults.set('language', 'pt-BR');

   /***
        AJAX Requests
    ***/
	function changeGroup(evt) {
		evt.preventDefault();
		
		if(this.id == 'product_type_group_new') { 
			$product_type =  $('#product_type_new');
		} else {
			$product_type =  $('#product_type_edit');
		} 

		$product_type.find('option').remove();
		$product_type.append($('<option>', {value: '', text: 'Selecione'}));
		$product_type.prop('disabled', true);
		$product_type.val('').trigger('change');

		if(this.value) {
			var jqxhr = $.ajax({
				url: '/tabacaria/dashboard/product/filter',
				type: 'GET',
				dataType: 'JSON',
				data: { id : this.value },
				beforeSend: function(request) {
					return request.setRequestHeader('X-CSRF-Token',  _token);
				}
			});
			
			jqxhr.done(function(data) {
				if(data && data.success && data.product_types) {
					var item = '';
					for (var i = 0; i < data.product_types.length; i++) {
						item += '<option value="'+data.product_types[i].id+'">'+data.product_types[i].name+'</option>';
					}	
					$product_type.append(item);
					$product_type.prop('disabled', false);

					//Caso tenha sido buscado por CEP a variavel ja vai estar preenchida para buscar a cidade
					if(product_type_id) {
						$product_type.val(product_type_id).trigger('change');
						product_type_id = null;
					}
				} else if(data.error){
					console.log(data);
				}
			}); 
		}
	
	}

    function newModal(evt) {
        evt.preventDefault();
        $div_modal = $('#div-modal');
        
        jqxhr = $.ajax({
            url: '/tabacaria/dashboard/product/create',
            type: 'GET',
            dataType: 'JSON',
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRF-Token',  _token);
            }
        });
       
        jqxhr.done(function(data) {
            if(data){
                if(data.success) {
			        $div_modal.html('').html(data.html);
			        $modal = $div_modal.find('#modal-new-product');
				
					//Inicia os plugins nos componentes
					$modal.find('.select2').select2();
					// MASK
					$modal.find('.money').mask("#.##0,00", {reverse: true});

					//Inicializa wysiHTML5	
					$modal.find("textarea").wysihtml5({locale: "pt-BR"});

			        $modal.find("#form").trigger('reset');
			        $modal.modal('show');

                } else if(data.error) {
                	console.log(data.error);
                }
            }
        });
    }

    function showNewProduct(evt) {
		$modal = $(evt.currentTarget);
		
		$modal.find('#filters').select2({
			placeholder: 'Pesquise um Filtro',
            width: '100%',
            minimumResultsForSearch: 'Infinity',
            tags: true,
            multiple: true,
			data: filters,
			language: 'pt-BR'
		}).val('').trigger('change');	
    }

    function save(evt) {
        evt.preventDefault();
        
        $(evt.currentTarget).attr('disabled', true);

        var $modal = $('#modal-new-product');
        var jqxhr;

        jqxhr = $.ajax({
            url: '/tabacaria/dashboard/product',
            type: 'POST',
            data: $( this ).serialize(),
            dataType: 'JSON',
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRF-Token',  _token);
            }
        });

        jqxhr.done(function(data) {
            $(evt.currentTarget).attr('disabled', false);
			$modal.find('#div-error').hide();
			$modal.find('em').html('');
           
            if(data){
                if(data.success) {
                    window.location.reload();
                    $('#modal').modal('hide');
                } else if(data.errors) {
					$modal.find('#div-error').show();
					$modal.find('em').html(data.error);	            
                }
            }
        });
    }


    function editModal(evt) {
        evt.preventDefault();
        $div_modal = $('$div-modal');
        product_id = $(this).closest('.item').find('.id').val();

        if(product_id) {

	        jqxhr = $.ajax({
	            url: '/tabacaria/dashboard/product/update/'+product_id,
	            type: 'GET',
	            dataType: 'JSON',
	            beforeSend: function(request) {
	                return request.setRequestHeader('X-CSRF-Token',  _token);
	            }
	        });
	       
	        jqxhr.done(function(data) {
	            if(data){
	                if(data.success) {
				        $div_modal.html('').html(data.html);
				        $modal = $div_modal.find('#modal-new-product');


						//Inicia os plugins nos componentes
						$modal.find('.select2').select2();
						
						//Inicializa wysiHTML5	
						$modal.find("textarea").wysihtml5({locale: "pt-BR"});

						// MASK
						$modal.find('.money').mask("#.##0,00", {reverse: true}); 
		
				        $modal.find("#form").trigger('reset');
				        $modal.modal('show');

	                } else if(data.error) {
	                	console.log(data.error);
	                }
	            }
	        });
        }
    }

    function showEditModal(evt) {
		$modal = $(evt.currentTarget);
						
		$modal.find('#filters').select2({
            width: '100%',
            minimumResultsForSearch: 'Infinity',
            tags: true,
            multiple: true,
			data: filters
		});

		if(data.products.product_filters) {
			var aux;
			var $filters = $modal_show.find('#filters-edit');
			for (var i = 0; i < data.products.product_filters.length; i++) {
				aux = data.products.product_filters[i].filter_group.name + ' - ' + data.products.product_filters[i].name;
				$filters.find('option').each(function(i, val) {
					if(this.text == aux) {
						$(this).prop('selected', true);
					}
				});
			}
			$filters.trigger('change').prop('disabled', true);
		}
    }


    function update(evt) {
        evt.preventDefault();
        $div_modal = $('$div-modal');
        product_id = $(this).closest('.item').find('.id').val();

        if(product_id) {
	        var $modal = $('#modal-edit-product');
	        var jqxhr;

	        jqxhr = $.ajax({
	            url: '/tabacaria/dashboard/product/update/'+ product_id,
	            type: 'PUT',
	            data: $( this ).serialize(),
	            dataType: 'JSON',
	            beforeSend: function(request) {
	                return request.setRequestHeader('X-CSRF-Token',  _token);
	            }
	        });

	        jqxhr.done(function(data) {
				$modal.find('#div-error').hide();
				$modal.find('em').html('');
	           
	            if(data){
	                if(data.success) {
	                    window.location.reload();
	                    $modal.modal('hide');
	                } else if(data.errors) {
						$modal.find('#div-error').show();
						$modal.find('em').html(data.error);	            
	                }
	            }
	        });

        }

    }

    function remove(evt) {
        evt.preventDefault();
        var id = $(this).closest('.item').find('.id').val();

		bootbox.confirm({
		    title: "Remover Produto?",
		    message: "Você realmente deseja remover esse Produto?",
		    buttons: {
		        cancel: {
		            label: '<i class="fa fa-times"></i> Cancelar'
		        },
		        confirm: {
		            label: '<i class="fa fa-check"></i> Confirmar'
		        }
		    },
		    callback: function (result) {
	        	
	        	if (!result || (!result.success && result.error)) {
            		revertFunc();

	        	} else {

		            evt.preventDefault();

		            var jqxhr = $.ajax({
		                    url: '/tabacaria/dashboard/product_type/' + id,
		                    type: 'DELETE',
		                    dataType: 'JSON',
		                    beforeSend: function(request) {
		                        return request.setRequestHeader('X-CSRF-Token',  _token);
		                    }
		                });
		            
		            jqxhr.done(function(data) {
		                if(data && data.success) {
		                    window.location.reload();
		                } else {
		                    console.log(data);
		                }
		            });
	        	}
		    }
		});
    }

    function addAlert(evt) {
        evt.preventDefault();
        var id = $(this).closest('.item').find('.id').val();
       
        if(id) {
			bootbox.confirm({
			    title: "Adicionar a Lista de Alerta?",
			    message: "Quando esse produto voltar em Estoque voce será notificado. Clique em Confirmar",
			    buttons: {
			        cancel: {
			            label: '<i class="fa fa-times"></i> Cancelar'
			        },
			        confirm: {
			            label: '<i class="fa fa-check"></i> Confirmar'
			        }
			    },
			    callback: function (result) {
		        	
		        	if (!result || (!result.success && result.error)) {
	            		revertFunc();

		        	} else {

			            evt.preventDefault();
				        if(id !== null) {
				            jqxhr = $.ajax({
				                url: '/tabacaria/dashboard/alert_user' + id,
				                type: 'post',
				                data: { product_id: id },
				                dataType: 'JSON',
				                beforeSend: function(request) {
				                    return request.setRequestHeader('X-CSRF-Token',  _token);
				                }
				            });            
				        }
			            
			            jqxhr.done(function(data) {
			                if(data && !data.success) {
			                    console.log(data);
			                    revertFunc();
			                } 
			            });
		        	}
			    }
			});
        }
    }

    function addWish(evt) {
        evt.preventDefault();
        var id = $(this).closest('.item').find('.id').val();
       
        if(id) {
			bootbox.confirm({
			    title: "Adicionar a Lista de Desejos?",
			    message: "Para não esquecer esse produto, clique em Confirmar",
			    buttons: {
			        cancel: {
			            label: '<i class="fa fa-times"></i> Cancelar'
			        },
			        confirm: {
			            label: '<i class="fa fa-check"></i> Confirmar'
			        }
			    },
			    callback: function (result) {
		        	
		        	if (!result || (!result.success && result.error)) {
	            		revertFunc();

		        	} else {

			            evt.preventDefault();
				        if(id !== null) {
				            jqxhr = $.ajax({
				                url: '/tabacaria/dashboard/wish_user' + id,
				                type: 'post',
				                data: { product_id: id },
				                dataType: 'JSON',
				                beforeSend: function(request) {
				                    return request.setRequestHeader('X-CSRF-Token',  _token);
				                }
				            });            
				        }
			            
			            jqxhr.done(function(data) {
			                if(data && !data.success) {
			                    console.log(data);
			                    revertFunc();
			                } 
			            });
		        	}
			    }
			});
        }
    }

    function showStock(evt) {
        evt.preventDefault();
        $div_modal = $('$div-modal');
        
        jqxhr = $.ajax({
            url: '/tabacaria/dashboard/product/stock',
            type: 'GET',
            dataType: 'JSON',
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRF-Token',  _token);
            }
        });
       
        jqxhr.done(function(data) {
            if(data){
                if(data.success) {
			        $div_modal.html('').html(data.html);
			        $modal = $div_modal.find('#modal-new-product');
				
					//Inicia os plugins nos componentes
					$modal.find('.select2').select2();

					// MASK
					$modal.find('.money').mask("#.##0,00", {reverse: true}); 
			        
			        $modal.find("#form").trigger('reset');
			        $modal.modal('show');

                } else if(data.error) {
                	console.log(data.error);
                }
            }
        });
    }

    function saveStock(evt) {
        evt.preventDefault();
        $(evt.currentTarget).attr('disabled', true);

        var $modal = $('#modal-new-product');
        var jqxhr;

        jqxhr = $.ajax({
            url: '/tabacaria/dashboard/product/stock',
            type: 'POST',
            data: $( this ).serialize(),
            dataType: 'JSON',
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRF-Token',  _token);
            }
        });

        jqxhr.done(function(data) {
            $(evt.currentTarget).attr('disabled', false);
			$modal.find('#div-error').hide();
			$modal.find('em').html('');
           
            if(data){
                if(data.success) {
                    $('#modal').modal('hide');
                    window.location.reload();
                } else if(data.errors) {
					$modal.find('#div-error').show();
					$modal.find('em').html(data.error);	            
                }
            }
        });
    }
    /***
        Events
    ***/
    $(document).delegate('.product_type_group','change', changeGroup);

    $(document).delegate('#btn-new-modal','click', newModal);
	$(document).delegate('#modal-new-product','shown.bs.modal', showNewProduct);
    $(document).delegate('#form','submit', save);
   
   	$(document).delegate('.btn-edit','click', editModal);
	$(document).delegate('#modal-edit-product','shown.bs.modal', showEditModal);
    $(document).delegate('#form-edit','submit', update);
    
    $(document).delegate('.btn-stock','click', showStock);
    $(document).delegate('#form-stock','submit', saveStock);
   
    $(document).delegate('.btn-delete','click', remove);

    $(document).delegate('.btn-alert','click', addAlert);
    $(document).delegate('.btn-wish','click', addWish);
	
});