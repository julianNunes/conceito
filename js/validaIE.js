	//Acre
	function checkIEAC(ie){
		if (ie.length != 13){return false;}
		else{
			if(ie.substr(0, 2) != '01'){return false;}
			else{
				var b = 4;
				var soma = 0;
				for (i=0;i<=10;i++){
					soma += ie[i] * b;
					b--;
					if(b == 1){b = 9;}
				}
				var dig = 11 - (soma % 11);
				if(dig >= 10){dig = 0;}
				if( !(dig == ie[11]) ){return false;}
				else{
					b = 5;
					soma = 0;
					for(i=0;i<=11;i++){
						soma += ie[i] * b;
						b--;
						if(b == 1){b = 9;}
					}
					dig = 11 - (soma % 11);
					if(dig >= 10){dig = 0;}

					return (dig == ie[12]);
				}
			}
		}
	}

	// Alagoas
	function checkIEAL(ie){
		if (ie.length != 9){return false;}
		else{
			if(ie.substr(0, 2) != '24'){return false;}
			else{
				var b = 9;
				var soma = 0;
				for(var i=0;i<=7;i++){
					soma += ie[i] * b;
					b--;
				}
				soma *= 10;
				var dig = soma - (parseInt(soma / 11)  * 11 );
				if(dig == 10){dig = 0;}

				return (dig == ie[8]);
			}
		}
	}

	//Amazonas
	function checkIEAM(ie){
		if (ie.length != 9){return false;}
		else{
			b = 9;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}
			if(soma <= 11){dig = 11 - soma;}
			else{
				r = soma % 11;
				if(r <= 1){dig = 0;}
				else{dig = 11 - r;}
			}

			return (dig == ie[8]);
		}
	}

	//Amapá
	function checkIEAP(ie){
		if (ie.length != 9){return false;}
		else{
			if(ie.substr(0, 2) != '03'){return false;}
			else{
				var i = ie.substr(0, 8);
				var p, d;
				if( (ie >= 3000001) && (i <= 3017000) ){ p = 5; d = 0;}
				else if( (i >= 3017001) && (i <= 3019022) ){ p = 9; d = 1;}
				else if (i >= 3019023){ p = 0; d = 0;}

				var b = 9;
				var soma = p;
				for(i=0;i<=7;i++){
					soma += ie[i] * b;
					b--;
				}
				dig = 11 - (soma % 11);
				if(dig == 10){dig = 0;}
				else if(dig == 11){dig = d;}

				return (dig == ie[8]);
			}
		}
	}

	//Bahia
	function checkIEBA(ie){
		if (ie.length != 8){return false;}
		else{

			arr1 = ['0','1','2','3','4','5','8'];
			arr2 = ['6','7','9'];

			i = ie.substr(0, 1);

			if(arr1.indexOf(i) >= 0)		{ modulo = 10;}
			else if(arr2.indexOf(i) >= 0)		{ modulo = 11;}

			b = 7;
			soma = 0;
			for(i=0;i<=5;i++){
				soma += ie[i] * b;
				b--;
			}

			i = soma % modulo;
			if (modulo == 10){
				if (i == 0) { dig = 0; } else { dig = modulo - i; }
			}else{
				if (i <= 1) { dig = 0; } else { dig = modulo - i; }
			}
			if( !(dig == ie[7]) ){return false;}
			else{
				b = 8;
				soma = 0;
				for(i=0;i<=5;i++){
					soma += ie[i] * b;
					b--;
				}
				soma += ie[7] * 2;
				i = soma % modulo;
				if (modulo == 10){
					if (i == 0) { dig = 0; } else { dig = modulo - i; }
				}else{
					if (i <= 1) { dig = 0; } else { dig = modulo - i; }
				}

				return (dig == ie[6]);
			}
		}
	}

	//Ceará
	function checkIECE(ie){
		if (ie.length != 9){return false;}
		else{
			b = 9;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}
			dig = 11 - (soma % 11);

			if (dig >= 10){dig = 0;}

			return (dig == ie[8]);
		}
	}

	// Distrito Federal
	function checkIEDF(ie){
		if (ie.length != 13){return false;}
		else{
			if( ie.substr(0, 2) != '07' ){return false;}
			else{
				b = 4;
				soma = 0;
				for (i=0;i<=10;i++){
					soma += ie[i] * b;
					b--;
					if(b == 1){b = 9;}
				}
				dig = 11 - (soma % 11);
				if(dig >= 10){dig = 0;}

				if( !(dig == ie[11]) ){return false;}
				else{
					b = 5;
					soma = 0;
					for(i=0;i<=11;i++){
						soma += ie[i] * b;
						b--;
						if(b == 1){b = 9;}
					}
					dig = 11 - (soma % 11);
					if(dig >= 10){dig = 0;}

					return (dig == ie[12]);
				}
			}
		}
	}

	//Espirito Santo
	function checkIEES(ie){
		if (ie.length != 9){return false;}
		else{
			b = 9;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}
			i = soma % 11;
			if (i < 2){dig = 0;}
			else{dig = 11 - i;}

			return (dig == ie[8]);
		}
	}

	//Goias
	function checkIEGO(ie){
		if (ie.length != 9){return false;}
		else{
			s = ie.substr(0, 2);

			if( !( (s == 10) || (s == 11) || (s == 15) ) ){return false;}
			else{
				n = ie.substr(0, 7);

				if(n == 11094402){
					if(ie[8] != 0){
						if(ie[8] != 1){
							return false;
						}else{return true;}
					}else{return true;}
				}else{
					b = 9;
					soma = 0;
					for(i=0;i<=7;i++){
						soma += ie[i] * b;
						b--;
					}
					i = soma % 11;
					if (i == 0){dig = 0;}
					else{
						if(i == 1){
							if((n >= 10103105) && (n <= 10119997)){dig = 1;}
							else{dig = 0;}
						}else{dig = 11 - i;}
					}

					return (dig == ie[8]);
				}
			}
		}
	}

	// Maranhão
	function checkIEMA(ie){
		if (ie.length != 9){return false;}
		else{
			if(ie.substr(0, 2) != 12){return false;}
			else{
				b = 9;
				soma = 0;
				for(i=0;i<=7;i++){
					soma += ie[i] * b;
					b--;
				}
				i = soma % 11;
				if (i <= 1){dig = 0;}
				else{dig = 11 - i;}

				return (dig == ie[8]);
			}
		}
	}

	// Mato Grosso
	function checkIEMT(ie){
		if (ie.length != 11){return false;}
		else{
			b = 3;
			soma = 0;
			for(i=0;i<=9;i++){
				soma += ie[i] * b;
				b--;
				if(b == 1){b = 9;}
			}
			i = soma % 11;
			if (i <= 1){dig = 0;}
			else{dig = 11 - i;}

			return (dig == ie[10]);
		}
	}

	// Mato Grosso do Sul
	function checkIEMS(ie){
		if (ie.length != 9){return false;}
		else{
			if(ie.substr(0, 2) != 28){return false;}
			else{
				b = 9;
				soma = 0;
				for(i=0;i<=7;i++){
					soma += ie[i] * b;
					b--;
				}
				i = soma % 11;
				if (i == 0){dig = 0;}
				else{dig = 11 - i;}

				if(dig > 9){dig = 0;}

				return (dig == ie[8]);
			}
		}
	}

	//Minas Gerais
	function checkIEMG(ie){
		if (ie.length != 13) { return false; }
		
		dig1 = ie.substr(11, 1);
		dig2 = ie.substr(12, 1);
		inscC = ie.substr(0, 3) + '0' + ie.substr(3);
		insc=inscC.split('');
		npos = 11;
		i = 1;
		ptotal = 0;
		psoma = 0;
		
		while (npos >= 0) {
			i++;
			psoma = insc[npos] * i;  
			if (psoma >= 10) { psoma -= 9; }
			
			ptotal += psoma;
			if (i == 2) { i = 0; }
			npos--;
		}

		nresto = ptotal % 10;
		if (nresto == 0) { nresto = 10; }

		nresto = 10 - nresto;
		if (nresto != dig1) { return false; }
		
		npos = 11;
		i = 1;
		ptotal = 0;
		is=ie.split('');
		while (npos >= 0) {
			i++;
			if (i == 12) { i = 2; }
			ptotal += is[npos] * i;
			npos--;
		}

		nresto = ptotal % 11;
		if ((nresto == 0) || (nresto == 1)) { nresto = 11; }
		nresto = 11 - nresto;  
		return (nresto == dig2);
	}

	//Minas Gerais
	function checkIEMGANTIGO(ie){

		// if (ie.length != 13){return false;}
		// else{
		// 	ie2 = ie.substr(0, 3) + '0' + ie.substr(3);

		// 	b = 1;
		// 	soma = "";
		// 	for(i=0;i<=11;i++){
		// 		soma += ie2[i] * b;
		// 		b++;
		// 		if(b == 3){b = 1;}
		// 	}
		// 	s = 0;
		// 	for(i=0;i<soma.length;i++){
		// 		s += soma[i];
		// 	}
		// 	i = ie2.substr(9, 2);
			
		// 	dig = i - s;
			
		// 	if(dig != ie[11]){return false;}
		// 	else{
		// 		b = 3;
		// 		soma = 0;
		// 		for(i=0;i<=11;i++){
		// 			soma += ie[i] * b;
		// 			b--;
		// 			if(b == 1){b = 11;}
		// 		}
		// 		i = soma % 11;
		// 		if(i < 2){dig = 0;}
		// 		else{dig = 11 - i;};

		// 		return (dig == ie[12]);
		// 	}
		// }
	}

	//Pará
	function checkIEPA(ie){
		if (ie.length != 9){return false;}
		else{
			if(ie.substr(0, 2) != 15){return false;}
			else{
				b = 9;
				soma = 0;
				for(i=0;i<=7;i++){
					soma += ie[i] * b;
					b--;
				}
				i = soma % 11;
				if (i <= 1){dig = 0;}
				else{dig = 11 - i;}

				return (dig == ie[8]);
			}
		}
	}

	//Paraíba
	function checkIEPB(ie){
		if (ie.length != 9){return false;}
		else{
			b = 9;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}
			i = soma % 11;
			if (i <= 1){dig = 0;}
			else{dig = 11 - i;}

			if(dig > 9){dig = 0;}

			return (dig == ie[8]);
		}
	}

	//Paraná
	function checkIEPR(ie){
		if (ie.length != 10){return false;}
		else{
			b = 3;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
				if(b == 1){b = 7;}
			}
			i = soma % 11;
			if (i <= 1){dig = 0;}
			else{dig = 11 - i;}

			if ( !(dig == ie[8]) ){return false;}
			else{
				b = 4;
				soma = 0;
				for(i=0;i<=8;i++){
					soma += ie[i] * b;
					b--;
					if(b == 1){b = 7;}
				}
				i = soma % 11;
				if(i <= 1){dig = 0;}
				else{dig = 11 - i;}

				return (dig == ie[9]);
			}
		}
	}

	//Pernambuco
	function checkIEPE(ie){
		if (ie.length == 9){
			b = 8;
			soma = 0;
			for(i=0;i<=6;i++){
				soma += ie[i] * b;
				b--;
			}
			i = soma % 11;
			if (i <= 1){dig = 0;}
			else{dig = 11 - i;}

			if ( !(dig == ie[7]) ){return false;}
			else{
				b = 9;
				soma = 0;
				for(i=0;i<=7;i++){
					soma += ie[i] * b;
					b--;
				}
				i = soma % 11;
				if (i <= 1){dig = 0;}
				else{dig = 11 - i;}

				return (dig == ie[8]);
			}
		}
		else if(ie.length == 14){
			b = 5;
			soma = 0;
			for(i=0;i<=12;i++){
				soma += ie[i] * b;
				b--;
				if(b == 0){b = 9;}
			}
			dig = 11 - (soma % 11);
			if(dig > 9){dig = dig - 10;}

			return (dig == ie[13]);
		}
		else{return false;}
	}

	//Piauí
	function checkIEPI(ie){
		if (ie.length != 9){return false;}
		else{
			b = 9;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}
			i = soma % 11;
			if(i <= 1){dig = 0;}
			else{dig = 11 - i;}
			if(dig >= 10){dig = 0;}

			return (dig == ie[8]);
		}
	}

	// Rio de Janeiro
	function checkIERJ(ie){
		if (ie.length != 8){return false;}
		else{
			b = 2;
			soma = 0;
			for(i=0;i<=6;i++){
				soma += ie[i] * b;
				b--;
				if(b == 1){b = 7;}
			}
			i = soma % 11;
			if (i <= 1){dig = 0;}
			else{dig = 11 - i;}

			return (dig == ie[7]);
		}
	}

	//Rio Grande do Norte
	function checkIERN(ie){
		if( !( (ie.length == 9) || (ie.length == 10) ) ){return false;}
		else{
			b = ie.length;
			if(b == 9){s = 7;}
			else{s = 8;}
			soma = 0;
			for(i=0;i<=s;i++){
				soma += ie[i] * b;
				b--;
			}
			soma *= 10;
			dig = soma % 11;
			if(dig == 10){dig = 0;}

			s += 1;
			return (dig == ie[s]);
		}
	}

	// Rio Grande do Sul
	function checkIERS(ie){
		if (ie.length != 10){return false;}
		else{
			b = 2;
			soma = 0;
			for(i=0;i<=8;i++){
				soma += ie[i] * b;
				b--;
				if (b == 1){b = 9;}
			}
			dig = 11 - (soma % 11);
			if(dig >= 10){dig = 0;}

			return (dig == ie[9]);
		}
	}

	// Rondônia
	function checkIERO(ie){
		if (ie.length == 9){
			b=6;
			soma =0;
			for(i=3;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}
			dig = 11 - (soma % 11);
			if(dig >= 10){dig = dig - 10;}

			return (dig == ie[8]);
		}
		else if(ie.length == 14){
			b=6;
			soma=0;
			for(i=0;i<=12;i++) {
				soma += ie[i] * b;
				b--;
				if(b == 1){b = 9;}
			}
			dig = 11 - ( soma % 11);
			if (dig > 9){dig = dig - 10;}

			return (dig == ie[13]);
		}
		else{return false;}
	}

	//Roraima
	function checkIERR(ie){
		if (ie.length != 9){return false;}
		else{
			if(ie.substr(0, 2) != 24){return false;}
			else{
				b = 1;
				soma = 0;
				for(i=0;i<=7;i++){
					soma += ie[i] * b;
					b++;
				}
				dig = soma % 9;

				return (dig == ie[8]);
			}
		}
	}

	//Santa Catarina
	function checkIESC(ie){

		if (ie.length != 9){return false;}
		else{
			b = 9;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}

			i = soma % 11;
			if (i <= 1)
				dig = 0;
			else
				dig = 11 - i;
			return (dig == ie[8]);

			// dig = 11 - (soma % 11);
			// if (dig <= 1){dig = 0;}

			// return (dig == ie[8]);
		}
	}

	//São Paulo
	function checkIESP(ie){
		if( ie.substr(0, 1)  == 'P' ){
			if (ie.length != 13){return false;}
			else{
				b = 1;
				soma = 0;
				for(i=1;i<=8;i++){
					soma += ie[i] * b;
					b++;
					if(b == 2){b = 3;}
					if(b == 9){b = 10;}
				}
				dig = soma % 11;
				return (dig == ie[9]);
			}
		}else{
			if (ie.length != 12){return false;}
			else{
				b = 1;
				soma = 0;
				for(i=0;i<=7;i++){
					soma += ie[i] * b;
					b++;
					if(b == 2){b = 3;}
					if(b == 9){b = 10;}
				}
				dig = soma % 11;
				if(dig > 9){dig = 0;}

				if(dig != ie[8]){return false;}
				else{
					b = 3;
					soma = 0;
					for(i=0;i<=10;i++){
						soma += ie[i] * b;
						b--;
						if(b == 1){b = 10;}
					}
					dig = soma % 11;

					return (dig == ie[11]);
				}
			}
		}
	}

	//Sergipe
	function checkIESE(ie){
		if (ie.length != 9){return false;}
		else{
			b = 9;
			soma = 0;
			for(i=0;i<=7;i++){
				soma += ie[i] * b;
				b--;
			}
			dig = 11 - (soma % 11);
			if (dig > 9){dig = 0;}

			return (dig == ie[8]);
		}
	}

	//Tocantins
	function checkIETO(ie){

		if (ie.length != 11){return false;}
		else{
			s = ie.substr(2, 2);
			if( (s=='01') || (s=='02') || (s=='03') || (s=='99') ){return false;}
			else{
				b=9;
				soma=0;
				for(var i=0;i<=9;i++){
					if( !((i == 2) || (i == 3)) ){
						soma += ie[i] * b;
						b--;
					}
				}
				i = soma % 11;
				if(i < 2){dig = 0;}
				else{dig = 11 - i;}

				return (dig == ie[10]);
			}
		}
	}


	function checkIE(ie, uf){
		uf = uf.toUpperCase();
		var func = 'checkIE'+uf;
		
		// eval($comando);
		// return $valida;
		// return window[func](ie);
		return eval(func)(ie);
	}

