<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class AddController extends Controller
{
    //Passando parâmetros para adicionar um novo bloco de telefone na view
    public function addPhone(Request $request) {
		try {

			//Analisar a posição do array de telefones
			$index = isset($request->index) ? $request->index : 0;

			//Se existir a view esperada segue o processamento
			if (view()->exists('dashboard.add.add_phone')) {
				return response()->json([
					'success' => true, 
					'html' => view('dashboard.add.add_phone', ['index' => $index])->render()
				]);
			}

		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}

	//Passando parâmetros para adicionar um novo bloco de email na view
	public function addEmail(Request $request) {
		try {

			//Analisar a posição do array de emails
			$index = isset($request->index) ? $request->index : 0;

			//se existir a view esperada segue o processamento
			if (view()->exists('dashboard.add.add_email')) {
				return response()->json([
					'success' => true, 
					'html' => view('dashboard.add.add_email', ['index' => $index])->render()
				]);
			}

		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}

	//Passando parâmetros para adicionar um novo bloco de conta bancária na view
	public function addAccount(Request $request) {
		try {

			//Analisar a posição do array de emails
			$index = isset($request->index) ? $request->index : 0;

			//se existir a view esperada segue o processamento
			if (view()->exists('dashboard.add.add_account')) {
				return response()->json([
					'success' => true, 
					'html' => view('dashboard.add.add_account', ['index' => $index])->render()
				]);
			}

		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}

	//Passando parâmetros para adicionar um novo bloco de upload de arquivo na view
	public function addFile(Request $request) {
		try {

			//Analisar a posição do array de arquivos
			$index = isset($request->index) ? $request->index : 0;

			//se existir a view esperada segue o processamento
			if (view()->exists('dashboard.add.add_file')) {
				return response()->json([
					'success' => true, 
					'html' => view('dashboard.add.add_file', ['index' => $index])->render()
				]);
			}

		} catch (Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}

	//Passando parâmetros para adicionar um novo bloco de contato na view
    public function addContact(Request $request) {
		try {
			
			//Analisar a posição do array de contatos
			$index = isset($request->index) ? $request->index : 0;
			$index_phone = isset($request->index_phone) ? $request->index_phone : 0;
			$index_email = isset($request->index_email) ? $request->index_email : 0;

			//Se existir a view esperada segue o processamento
			if (view()->exists('dashboard.add.add_contact')) {
				return response()->json([
					'success' => true, 
					'html' => view('dashboard.add.add_contact', ['index' => $index, 'index_phone' => $index_phone, 'index_email' => $index_email])->render()
				]);
			}

		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}

	//Passando parâmetros para adicionar uma nova linha de telefone no bloco de contatos adicionais
    public function addContactPhone(Request $request) {
		try {

			//Analisar a posição do array de contatos
			$index = isset($request->index) ? $request->index : 0;
			$index_phone = isset($request->index_phone) ? $request->index_phone : 0;

			//Se existir a view esperada segue o processamento
			if (view()->exists('dashboard.add.add_contact_phone')) {
				return response()->json([
					'success' => true, 
					'html' => view('dashboard.add.add_contact_phone', ['index' => $index, 'index_phone' => $index_phone])->render()
				]);
			}

		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}

	//Passando parâmetros para adicionar uma nova linha de email no bloco de contatos adicionais
    public function addContactEmail(Request $request) {
		try {
			
			//Analisar a posição do array de contatos
			$index = isset($request->index) ? $request->index : 0;
			$index_email = isset($request->index_email) ? $request->index_email : 0;

			//Se existir a view esperada segue o processamento
			if (view()->exists('dashboard.add.add_contact_email')) {
				return response()->json([
					'success' => true, 
					'html' => view('dashboard.add.add_contact_email', ['index' => $index, 'index_email' => $index_email])->render()
				]);
			}

		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}
}
