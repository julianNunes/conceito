<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Address;
use App\Models\State;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AddressController extends Controller
{
	public function index() {
		try{
			$address = Address::orderBy('default', 'desc')->get(); 
			//Buscandos todos os estados e os organizando pelas iniciais.
			$states = State::orderBy('initials')->get(['id', 'initials']);          
			return view('dashboard.user.address.index')->with(['address' => $address, 'states' => $states]);

		} catch(\Exception $e) {
           dd($e->getMessage());
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';
			//verifico todos os dados de endereco
			if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 
			if($request->street == ''){  $errors .= 'O campo Logradouro é obrigatório. <br/>';  } 
			if($request->number == ''){  $errors .= 'O campo Número é obrigatório. <br/>';  } 		
			if($request->district == ''){  $errors .= 'O campo Bairro é obrigatório. <br/>';  } 
			if($request->zipcode == ''){  $errors .= 'O campo CEP é obrigatório. <br/>';  }
			if($request->type == ''){  $errors .= 'O campo Tipo do Endereço é obrigatório. <br/>';  }	

			if($request->address_type == 'condominium') {

				if($request->address_cond == '') 	{ $errors .= 'O campo Endereço do Condiminio é obrigatório. <br/>'; }
				if($request->number_cond == '') 		{ $errors .= 'O campo Numero do Condiminio é obrigatório. <br/>'; }
			}

			if($errors != ''){
	            return response()->json([
	                'success' => false, 
	                'error' =>  $errors
	            ]); 

			} else{       

				$params = array(
					'name' 			=> $request->name,
					'street' 		=> $request->street,
					'number' 		=> $request->number,					
					'comp' 			=> $request->comp, 
					'district' 		=> $request->district, 					
					'zipcode' 		=> $request->zipcode,
					'info' 			=> $request->info,
					'type' 			=> $request->type,
					'address_cond'	=> $request->address_cond,
					'number_cond'	=> $request->number_cond,
					'default' 		=> $request->default,
					'city_id' 		=> $request->city,
					'user_id' 		=> Auth::user()->id,
				);

				$address = Address::create($params);
				if(isset($address)) {

					if($request->default == 1) {
						DB::table('adresses')->where('id', '!=', $address->id)->update(['default' => 0]);
					}
									
		            return response()->json([
		                'success' => true
		            ]);  
				}
			}

		} catch(\Exception $e) {
            return response()->json([
                'success' => false, 
                'error' => $e->getMessage()
            ]); 
		}
	}

	public function edit($id) {
		try {
			//Buscando o ciente requisitado e seus relacionamentos.
			$address = Address::with('city.state')->find($id);
			//Se encontrar o cliente buscado acima...
			if(isset($address)) { 
				return response()->json([ 'success' => true, 'address' => $address ]);
			}

			return response()->json([ 'success' => false ]);

		} catch(\Exception $e) {
            return response()->json([
                'success' => false, 
                'error' => $e->getTrace()
            ]); 
		}
	}

	public function update(Request $request, $id) {  
		try{

			$address = Address::find($id); 
			if(isset($address)) {

				//Validação de erros
				$errors = '';
				//verifico todos os dados de endereco
				if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 
				if($request->street == ''){  $errors .= 'O campo Logradouro é obrigatório. <br/>';  } 
				if($request->number == ''){  $errors .= 'O campo Número é obrigatório. <br/>';  } 		
				if($request->district == ''){  $errors .= 'O campo Bairro é obrigatório. <br/>';  } 
				if($request->zipcode == ''){  $errors .= 'O campo CEP é obrigatório. <br/>';  }
				if($request->type == ''){  $errors .= 'O campo Tipo do Endereço é obrigatório. <br/>';  }	

				if($request->address_type == 'condominium') {

					if($request->address_cond == '') 	{ $errors .= 'O campo Endereço do Condiminio é obrigatório. <br/>'; }
					if($request->number_cond == '') 		{ $errors .= 'O campo Numero do Condiminio é obrigatório. <br/>'; }
				}

				if($errors != ''){
		            return response()->json([
		                'success' => false, 
		                'error' =>  $errors
		            ]); 

				} else{       

					$params = array(
						'name' 			=> $request->name,
						'street' 		=> $request->street,
						'number' 		=> $request->number,					
						'comp' 			=> $request->comp, 
						'district' 		=> $request->district, 					
						'zipcode' 		=> $request->zipcode,
						'info' 			=> $request->info,
						'type' 			=> $request->type,
						'address_cond'	=> $request->address_cond,
						'number_cond'	=> $request->number_cond,
						'default' 		=> $request->default,
						'city_id' 		=> $request->city
					);

					$address->update($params);

					if($request->default == 1) {
						DB::table('adresses')->where('id', '!=', $address->id)->where('user_id', Auth::user()->id)->update(['default' => 0]);
					}
									
		            return response()->json([
		                'success' => true
		            ]);  
				}
			}

	        return response()->json([
	            'success' => false, 
	            'error' =>  'Não foi encontrado Endereço'
	        ]); 

		} catch(\Exception $e) {
            return response()->json([
                'success' => false, 
                'error' => $e->getTrace()
            ]); 
		}
	}


	public function destroy(Request $request, $id) {
		try{
			$address = Address::find($id);  
			if(isset($address)) {

				if($address->default == 1) {
					$address->delete();

					$aux = Auth::user()->adreeses()->first();
					if(isset($aux)) { $aux->update(['default' => 1]); }
					
	            	return response()->json([ 'success' =>  true]);  
				} else {

					$address->delete();
	            	return response()->json([ 'success' =>  true]);  
				}
			}

			return response()->json([ 'success' => false ]);

		} catch (\Exception $e) {
            return response()->json([
                'success' => false, 
                'error' => $e->getTrace()
            ]); 			
		}
	}
}
