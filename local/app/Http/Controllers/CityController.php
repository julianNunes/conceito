<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{

	public function getByState(Request $request) {
		try {
			
			$cities = City::where('state_id', '=', $request->state_id)->get(['id', 'name']);
			
			return response()->json([
				'success' => true, 
				'cities' => $cities
			]);

		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}
}