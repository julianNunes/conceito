<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Company;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CompanyController extends Controller
{

	public function selection(Request $request, $id) { 
		try{
			//Validação de erros
			$errors = '';
			if($request->id == ''){  $errors .= 'O campo Id é obrigatório. <br/>';  } 

			if(isset($id)){
				$company= Company::find($id);
				if(isset($company)) { 
					
					$user = Auth::user();
					$user->update(['company_id' => $company->id]);
					
					return back();
				} 
				\Session::flash('warning_message', 'Empresa não encontrada');
			} 

			return back();

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500); 
		}
	}

	public function index() {
		try{
			
			$companies = Company::orderBy('name')->get(); 
			return view('dashboard.company.index')->with(['companies'=> $companies]);

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function create() {
		try{
		
			return view('dashboard.company.edit')->with([
				'page_description' 	=> 'Cadastrar',
				'route'				=> ['name' => 'company.store', 'params' => []]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';
			
			// if($request->company_name == ''){  $errors .= 'O campo Razão Social é obrigatório. <br/>';  } 
			if($request->name == '') { $errors .= 'O campo Nome Fantasia é obrigatório. <br/>'; 	}	
			if($request->cnpj == '') { $errors .= 'O campo CNPJ é obrigatório. <br/>';  } 
			if($request->cnpj != '' && strlen($request->cnpj) < 14) {  $errors .= 'CNPJ inválido. <br/>';  }
			if($request->phone_number == '') { $errors .= 'O campo Número de Telefone é obrigatório. <br/>';  } 
            if($request->email == ''){  $errors .= 'O campo Email é obrigatório. <br/>';  } 
            if($request->active == ''){  $errors .= 'O campo Ativo é obrigatório. <br/>';  } 
			
			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			}    
			
			//Verifica se o CPF ou CNPJ ja existe
			$cnpj_count = Company::where('cnpj','=' , $request->cnpj)->count(); 
			
			if($cnpj_count > 0) { $errors .= 'O CNPJ já esta cadastrado. <br/>'; }

			if($errors != ''){
				\Session::flash('warning_message', $errors);
				return back();
			}    
			
			$company = Company::create([
				'name' 			=> $request->name, 
				'cnpj' 			=> $request->cnpj, 
				'phone_number' 	=> $request->phone_number, 
				'email' 		=> $request->email, 
				'active' 		=> $request->active,
				'user_id'		=> Auth::user()->id,
			]);

			if(isset($company)) {
				\Session::flash('success_message', 'Empresa cadastrada com sucesso');
				return \Redirect::route('company.index');  
			}

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			$company= Company::find($id);
			if(isset($company)) { 
				return view('dashboard.company.edit')->with([
					'company' 			=> $company,
					'page_description' 	=> 'Editar',
					'route'				=> ['name' => 'company.update', 'params' => ['id' => $id] ]
				]);
			}

			\Session::flash('warning_message', 'Empresa não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$company= Company::find($id); 
			if(isset($company)) {

				//Validação de erros
				$errors = '';
				if($request->name == '') { $errors .= 'O campo Nome Fantasia é obrigatório. <br/>'; 	}	
				if($request->cnpj == '') { $errors .= 'O campo CNPJ é obrigatório. <br/>';  } 
				if($request->cnpj != '' && strlen($request->cnpj) < 14) {  $errors .= 'CNPJ inválido. <br/>';  }
				if($request->phone_number == '') { $errors .= 'O campo Número de Telefone é obrigatório. <br/>';  } 
				if($request->email == ''){  $errors .= 'O campo Email é obrigatório. <br/>';  } 
				if($request->active == ''){  $errors .= 'O campo Ativo é obrigatório. <br/>';  } 

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();

				} 
							
				//Verifica se o CPF ou CNPJ ja existe
				$cnpj_count = Company::where('cnpj','=' , $request->cnpj)->where('id', '!=', $company->id)->count(); 
				
				if($cnpj_count > 0) { $errors .= 'O CNPJ já esta cadastrado. <br/>'; }

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				}           

				$company->update([
					'name' 			=> $request->name, 
					'cnpj' 			=> $request->cnpj, 
					'phone_number' 	=> $request->phone_number, 
					'email' 		=> $request->email, 
					'active' 		=> $request->active
				]);
				
				\Session::flash('success_message', 'Empresa atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Empresa não encontrada');
            return back();

		} catch(\Exception $e) {
			dd($e->getMessage());
            abort(500);
		}
	}

	
	public function show($id) {
		try {

			$company= Company::find($id);
			if(isset($company)) { 
				return view('dashboard.company.edit')->with([
					'company' 			=> $company,
					'route'				=> ['name' => 'company.edit', 'params' => ['id' => $company->id]],
					'page_description' 	=> 'Ver',
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Empresa não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			$company= Company::find($id);
			if(isset($company)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Empresa',
					'route'			=> ['name' => 'company.destroy', 'params' => ['id' => $company->id]],
					'fields' => [
						['col' => '6', 'title' => 'Nome', 'value' => $company->name],
						['col' => '6', 'title' => 'CNPJ', 'value' => $company->cnpj]
					]
				]);
			}

			\Session::flash('warning_message', 'Empresa não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{
			$company= Company::find($id);  
			if(isset($company)) {

				$company->delete();
				\Session::flash('success_message', 'Empresa removida com sucesso');
				return \Redirect::route('company.index');  
			}

			\Session::flash('warning_message', 'Empresa não encontrada');
            return \Redirect::route('company.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}
}
