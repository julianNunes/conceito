<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Fabricator;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class FabricatorController extends Controller
{
	public function index() {
		try{
		
			$fabricators = Fabricator::where('company_id', Auth::user()->company->id)->orderBy('name')->get(); 
			return view('dashboard.fabricator.index')->with(['fabricators'=> $fabricators]);

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function create() {
		try{
		
			return view('dashboard.fabricator.edit')->with([
				'page_description' 	=> 'Cadastrar',
				'route'				=> ['name' => 'fabricator.store' , 'params' => [] ]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';
			//verifico todos os dados de endereco
			if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			} 

			//Verifica se o Nome ja existe
			$name_count = Fabricator::where('name','=' , $request->name)
			->where('company_id', Auth::user()->company->id)
			->count(); 
			
			if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			} 

			$fabricator= Fabricator::create([
				'name' 		=> $request->name, 
				'active' 	=> $request->active,
				'company_id'=> Auth::user()->company->id
			]);

			if(isset($fabricator)) {
				\Session::flash('success_message', 'Fabricante cadastrada com sucesso');
				return \Redirect::route('fabricator.index');  
			}

		} catch(\Exception $e) {
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			$fabricator= Fabricator::find($id);
			if(isset($fabricator)) { 
				return view('dashboard.fabricator.edit')->with([
					'fabricator' 		=> $fabricator,
					'page_description' 	=> 'Editar',
					'route'				=> ['name' => 'fabricator.update', 'params' => ['id' => $fabricator->id]]
				]);
			}

			\Session::flash('warning_message', 'Fabricante não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$fabricator= Fabricator::find($id); 
			if(isset($fabricator)) {

				//Validação de erros
				$errors = '';
				//verifico todos os dados de endereco
				if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				}  

				//Verifica se o Nome ja existe
				$name_count = Fabricator::where('name','=' , $request->name)
				->where('company_id', Auth::user()->company->id)
				->where('id', '!=', $fabricator->id)
				->count(); 
				
				if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				} 

				$fabricator->update([
					'name' 		=> $request->name, 
					'active' 	=> $request->active,
					'company_id'=> Auth::user()->company->id
				]);

				\Session::flash('success_message', 'Fabricante atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Fabricante não encontrada');
            return back();

		} catch(\Exception $e) {
            abort(500);
		}
	}

	
	public function show($id) {
		try {

			$fabricator= Fabricator::find($id);
			if(isset($fabricator)) { 
				return view('dashboard.fabricator.edit')->with([
					'fabricator' 		=> $fabricator,
					'route'				=> ['name' => 'fabricator.edit', 'params' => ['id' => $fabricator->id]],
					'page_description' 	=> 'Ver',
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Fabricante não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			$fabricator= Fabricator::find($id);
			if(isset($fabricator)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Fabricante',
					'route'			=> ['name' => 'fabricator.destroy', 'params' => ['id' => $fabricator->id]],
					'fields' => [
						['col' => '6', 'title' => 'Nome', 'value' => $fabricator->name]
					]
				]);
			}

			\Session::flash('warning_message', 'Fabricante não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{
			$fabricator= Fabricator::find($id);  
			if(isset($fabricator)) {
				$fabricator->delete();
				
				\Session::flash('success_message', 'Fabricante removida com sucesso');
				return \Redirect::route('fabricator.index');  
			}

			\Session::flash('warning_message', 'Fabricante não encontrada');
            return \Redirect::route('fabricator.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}
}
