<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Family;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class FamilyController extends Controller
{
	public function index() {
		try{
		
			$families = Family::where('company_id', Auth::user()->company->id)->orderBy('name')->get(); 
			return view('dashboard.family.index')->with(['families'=> $families]);

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function create() {
		try{
		
			return view('dashboard.family.edit')->with([
				'page_description' 	=> 'Cadastrar',
				'route'				=> ['name' => 'family.store' , 'params' => [] ]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';
			//verifico todos os dados de endereco
			if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();

			}    

			//Verifica se o Nome ja existe
			$name_count = Family::where('name','=' , $request->name)
			->where('company_id', Auth::user()->company->id)
			->count(); 
			
			if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			} 

			$family = Family::create([
				'name' 		=> $request->name, 
				'active' 	=> $request->active,
				'company_id'=> Auth::user()->company->id
			]);

			if(isset($family)) {
				\Session::flash('success_message', 'Família cadastrada com sucesso');
				return \Redirect::route('family.index');  
			}

		} catch(\Exception $e) {
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			$family= Family::find($id);
			if(isset($family)) { 
				return view('dashboard.family.edit')->with([
					'family' 			=> $family,
					'page_description' 	=> 'Editar',
					'route'				=> ['name' => 'family.update', 'params' => ['id' => $family->id]]
				]);
			}

			\Session::flash('warning_message', 'Família não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$family= Family::find($id); 
			if(isset($family)) {

				//Validação de erros
				$errors = '';
				//verifico todos os dados de endereco
				if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				}        

				//Verifica se o Nome ja existe
				$name_count = Family::where('name','=' , $request->name)
				->where('company_id', Auth::user()->company->id)
				->where('id', '!=', $family->id)
				->count(); 
				
				if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				}

				$family->update([
					'name' 		=> $request->name, 
					'active' 	=> $request->active,
					'company_id'=> Auth::user()->company->id
				]);

				\Session::flash('success_message', 'Família atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Família não encontrada');
            return back();

		} catch(\Exception $e) {
            abort(500);
		}
	}

	
	public function show($id) {
		try {

			$family = Family::find($id);
			if(isset($family)) { 
				return view('dashboard.family.edit')->with([
					'family' 			=> $family,
					'route'				=> ['name' => 'family.edit', 'params' => ['id' => $family->id]],
					'page_description' 	=> 'Ver',
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Família não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			$family= Family::find($id);
			if(isset($family)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Família',
					'route'			=> ['name' => 'family.destroy', 'params' => ['id' => $family->id]],
					'fields' => [
						['col' => '6', 'title' => 'Nome', 'value' => $family->name]
					]
				]);
			}

			\Session::flash('warning_message', 'Família não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{
			$family= Family::find($id);  
			if(isset($family)) {
				$family->delete();

				\Session::flash('success_message', 'Família removida com sucesso');
				return \Redirect::route('family.index');  
			}

			\Session::flash('warning_message', 'Família não encontrada');
            return \Redirect::route('family.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}
}
