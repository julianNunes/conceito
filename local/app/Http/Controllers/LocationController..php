<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Location;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class LocationController extends Controller
{
	public function index() {
		try{

			$locations = Location::with('location')->where('company_id', Auth::user()->company->id)->orderBy('name')->get(); 
			return view('dashboard.location.index')->with(['locations'=> $locations]);
			
		} catch(\Exception $e) {
			
			abort(500);
		}
	}
	
	public function create() {
		try{
			
			$locations = Location::where('company_id', Auth::user()->company->id)->orderBy('name')->get(); 
			return view('dashboard.location.edit')->with([
				'page_description' 	=> 'Cadastrar',
				'locations' 		=> $locations,
				'route'				=> ['name' => 'location.store' , 'params' => [] ]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';
			//verifico todos os dados de endereco
			if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 
			
			if($errors != ''){
				\Session::flash('warning_message', $errors);
				return back();
			}    
			
			//Verifica se o Nome ja existe
			$name_count = Location::where('name','=' , $request->name)
			->where('company_id', Auth::user()->company->id)
			->count(); 

			if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			}    

			$location = Location::create([
				'name' 			=> $request->name, 
				'description' 	=> $request->description, 
				'active' 		=> $request->active,
				'company_id'	=> Auth::user()->company->id
			]);
			if(isset($location)) {
				\Session::flash('success_message', 'Localização cadastrada com sucesso');
				return \Redirect::route('location.index');  
			}

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			$location = Location::find($id);
			if(isset($location)) { 
				return view('dashboard.location.edit')->with([
					'location' 			=> $location,
					'page_description' 	=> 'Editar',
					'route'				=> ['name' => 'location.update', 'params' => ['id' => $location->id]]
				]);
			}

			\Session::flash('warning_message', 'Localização não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$location = Location::find($id); 
			if(isset($location)) {

				//Validação de erros
				$errors = '';
				//verifico todos os dados de endereco
				if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 
				
				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				}
				

				//Verifica se o Nome ja existe
				$name_count = Location::where('name','=' , $request->name)
				->where('company_id', Auth::user()->company->id)
				->where('id', '!=', $location->id)
				->count(); 

				if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				}
				$location->update([
					'name' 			=> $request->name, 
					'description' 	=> $request->description, 
					'active' 		=> $request->active
				]);

				\Session::flash('success_message', 'Localização atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Localização não encontrada');
            return back();

		} catch(\Exception $e) {
			dd($e->getMessage());	
			abort(500); 
		}
	}

	
	public function show($id) {
		try {

			$location = Location::find($id);
			if(isset($location)) { 
				return view('dashboard.location.edit')->with([
					'location' 			=> $location,
					'route'				=> ['name' => 'location.edit', 'params' => ['id' => $location->id]],
					'page_description' 	=> 'Ver',
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Localização não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			$location = Location::find($id);
			if(isset($location)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Localização',
					'route'			=> ['name' => 'location.destroy', 'params' => ['id' => $location->id]],
					'fields' 		=> [
						['col' => '6', 'title' => 'Nome', 'value' => $location->name],
						['col' => '6', 'title' => 'Nome do Pai', 'value' => isset($location->location) ? $location->location->name : '']
					]
				]);
			}

			\Session::flash('warning_message', 'Localização não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{
			$location = Location::find($id);  
			if(isset($location)) {
				$location->delete();
				
				\Session::flash('success_message', 'Localização removida com sucesso');
				return \Redirect::route('location.index');  
			}

			\Session::flash('warning_message', 'Localização não encontrada');
            return \Redirect::route('location.index');  

		} catch (\Exception $e) {
            abort(500); 			
		}
	}
}
