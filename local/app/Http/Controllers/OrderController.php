<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Property;
use App\Models\Provider;
use App\Models\Order;
use App\Models\TypeOs;
use App\Models\User;
use Auth;
use Gate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{

	public function index(Request $request) {
		try{

			if(!empty($request->start)) { $start = Carbon::createFromFormat('d-m-Y', $request->start)->startOfDay(); }
			else 						{ $start = null; }
			
			if(!empty($request->end)) 	{ $end = Carbon::createFromFormat('d-m-Y', $request->end)->endOfDay(); }
			else 						{ $end = null; }
		
			$orders = Order::with([
				'user' => function($q) 		{ $q->withTrashed(); },
				'typeOs' => function($q) 	{ $q->withTrashed(); },
				'property' => function($q)	{ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); }
			])
			->when(Auth::user()->administrator == 1, function ($q) {
				return $q->where('company_id', Auth::user()->company->id);
				
			})	
			->when(!empty($start), function ($q) use ($start) {
				return $q->where('date_open', '>=', $start->toDateTimeString());
			})
			->when(!empty($end), function ($q) use ($end) {
				return $q->where('date_open', '<=', $end->toDateTimeString());
			})	
			->when(Auth::user()->administrator == 0, function ($q) use ($end) {
				return $q->where('user_id',  Auth::user()->id);
			})	
			->get(); 

			$params = [
				'orders'	=> $orders, 
				'start' 	=> !empty($start) ? $start->format('d/m/Y') : null, 
				'end' 		=> !empty($end) ? $end->format('d/m/Y') : null
			];

			//Collapse Sidebar
			\Session::flash('sidebar-collapse', 'sidebar-collapse');
			return view('dashboard.order.index', $params);

		} catch(\Exception $e) {
			dd($e->getTrace());
			abort(500);
		}
	}

	public function create() {
		try{

			if(Auth::user()->administrator == 0) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	
					
			$users = null;
			if(\Auth::user()->administrator == 1) {
				$users = User::where('id', '!=', \Auth::user())->where('active', 1)->get(['id', 'name', 'nickname']);
			}

			return view('dashboard.order.edit')->with([
				'users' 			=> $users,
				'types' 			=> TypeOs::where('company_id', Auth::user()->company->id)->where('active', 1)->get(['id', 'name']),
				'property' 			=> Property::where('company_id', Auth::user()->company->id)->where('active', 1)->get(['id', 'model', 'description']),
				'providers' 		=> Provider::where('company_id', Auth::user()->company->id)->where('active', 1)->get(['id', 'name']),
				'page_description' 	=> 'Cadastrar',
				'route'				=> ['name' => 'order.store' , 'params' => [] ]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';

			if($request->activity == '') { $errors .= 'O campo Atividade é obrigatório. <br/>'; 	}	
			// if($request->observation == '') { $errors .= 'O campo Observação é obrigatório. <br/>';  } 
			if($request->date_open == '') { $errors .= 'O campo Data de Abertura é obrigatório. <br/>';  } 
            // if($request->date_start_order == ''){  $errors .= 'O campo Data de Inicio da OS é obrigatório. <br/>';  } 
			// if($request->date_close == ''){  $errors .= 'O campo Data de Entrega é obrigatório. <br/>';  } 
			if($request->urgent == ''){  $errors .= 'O campo Urgente é obrigatório. <br/>';  } 
			if($request->type_os_id == ''){  $errors .= 'O campo Tipo de OS é obrigatório. <br/>';  } 
			if($request->property_id == ''){  $errors .= 'O campo Fabricante é obrigatório. <br/>';  } 
			if($request->provider_id == ''){  $errors .= 'O campo Equipamento é obrigatório. <br/>';  } 
			if(Auth::user()->administrator == 1) {
				if($request->user_id == ''){  $errors .= 'O campo Usuário é obrigatório. <br/>';  } 
			}

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();

			} else{       
					
				$order = Order::create([
					'activity' 			=> $request->activity, 
					'observation' 		=> trim($request->observation), 
					'date_open' 		=> $request->date_open, 
					'date_start_order' 	=> isset($request->date_start_order) ? $request->date_start_order : null, 
					'date_close'		=> isset($request->date_close) ? $request->date_close : null,
					'urgent' 			=> $request->urgent,
					'status' 			=> 'pending',
					'type_os_id' 		=> $request->type_os_id,
					'property_id' 		=> $request->property_id,
					'provider_id' 		=> $request->provider_id,
					'user_id' 			=> Auth::user()->administrator ? $request->user_id : null,
					'company_id'		=> Auth::user()->company->id
				]);

				if(isset($order)) {
					\Session::flash('success_message', 'Ordem de Serviço cadastrada com sucesso');
					return \Redirect::route('order.index');  
				}
			}

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500); 
		}
	}

	public function edit(Request $request, $id) {
		try {
			
			if(Auth::user()->administrator == 0) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	

			$order= Order::with([
				'user' => function($q) 		{ $q->withTrashed(); },
				'typeOs' => function($q) 	{ $q->withTrashed(); },
				'property' => function($q)	{ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);

			if(isset($order)) { 
				
				$users = null;
				if(Auth::user()->administrator == 1) {
					$users = User::where('id', '!=', \Auth::user())->where('active', 1)->where('id', '!=', $order->user_id)->get(['id', 'name', 'nickname']);
				}

				$types 		= TypeOs::where('company_id', Auth::user()->company->id)->where('active', 1)->where('id', '!=', $order->type_os_id)->get(['id', 'name']);
				$property 	= Property::where('company_id', Auth::user()->company->id)->where('active', 1)->where('id', '!=', $order->property_id)->get(['id', 'model']);
				$providers 	= Provider::where('company_id', Auth::user()->company->id)->where('active', 1)->where('id', '!=', $order->provider_id)->get(['id', 'name']);

				$params = [
					'users' 			=> $users,
					'order' 			=> $order,
					'types' 			=> $types,
					'property' 			=> $property,
					'providers' 		=> $providers,
					'page_description' 	=> 'Cadastrar',
					'route'				=> ['name' => 'order.update' , 'params' => ['id' => $order->id] ]
				];

				return view('dashboard.order.edit', $params);
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return back();

		} catch(\Exception $e) {
			dd($e->getTrace());
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$order = Order::with([
				'typeOs' => function($q) 	{ $q->withTrashed(); },
				'property' => function($q)	{ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);

			if(isset($order)) {

				//Validação de erros
				$errors = '';
				if($request->activity == '') { $errors .= 'O campo Atividade é obrigatório. <br/>'; 	}	
				// if($request->observation == '') { $errors .= 'O campo Observação é obrigatório. <br/>';  } 
				if($request->date_open == '') { $errors .= 'O campo Data de Abertura é obrigatório. <br/>';  } 
				// if($request->date_start_order == ''){  $errors .= 'O campo Data de Inicio da OS é obrigatório. <br/>';  } 
				// if($request->date_close == ''){  $errors .= 'O campo Data de Entrega é obrigatório. <br/>';  } 
				if($request->urgent == ''){  $errors .= 'O campo Urgente é obrigatório. <br/>';  } 
				if($request->type_os_id == ''){  $errors .= 'O campo Tipo de OS é obrigatório. <br/>';  } 
				if($request->property_id == ''){  $errors .= 'O campo Fabricante é obrigatório. <br/>';  } 
				if($request->provider_id == ''){  $errors .= 'O campo Equipamento é obrigatório. <br/>';  } 
				if(Auth::user()->administrator == 1) {
					if($request->user_id == ''){  $errors .= 'O campo Usuário é obrigatório. <br/>';  } 
				}

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();

				} else{       

					$order->update([
						'activity' 			=> $request->activity, 
						'observation' 		=> trim($request->observation), 
						'date_open' 		=> $request->date_open, 
						'date_start_order' 	=> isset($request->date_start_order) ? $request->date_start_order : null, 
						'date_close'		=> isset($request->date_close) ? $request->date_close : null,
						'urgent' 			=> $request->urgent,
						'status' 			=> $request->status,
						'type_os_id' 		=> $request->type_os_id,
						'property_id' 		=> $request->property_id,
						'provider_id' 		=> $request->provider_id,
						'user_id' 			=> (Auth::user()->administrator == 1) ? $request->user_id : null,
						'company_id'		=> Auth::user()->company->id
					]);

		            \Session::flash('success_message', 'Ordem de Serviço atualizado com sucesso');
					return back(); 
				}
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return back();

		} catch(\Exception $e) {
			
            abort(500);
		}
	}
	
	public function show($id) {
		try {

			$order= Order::with([
				'user' => function($q) 	{ $q->withTrashed(); },
				'typeOs' => function($q) 	{ $q->withTrashed(); },
				'property' => function($q)	{ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);

			if(isset($order)) { 
				return view('dashboard.order.edit')->with([
					'order' 			=> $order,
					'route'				=> ['name' => 'order.edit' , 'params' => ['id' => $order->id] ],
					'page_description' 	=> 'Ver',
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			if(Auth::user()->administrator == 1) {
				\Session::flash('warning_message', 'Voce não tem permissão para excluir Ordem de Serviço');
				return back();				
			}

			$order= Order::find($id);
			if(isset($order)) { 
				return view('dashboard.layout.delete')->with([
					'fields' => [
						['col' => '6', 'title' => 'OS', 'value' => $order->id],
						['col' => '6', 'title' => 'Data de Início', 'value' => $order->id]
					]
				]);
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{

			if(Auth::user()->administrator == 1) {
				\Session::flash('warning_message', 'Voce não tem permissão para excluir Ordem de Serviço');
				return back();				
			}

			$order= Order::find($id);  
			if(isset($order)) {
				$order->delete();
				
				\Session::flash('success_message', 'Ordem de Serviço removida com sucesso');
				return \Redirect::route('order.index');  
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');

		} catch (\Exception $e) {
            abort(500);			
		}
	}

	public function confirmDone($id) {
		try {

			$order= Order::with([
				'user' => function($q) 		{ $q->withTrashed(); },
				'typeOs' => function($q) 	{ $q->withTrashed(); },
				'property' => function($q)	{ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);

			if(isset($order)) { 
				return view('dashboard.order.edit')->with([
					'show' 				=> true,
					'done'	 			=> true,
					'order' 			=> $order,
					'route'				=> ['name' => 'order.done' , 'params' => ['id' => $order->id] ],
					'page_description' 	=> 'Encerrar',
					'done'				=> true
				]);
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return back();

		} catch(\Exception $e) {
			
			abort(500);
		}
	}	

	public function done(Request $request, $id) {
		try{
			$order= Order::find($id);  
			if(isset($order)) {
				$order->update(['date_close' => new \DateTime('now'), 'status' => 'done', 'commentary' => $request->commentary]);

				\Session::flash('success_message', 'Ordem de Serviço Encerrada com sucesso');
				return \Redirect::route('order.index');  
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return \Redirect::route('order.index');  

		} catch (\Exception $e) {
			dd($e->getTrace());
            abort(500);			
		}
	}	

	public function confirmCancel($id) {
		try {

			$order= Order::with([
				'typeOs' => function($q) 	{ $q->withTrashed(); },
				'property' => function($q)	{ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);

			if(isset($order)) { 
				return view('dashboard.order.edit')->with([
					'show' 				=> true,
					'cancel'			=> true,
					'order' 			=> $order,
					'route'				=> ['name' => 'order.cancel' , 'params' => ['id' => $order->id] ],
					'page_description' 	=> 'Cancelar',
					'done'				=> true
				]);
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function cancel(Request $request, $id) {
		try{
			$order= Order::find($id);  
			if(isset($order)) {
				$order->update(['status' => 'canceled', 'commentary' => $request->commentary]);

				\Session::flash('success_message', 'Ordem de Serviço Cancelada com sucesso');
				return \Redirect::route('order.index');  
			}

			\Session::flash('warning_message', 'Ordem de Serviço não encontrada');
            return \Redirect::route('order.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}

	public function impress(Request $request) {
		//Verifica a permissão do usuário
		// $permission = 'index-show';
		// if (Gate::denies('checkUserPermission', ['attendance', $permission])) { abort(403); }
		
		try {

			if(isset($request->id)) {
				$order= Order::with([
					'user' => function($q) 	{ $q->withTrashed(); },
					'typeOs' => function($q) 	{ $q->withTrashed(); },
					'property' => function($q)	{ $q->withTrashed(); },
					'provider' => function($q) 	{ $q->withTrashed(); }
				])
				->find($request->id);
					
				if(isset($order)) {
					if (view()->exists('dashboard.order.impress')) {
						return response()->json([
							'success' => true, 
							'html' => view('dashboard.order.impress', ['order' => $order])->render()
						]);
					}
				} 
			}
			return response()->json(['success' => false]);
			
		} catch (\Exception $e) {
			return response()->json(['success' => false, 'error' => $e->getTrace()]);
		}
	}	
}
