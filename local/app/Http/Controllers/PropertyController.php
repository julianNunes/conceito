<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Fabricator;
use App\Models\Family;
use App\Models\Location;
use App\Models\Property;
use App\Models\Provider;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PropertyController extends Controller
{

	public function index() {
		try{
		
			$property = Property::with([
				'family' => function($q) 	{ $q->withTrashed(); },
				'fabricator' => function($q){ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); },
				'location' => function($q) 	{ $q->withTrashed(); }
			])
			->where('company_id', Auth::user()->company->id)
			->orderBy('code')
			->get();
			
			//Collapse Sidebar
			\Session::flash('sidebar-collapse', 'sidebar-collapse');			 
			return view('dashboard.property.index')->with(['property'=> $property]);

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function create() {
		try{

			$params = [
				'fabricators' 		=> Fabricator::where('company_id', Auth::user()->company->id)->where('active', 1)->get(['id', 'name']),
				'families' 			=> Family::where('company_id', Auth::user()->company->id)->where('active', 1)->get(['id', 'name']),
				'providers' 		=> Provider::where('company_id', Auth::user()->company->id)->where('active', 1)->get(['id', 'name']),
				'locations' 		=> Location::where('company_id', Auth::user()->company->id)->where('active', 1)->get(['id', 'name']),
				'page_description' 	=> 'Cadastrar',
				'route'				=> ['name' => 'property.store' , 'params' => [] ]
			];

			return view('dashboard.property.edit', $params);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';

			// if($request->company_name == ''){  $errors .= 'O campo Razão Social é obrigatório. <br/>';  } 
			if($request->model == '') { $errors .= 'O campo Modelo é obrigatório. <br/>'; 	}	
			if($request->description == '') { $errors .= 'O campo Descrição é obrigatório. <br/>';  } 
			if($request->code == '') { $errors .= 'O campo Plaqueta é obrigatório. <br/>';  } 
            if($request->date_purchase == ''){  $errors .= 'O campo Data de Aquisição é obrigatório. <br/>';  } 
			// if($request->last_maintenance == ''){  $errors .= 'O campo Ultima Manutenção é obrigatório. <br/>';  } 
			if($request->total == '') {  $errors .= 'O campo Total é obrigatório. <br/>';  } 
            else {
				$total = str_replace(",",".", str_replace(".","", $request->total));
                if($total <= 0.00) { $errors .= 'O campo Total precisa ser maior que zero. <br/>'; }
            }
			if($request->nf == ''){  $errors .= 'O campo Nota Fiscal é obrigatório. <br/>';  } 
			if($request->active == ''){  $errors .= 'O campo Ativo é obrigatório. <br/>';  } 
			if($request->fabricator_id == ''){  $errors .= 'O campo Fabricante é obrigatório. <br/>';  } 
			if($request->family_id == ''){  $errors .= 'O campo Família é obrigatório. <br/>';  } 
			if($request->provider_id == ''){  $errors .= 'O campo Fornecedor é obrigatório. <br/>';  } 
			if($request->location_id == ''){  $errors .= 'O campo Localização é obrigatório. <br/>';  } 

			
			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			}      

			$property= Property::create([
				'model' 			=> $request->model, 
				'description' 		=> $request->description, 
				'code' 				=> $request->code, 
				'date_purchase' 	=> $request->date_purchase, 
				'last_maintenance'	=> isset($request->last_maintenance) ? $request->last_maintenance : null,
				'nf' 				=> $request->nf,
				'total' 			=> $request->total,
				'active' 			=> $request->active,
				'fabricator_id' 	=> $request->fabricator_id,
				'family_id' 		=> $request->family_id,
				'provider_id' 		=> $request->provider_id,
				'location_id' 		=> $request->location_id,
				'company_id'		=> Auth::user()->company->id
			]);

			if(isset($property)) {
				\Session::flash('success_message', 'Fornecedor cadastrada com sucesso');
				return \Redirect::route('property.index');  
			}

		} catch(\Exception $e) {
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			$property= Property::with([
				'family' => function($q) 	{ $q->withTrashed(); },
				'fabricator' => function($q){ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); },
				'location' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);
			
			if(isset($property)) { 

				$fabricators = Fabricator::where('company_id', Auth::user()->company->id)->where('active', 1)->where('id', '!=', $property->fabricator_id)->get(['id', 'name']);
				$families = Family::where('company_id', Auth::user()->company->id)->where('active', 1)->where('id', '!=', $property->family_id)->get(['id', 'name']);
				$providers = Provider::where('company_id', Auth::user()->company->id)->where('active', 1)->where('id', '!=', $property->provider_id)->get(['id', 'name']);
				$locations = Location::where('company_id', Auth::user()->company->id)->where('active', 1)->where('id', '!=', $property->location_id)->get(['id', 'name']);

				$params = [
					'property' 		=> $property,
					'fabricators' 		=> $fabricators,
					'families' 			=> $families,
					'providers' 		=> $providers,
					'locations' 		=> $locations,
					'page_description' 	=> 'Cadastrar',
					'route'				=> ['name' => 'property.update', 'params' => ['id' => $property->id]]
				];

				return view('dashboard.property.edit', $params);
			}

			\Session::flash('warning_message', 'Bens não encontrada');
            return back();

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$property = Property::with([
				'family' => function($q) 	{ $q->withTrashed(); },
				'fabricator' => function($q){ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); },
				'location' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);

			if(isset($property)) {

				//Validação de erros
				$errors = '';
				if($request->model == '') { $errors .= 'O campo Modelo é obrigatório. <br/>'; 	}	
				if($request->description == '') { $errors .= 'O campo Descrição é obrigatório. <br/>';  } 
				if($request->code == '') { $errors .= 'O campo Plaqueta é obrigatório. <br/>';  } 
				if($request->date_purchase == ''){  $errors .= 'O campo Data de Aquisição é obrigatório. <br/>';  } 
				// if($request->last_maintenance == ''){  $errors .= 'O campo Ultima Manutenção é obrigatório. <br/>';  } 
				if($request->total == '') {  $errors .= 'O campo Total é obrigatório. <br/>';  } 
				else {
					$total = str_replace(",",".", str_replace(".","", $request->total));
					if($total <= 0.00) { $errors .= 'O campo Total precisa ser maior que zero. <br/>'; }
				}
				if($request->nf == ''){  $errors .= 'O campo Nota Fiscal é obrigatório. <br/>';  } 
				if($request->active == ''){  $errors .= 'O campo Ativo é obrigatório. <br/>';  } 
				if($request->fabricator_id == ''){  $errors .= 'O campo Fabricante é obrigatório. <br/>';  } 
				if($request->family_id == ''){  $errors .= 'O campo Família é obrigatório. <br/>';  } 
				if($request->provider_id == ''){  $errors .= 'O campo Fornecedor é obrigatório. <br/>';  } 
				if($request->location_id == ''){  $errors .= 'O campo Localização é obrigatório. <br/>';  } 

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				}   

				$property->update([
					'model' 			=> $request->model, 
					'description' 		=> $request->description, 
					'code' 				=> $request->code, 
					'date_purchase' 	=> $request->date_purchase, 
					'last_maintenance'	=> isset($request->last_maintenance) ? $request->last_maintenance : null,
					'nf' 				=> $request->nf,
					'total' 			=> $request->total,
					'active' 			=> $request->active,
					'fabricator_id' 	=> $request->fabricator_id,
					'family_id' 		=> $request->family_id,
					'provider_id' 		=> $request->provider_id,
					'location_id' 		=> $request->location_id
				]);

				\Session::flash('success_message', 'Bens atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Bens não encontrada');
            return back();

		} catch(\Exception $e) {
            abort(500);
		}
	}

	
	public function show($id) {
		try {

			$property = Property::with([
				'family' => function($q) 	{ $q->withTrashed(); },
				'fabricator' => function($q){ $q->withTrashed(); },
				'provider' => function($q) 	{ $q->withTrashed(); },
				'location' => function($q) 	{ $q->withTrashed(); }
			])
			->find($id);
			
			if(isset($property)) { 
				return view('dashboard.property.edit')->with([
					'property' 			=> $property,
					'route'				=> ['name' => 'property.edit', 'params' => ['id' => $property->id]],
					'page_description' 	=> 'Ver',
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Bens não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			$property= Property::find($id);
			if(isset($property)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Bens',
					'route'			=> ['name' => 'property.destroy', 'params' => ['id' => $property->id]],
					'fields' => [
						['col' => '6', 'title' => 'Nome', 'value' => $property->name]
					]
				]);
			}

			\Session::flash('warning_message', 'Bens não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{
			$property= Property::find($id);  
			if(isset($property)) {
				$property->delete();

				\Session::flash('success_message', 'Bens removida com sucesso');
				return \Redirect::route('property.index');  
			}

			\Session::flash('warning_message', 'Bens não encontrada');
            return \Redirect::route('property.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}
}
