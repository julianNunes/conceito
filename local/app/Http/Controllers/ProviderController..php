<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Address;
use App\Models\City;
use App\Models\State;
use App\Models\Provider;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ProviderController extends Controller
{

	public function index() {
		try{
		
			$providers = Provider::where('company_id', Auth::user()->company->id)->orderBy('name')->get(); 
			return view('dashboard.provider.index')->with(['providers'=> $providers]);

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function create() {
		try{

			$states = State::orderBy('initials')->get(['id', 'initials']);
			return view('dashboard.provider.edit')->with([
				'page_description' 	=> 'Cadastrar',
				'states' 			=> $states,
				'route'				=> ['name' => 'provider.store' , 'params' => [] ]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';
			
			// if($request->company_name == ''){  $errors .= 'O campo Razão Social é obrigatório. <br/>';  } 
			if($request->name == '') { $errors .= 'O campo Nome Fantasia é obrigatório. <br/>'; 	}	
			if($request->cpf_cnpj == '') { $errors .= 'O campo CNPJ é obrigatório. <br/>';  } 
			if($request->cpf_cnpj != '' && strlen($request->cpf_cnpj) < 14) {  $errors .= 'CNPJ inválido. <br/>';  }
			if($request->phone_number == '') { $errors .= 'O campo Número de Telefone é obrigatório. <br/>';  } 
            if($request->email == ''){  $errors .= 'O campo Email é obrigatório. <br/>';  } 
            if($request->active == ''){  $errors .= 'O campo Ativo é obrigatório. <br/>';  } 
			// Endereço
			if($request->street == ''){  $errors .= 'O campo Logradouro é obrigatório. <br/>';  } 
			if($request->number == ''){  $errors .= 'O campo Número é obrigatório. <br/>';  } 		
			if($request->district == ''){  $errors .= 'O campo Bairro é obrigatório. <br/>';  } 
			if($request->zipcode == ''){  $errors .= 'O campo CEP é obrigatório. <br/>';  }
			if($request->city_id == ''){  $errors .= 'O campo Cidade do Endereço é obrigatório. <br/>';  }				
			
			if($errors != ''){
				\Session::flash('warning_message', $errors);
				return back();
			}      
			
			//Verifica se o Nome ja existe
			$cnpj_count = Provider::where('cpf_cnpj','=' , $request->cpf_cnpj)
			->where('company_id', Auth::user()->company->id)
			->count(); 
			
			if($cnpj_count > 0) { $errors .= 'O CNPJ já esta cadastrado. <br/>'; }
			
			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			}      

			$provider= Provider::create([
				'name' 			=> $request->name, 
				'cpf_cnpj' 		=> $request->cpf_cnpj, 
				'phone_number' 	=> $request->phone_number, 
				'email' 		=> $request->email, 
				'active' 		=> $request->active,
				'company_id'	=> Auth::user()->company->id
			]);

			if(isset($provider)) {
				$address = Address::create([
					'street' 				=> $request->street,
					'number' 				=> $request->number,					
					'comp' 					=> $request->comp, 
					'district' 				=> $request->district, 					
					'zipcode' 				=> $request->zipcode,
					'info' 					=> $request->info,
					'city_id' 				=> $request->city_id,
					'addressable_type' 		=> Provider::class,
					'addressable_id' 		=> $provider->id
				]);

				\Session::flash('success_message', 'Fornecedor cadastrada com sucesso');
				return \Redirect::route('provider.index');  
			}
			
			\Session::flash('warning_message', 'Fornecedor não cadastrado.');
			return \Redirect::route('provider.index');  

		} catch(\Exception $e) {
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			$provider = Provider::with(['address' => function($q) { $q->withTrashed(); }])->find($id);
			if(isset($provider)) { 
				
				$states = State::where('id', '!=', $provider->address->city->state->id)->orderBy('initials')->get(['id', 'initials']);       
				$cities = City::where('state_id', '=', $provider->address->city->state->id)->where('id', '!=', $provider->address->city->id)->orderBy('name')->get(['id', 'name']);          
				
				return view('dashboard.provider.edit')->with([
					'provider' 			=> $provider,
					'states' 			=> $states,
					'cities' 			=> $cities,
					'page_description' 	=> 'Editar',
					'route'				=> ['name' => 'provider.update', 'params' => ['id' => $provider->id]]
				]);
			}

			\Session::flash('warning_message', 'Fornecedor não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$provider = Provider::with(['address' => function($q) { $q->withTrashed(); }])->find($id); 
			if(isset($provider)) {

				//Validação de erros
				$errors = '';
				
				if($request->name == '') { $errors .= 'O campo Nome Fantasia é obrigatório. <br/>'; 	}	
				if($request->cpf_cnpj == '') { $errors .= 'O campo CNPJ é obrigatório. <br/>';  } 
				if($request->cpf_cnpj != '' && strlen($request->cpf_cnpj) < 14) {  $errors .= 'CNPJ inválido. <br/>';  }
				if($request->phone_number == '') { $errors .= 'O campo Número de Telefone é obrigatório. <br/>';  } 
				if($request->email == ''){  $errors .= 'O campo Email é obrigatório. <br/>';  } 
				if($request->active == ''){  $errors .= 'O campo Ativo é obrigatório. <br/>';  } 

				// Endereço
				if($request->street == ''){  $errors .= 'O campo Logradouro é obrigatório. <br/>';  } 
				if($request->number == ''){  $errors .= 'O campo Número é obrigatório. <br/>';  } 		
				if($request->district == ''){  $errors .= 'O campo Bairro é obrigatório. <br/>';  } 
				if($request->zipcode == ''){  $errors .= 'O campo CEP é obrigatório. <br/>';  }
				if($request->city_id == ''){  $errors .= 'O campo Cidade do Endereço é obrigatório. <br/>';  }

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				} 
				
				//Verifica se o Nome ja existe
				$cnpj_count = Provider::where('cpf_cnpj','=' , $request->cpf_cnpj)
				->where('company_id', Auth::user()->company->id)
				->where('id', '!=', $provider->id)
				->count(); 
				
				if($cnpj_count > 0) { $errors .= 'O CNPJ já esta cadastrado. <br/>'; }

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				} 
				
				$provider->update([
					'name' 			=> $request->name, 
					'cpf_cnpj' 		=> $request->cpf_cnpj, 
					'phone_number' 	=> $request->phone_number, 
					'email' 		=> $request->email, 
					'active' 		=> $request->active
				]);
				
				$provider->address->update([
					'street' 	=> $request->street,
					'number' 	=> $request->number,					
					'comp' 		=> $request->comp, 
					'district' 	=> $request->district, 					
					'zipcode' 	=> $request->zipcode,
					'info' 		=> $request->info,
					'city_id' 	=> $request->city_id
				]);

				\Session::flash('success_message', 'Fornecedor atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Fornecedor não encontrada');
            return back();

		} catch(\Exception $e) {
			dd($e->getTrace());
            abort(500);
		}
	}

	
	public function show($id) {
		try {

			$provider= Provider::with(['location' => function($q) { $q->withTrashed(); }])->with('address')->find($id);
			if(isset($provider)) { 
				return view('dashboard.provider.edit')->with([
					'provider' 			=> $provider,
					'page_description' 	=> 'Ver',
					'route'			=> ['name' => 'provider.edit', 'params' => ['id' => $provider->id]],
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Fornecedor não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			$provider= Provider::find($id);
			if(isset($provider)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Fornecedores',
					'route'			=> ['name' => 'provider.destroy', 'params' => ['id' => $provider->id]],
					'fields' => [
						['col' => '6', 'title' => 'Nome', 'value' => $provider->name]
					]
				]);
			}

			\Session::flash('warning_message', 'Fornecedor não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{
			$provider= Provider::find($id);  
			if(isset($provider)) {
				$provider->delete();

				\Session::flash('success_message', 'Fornecedor removida com sucesso');
				return \Redirect::route('provider.index');  
			}

			\Session::flash('warning_message', 'Fornecedor não encontrada');
            return \Redirect::route('provider.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}
}
