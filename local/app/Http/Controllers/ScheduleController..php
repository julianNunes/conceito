<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Order;
use Auth;
use Gate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ScheduleController extends Controller
{
	//Página Inicial
	public function index() {
		try{			
			
			//Collapse Sidebar
			\Session::flash('sidebar-collapse', 'sidebar-collapse');
			return view('dashboard.schedule.index');

		} catch (\Exception $e) {
			abort(500);
		}
	}	

	//Metodo utilizado para buscar todos os agendamentos em um determinado periodo
	public function getByDay(Request $request) {	 	
	 	try{

			if(isset($request->start) && isset($request->end)) { 

				$orders = Order::with([
					'typeOs' => function($q) { $q->withTrashed(); },
					'user' => function($q) { $q->withTrashed()->addSelect(['id', 'name', 'nickname']); }
				])
				->where('date_open', '>=', $request->start)
				->where('date_open', '<=', $request->end)
				->when(!Auth::user()->administrator == 1, function ($q) {
					return $q->where('user_id', Auth::user()->user->id);
				})	
				->orderBy('date_open', 'DESC')
				->get(); 

				$events = [];
				$aux;
				if(count($orders) >0) {
					
					$now = Carbon::now()->endOfDay();
					foreach ($orders as $key => $order) {
						$aux = [];
						if($order->urgent == 1) { 
							$aux['color'] = '#dd4b39'; //bootstrap class 'text-blue'
						} else {
							$aux['color'] = '#00c0ef'; //bootstrap class 'text-aqua'
						} 

						$title = $order->id.' - '.$order->typeOs->name;
						if(Auth::user()->administrator == 1 ) {
							$title .= ' - '.$order->user->getFormattedName();
						}
						
						$aux['id']		= $order->id;
						$aux['editable'] = false;
						$aux['title']	= $title;
						$aux['title_2']	= $title.' - '.$order->activity;
						$aux['start']	= $order->date_open;
						// $aux['end']		= $order->end;
						$aux['allDay'] 	= true;
						
						$events[$key] = $aux;
					}
				}

				return \Response::json([
					'success' 		=> true,
					'events' 		=> $events
				]);
			} 

		} catch (\Exception $e) {
			
			return \Response::json([
				'success' 	=> false,
				'error' 	=> $e->getMessage(),
				'error2' 	=> $e->getTrace()
			]);
		}
	}
}
