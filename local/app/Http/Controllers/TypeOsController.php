<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\TypeOs;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class TypeOsController extends Controller
{
	public function index() {
		try{
		
			$types = TypeOs::where('company_id', Auth::user()->company->id)->orderBy('name')->get(); 
			return view('dashboard.type_os.index')->with(['types'=> $types]);

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500);
		}
	}

	public function create() {
		try{
		
			return view('dashboard.type_os.edit')->with([
				'page_description' 	=> 'Cadastrar',
				'route'				=> ['name' => 'type-os.store' , 'params' => [] ]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{
			//Validação de erros
			$errors = '';
			//verifico todos os dados de endereco
			if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			}   
			
			//Verifica se o Nome ja existe
			$name_count = TypeOs::where('name','=' , $request->name)
			->where('company_id', Auth::user()->company->id)
			->count(); 
			
			if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }

			if($errors != ''){
				\Session::flash('warning_message', $errors);
				return back();
			}   

			$type_os = TypeOs::create([
				'name' 		=> $request->name, 
				'active' 	=> $request->active,
				'company_id'=> Auth::user()->company->id
			]);
			if(isset($type_os)) {
				\Session::flash('success_message', 'Tipo de OS cadastrada com sucesso');
				return \Redirect::route('type-os.index');  
			}

		} catch(\Exception $e) {
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			$type_os= TypeOs::find($id);
			if(isset($type_os)) { 
				return view('dashboard.type_os.edit')->with([
					'type' 				=> $type_os,
					'page_description' 	=> 'Editar',
					'route'				=> ['name' => 'type-os.update', 'params' => ['id' => $type_os->id]]
				]);
			}

			\Session::flash('warning_message', 'Tipo de OS não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			$type_os = TypeOs::find($id); 
			if(isset($type_os)) {

				//Validação de erros
				$errors = '';
				//verifico todos os dados de endereco
				if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				} 
				
				//Verifica se o Nome ja existe
				$name_count = TypeOs::where('name','=' , $request->name)
				->where('company_id', Auth::user()->company->id)
				->where('id', '!=', $type_os->id)
				->count(); 
				
				if($name_count > 0) { $errors .= 'O Nome já esta cadastrado. <br/>'; }
				
				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				} 
				
				$type_os->update([
					'name' 		=> $request->name, 
					'active' 	=> $request->active,
					'company_id'=> Auth::user()->company->id
				]);

				\Session::flash('success_message', 'Tipo de OS atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Tipo de OS não encontrada');
            return back();

		} catch(\Exception $e) {
			dd($e->getTrace());
            abort(500);
		}
	}

	
	public function show($id) {
		try {

			$type_os= TypeOs::find($id);
			if(isset($type_os)) { 
				return view('dashboard.type_os.edit')->with([
					'type'	 			=> $type_os,
					'page_description' 	=> 'Ver',
					'route'				=> ['name' => 'type-os.edit', 'params' => ['id' => $type_os->id]],
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Tipo de OS não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			$type_os= TypeOs::find($id);
			if(isset($type_os)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Tipo de Os',
					'route'			=> ['name' => 'type-os.destroy', 'params' => ['id' => $type_os->id]],
					'fields' => [
						['col' => '6', 'title' => 'Nome', 'value' => $type_os->name]
					]
				]);
			}

			\Session::flash('warning_message', 'Tipo de OS não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{
			$type_os= TypeOs::find($id);  
			if(isset($type_os)) {
				$type_os->delete();

				\Session::flash('success_message', 'Tipo de OS removida com sucesso');
				return \Redirect::route('type-os.index');  
			}
			
			\Session::flash('warning_message', 'Tipo de OS não encontrada');
			return \Redirect::route('type-os.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}
}
