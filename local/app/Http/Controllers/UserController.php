<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function home(Request $request) {
       return view('login/login');
    }

    public function signup(Request $request ) {
        return view('login/sign_up');   
    }

    public function config() {
        try {
            return view('dashboard.user.config.index');  

        } catch(Excpetion $e) {
            abort(400);
        } 
    }

    public function updateConfig(Request $request) {
        //Verifica a permissão do usuário
        try {
              
            // //Valida se o usuário logado é o root
            // if($id == 1){ return \Redirect::route('user.index'); }

            //Validação de erros
            $errors = '';
            if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  } 
            if($request->email == ''){  $errors .= 'O campo Email é obrigatório. <br/>';  } 
            if($request->password == ''){  $errors .= 'O campo Senha é obrigatório. <br/>';  } 
            if($request->password_confirmation == ''){  $errors .= 'O campo Confirmar Senha é obrigatório. <br/>';  } 
            if($request->password != '' && $request->password_confirmation != '' && $request->password_confirmation != $request->password) {  $errors .= 'As senhas não são iguais. <br/>';  }

            if($errors != ''){
                \Session::flash('flash_message', $errors);
                return back();
            }

            $user = Auth::user();
            if(isset($user) && $user->id > 0) {

                if($request->email != $user->email) {
                    
                    //Verifica se o email é unico
                    $user_email = User::where('email','=' , $request->email)->where('id','!=' , $user->id)->withTrashed()->count(); 
                    if($user_email > 0){ 
                        
                        \Session::flash('flash_message', 'Este e-mail ja está cadastrado. <br/>');
                        return back();
                    }       
                } 

                $params = array(
                    'name'              => $request->name,
                    'nickname'          => $request->nickname,
                    'email'             => $request->email
                );

                if($request->password != '') { $params['password'] = bcrypt($request->password); }

                $user->update($params);

                \Session::flash('flash_message2', 'Edição realizada com sucesso.'); 
                return back();
            }
            return back();

        } catch(Excpetion $e) {
            abort(500);
        }
    }

    public function index() {
		try{

			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	

			$users = User::where('id', '!=', Auth::user()->id)->orderBy('name')->get(); 
			return view('dashboard.user.index')->with(['users'=> $users]);

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500);
		}
	}

	public function create() {
		try{
			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	
			
			return view('dashboard.user.edit')->with([
				'page_description' 	=> 'Cadastrar',
				'route'				=> ['name' => 'user.store' , 'params' => [] ]
			]);

		} catch(\Exception $e) {
			abort(500);
		}
	}


	public function store(Request $request) { 
		try{

			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	

			//Validação de erros
            $errors = '';
            
			if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  }
			if($request->email == ''){  $errors .= 'O campo Email é obrigatório. <br/>';  } 		
			if($request->password == ''){  $errors .= 'O campo Senha é obrigatório. <br/>';  } 
			if($request->password_confirmation == ''){  $errors .= 'O campo Confirmar Senha é obrigatório. <br/>';  } 
			if($request->password != '' && $request->password_confirmation != '' && $request->password_confirmation != $request->password) {  $errors .= 'As senhas não são iguais. <br/>';  }
			if($request->active == ''){  $errors .= 'O campo Ativo é obrigatório. <br/>';  }

			if($errors != ''){
                \Session::flash('warning_message', $errors);
                return back();
			}   
			
            //Verifica se o Email ja existe
            $email_count = User::where('email','=' , $request->email)
            ->count(); 
            
            if($email_count > 0) { $errors .= 'O Email já esta cadastrado. <br/>'; }

			if($errors != ''){
				\Session::flash('warning_message', $errors);
				return back();
			}   

			$user = User::create([
				'name'         		=> $request->name,
				'nickname'        	=> $request->nickname,
				'email'        		=> $request->email,
				'password'     		=> bcrypt($request->password),
				'active'  			=> $request->active,
				'administrator'  	=> $request->administrator
            ]);
            
			if(isset($user)) {
				\Session::flash('success_message', 'Usuário cadastrada com sucesso');
				return \Redirect::route('user.index');  
			}

		} catch(\Exception $e) {
			dd($e->getMessage());
			abort(500); 
		}
	}

	public function edit($id) {
		try {

			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	

			$user= User::find($id);
			if(isset($user)) { 
				return view('dashboard.user.edit')->with([
					'user' 				=> $user,
					'page_description' 	=> 'Editar',
					'route'				=> ['name' => 'user.update', 'params' => ['id' => $user->id]]
				]);
			}

			\Session::flash('warning_message', 'Usuário não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function update(Request $request, $id) {  
		try{

			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	

			$user = User::find($id); 
			if(isset($user)) {

				//Validação de erros
				$errors = '';
                if($request->name == ''){  $errors .= 'O campo Nome é obrigatório. <br/>';  }
                if($request->email == ''){  $errors .= 'O campo Email é obrigatório. <br/>';  } 		
                if($request->password == ''){  $errors .= 'O campo Senha é obrigatório. <br/>';  } 
                if($request->password_confirmation == ''){  $errors .= 'O campo Confirmar Senha é obrigatório. <br/>';  } 
                if($request->password != '' && $request->password_confirmation != '' && $request->password_confirmation != $request->password) {  $errors .= 'As senhas não são iguais. <br/>';  }

				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				} 
				
				//Verifica se o Email ja existe
				$email_count = User::where('email','=' , $request->email)
				->where('id', '!=', $user->id)
				->count(); 
				
				if($email_count > 0) { $errors .= 'O Email já esta cadastrado. <br/>'; }
				
				if($errors != ''){
					\Session::flash('warning_message', $errors);
					return back();
				} 
				
				$user->update([
                    'name'         		=> $request->name,
                    'nickname'        	=> $request->nickname,
                    'email'        		=> $request->email,
                    'password'     		=> bcrypt($request->password),
                    'active'  			=> $request->active,
                    'administrator' 	=> $request->administrator,
				]);

				\Session::flash('success_message', 'Usuário atualizado com sucesso');
				return back(); 
			}

			\Session::flash('warning_message', 'Usuário não encontrada');
            return back();

		} catch(\Exception $e) {
			dd($e->getTrace());
            abort(500);
		}
	}
	
	public function show($id) {
		try {

			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	

			$user= User::find($id);
			if(isset($user)) { 
				return view('dashboard.user.edit')->with([
					'user'	 			=> $user,
					'page_description' 	=> 'Ver',
					'route'				=> ['name' => 'user.edit', 'params' => ['id' => $user->id]],
					'show'				=> true
				]);
			}

			\Session::flash('warning_message', 'Usuário não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}

	public function delete($id) {
		try {

			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	

			$user= User::find($id);
			if(isset($user)) { 
				return view('dashboard.layout.delete')->with([
					'page_title' 	=> 'Usuário',
					'route'			=> ['name' => 'user.destroy', 'params' => ['id' => $user->id]],
					'fields' => [
						['col' => '6', 'title' => 'Nome', 'value' => $user->name]
					]
				]);
			}

			\Session::flash('warning_message', 'Usuário não encontrada');
            return back();

		} catch(\Exception $e) {
			abort(500);
		}
	}	

	public function destroy(Request $request, $id) {
		try{

			if(!Auth::user()->administrator == 1 ) {
				\Session::flash('warning_message', 'Voce não tem permissão para essa ação');
				return back();				
			}	
						
			$user= User::find($id);  
			if(isset($user)) {
				$user->delete();

				\Session::flash('success_message', 'Usuário removida com sucesso');
				return \Redirect::route('user.index');  
			}
			
			\Session::flash('warning_message', 'Usuário não encontrada');
			return \Redirect::route('user.index');  

		} catch (\Exception $e) {
            abort(500);			
		}
	}
}
