<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () { return redirect('/dashboard');});

Route::auth();

Route::group(['prefix' => 'dashboard', 'as' => 'home.', 'middleware' => 'dashboard'], function() {   
	Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);     
});
/* Admin */
Route::group(['prefix' => 'dashboard/admin', 'as' => 'admin.', 'middleware' => 'dashboard'], function() {
	// Route::get('/', ['as' => 'index', 'uses' => 'AdminController@index']);
	Route::get('/config', ['as' => 'config', 'uses' => 'UserController@config']);
	Route::post('/update', ['as' => 'update-config', 'uses' => 'UserController@updateConfig']);
});

//Usuario
Route::group(['prefix' => 'dashboard/usuario', 'as' => 'user.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'UserController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'UserController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'UserController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'UserController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'UserController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'UserController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'UserController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'UserController@destroy']); 	  
});

//Add
Route::group(['prefix' => 'dashboard/adicionar', 'as' => 'add.', 'middleware' => 'dashboard'], function() {
	Route::get('/novo-email', ['as' => 'add-email', 'uses' => 'AddController@addEmail']);
	Route::get('/novo-telefone', ['as' => 'add-phone', 'uses' => 'AddController@addPhone']); 
	Route::get('/novo-arquivo', ['as' => 'add-file', 'uses' => 'AddController@addFile']);	
	Route::get('/nova-conta', ['as' => 'add-account', 'uses' => 'AddController@addAccount']);
	Route::get('/novo-contato', ['as' => 'add-contact', 'uses' => 'AddController@addContact']);
	Route::get('/novo-telefone-contato', ['as' => 'add-contact-phone', 'uses' => 'AddController@addContactPhone']);
	Route::get('/novo-email-contato', ['as' => 'add-contact-email', 'uses' => 'AddController@addContactEmail']);
	
});

//Empresa
Route::group(['prefix' => 'dashboard/empresa', 'as' => 'company.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'CompanyController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'CompanyController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'CompanyController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'CompanyController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'CompanyController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'CompanyController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'CompanyController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'CompanyController@destroy']); 	
	Route::get('{id}/selecao', ['as' => 'selection', 'uses' => 'CompanyController@selection']);   
});

//Location
Route::group(['prefix' => 'dashboard/locatizacao', 'as' => 'location.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'LocationController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'LocationController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'LocationController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'LocationController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'LocationController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'LocationController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'LocationController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'LocationController@destroy']); 	
});

//Fornecedor
Route::group(['prefix' => 'dashboard/fornecedor', 'as' => 'provider.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'ProviderController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'ProviderController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'ProviderController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'ProviderController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'ProviderController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'ProviderController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'ProviderController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'ProviderController@destroy']); 	
});

//Family
Route::group(['prefix' => 'dashboard/familia', 'as' => 'family.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'FamilyController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'FamilyController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'FamilyController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'FamilyController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'FamilyController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'FamilyController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'FamilyController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'FamilyController@destroy']); 	
});

//Fabricante
Route::group(['prefix' => 'dashboard/fabricante', 'as' => 'fabricator.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'FabricatorController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'FabricatorController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'FabricatorController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'FabricatorController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'FabricatorController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'FabricatorController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'FabricatorController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'FabricatorController@destroy']); 	
});

//Equipamentos 
Route::group(['prefix' => 'dashboard/bens', 'as' => 'property.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'PropertyController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'PropertyController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'PropertyController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'PropertyController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'PropertyController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'PropertyController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'PropertyController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'PropertyController@destroy']); 	
});

//Tipo de Os 
Route::group(['prefix' => 'dashboard/tipo-os', 'as' => 'type-os.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'TypeOsController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'TypeOsController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'TypeOsController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'TypeOsController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'TypeOsController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'TypeOsController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'TypeOsController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'TypeOsController@destroy']); 	
});

//Ordem de Serviço 
Route::group(['prefix' => 'dashboard/ordem-servico', 'as' => 'order.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'OrderController@index']);
	Route::post('/', ['as' => 'store', 'uses' => 'OrderController@store']);
	Route::get('/cadastrar', ['as' => 'create', 'uses' => 'OrderController@create']); 
	Route::get('{id}/ver', ['as' => 'show', 'uses' => 'OrderController@show']);  
	Route::get('{id}/editar', ['as' => 'edit', 'uses' => 'OrderController@edit']);
	Route::post('{id}/editar', ['as' => 'update', 'uses' => 'OrderController@update']);   
	Route::get('{id}/excluir', ['as' => 'delete', 'uses' => 'OrderController@delete']);
	Route::post('{id}/excluir', ['as' => 'destroy', 'uses' => 'OrderController@destroy']); 	
	Route::get('{id}/cancelar', ['as' => 'confirm-cancel', 'uses' => 'OrderController@confirmCancel']);
	Route::post('{id}/cancelar', ['as' => 'cancel', 'uses' => 'OrderController@cancel']); 	
	Route::get('{id}/encerrar', ['as' => 'confirm-done', 'uses' => 'OrderController@confirmDone']);
	Route::post('{id}/encerrar', ['as' => 'done', 'uses' => 'OrderController@done']); 	
	Route::get('/imprimir', ['as' => 'impress', 'uses' => 'OrderController@impress']);
});

Route::group(['prefix' => 'dashboard/agenda', 'as' => 'schedule.', 'middleware' => 'dashboard'], function() {
	Route::get('/', ['as' => 'index', 'uses' => 'ScheduleController@index']);
	Route::get('/por-data', ['as' => 'get-by-day', 'uses' => 'ScheduleController@getByDay']);
});

//City
Route::group(['prefix' => 'dashboard/cidade', 'as' => 'city.', 'middleware' => 'dashboard'], function() {
	Route::get('/por-estado', ['as' => 'state', 'uses' => 'CityController@getByState']);
});






