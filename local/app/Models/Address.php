<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model {

	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'adresses';
	protected $fillable = ['street','number', 'comp', 'district', 'zipcode', 'type', 'info','address_cond', 'number_cond', 'city_id', 'addressable_type', 'addressable_id'];

	// Relationships
	public function addressable() {
	  return $this->morphTo();
	}    	

	public function city() {
		return $this->belongsTo('App\Models\City', 'city_id', 'id');	
	}    	
}
