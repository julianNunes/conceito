<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class City extends Model {

	protected $table = 'city';
	protected $fillable = ['name', 'slug', 'state_id'];

	 // Relationships
	public function state() {
		return $this->belongsTo('App\Models\State', 'state_id', 'id');
	}    	
}
