<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model 
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'companies';
	protected $fillable = ['name', 'company_name', 'cnpj',  'email', 'phone_number','active', 'user_id'];

	// Relationships
	public function user() {
		return $this->belongsTo('App\Models\User', 'id', 'user_id');
	}
    
    //Acessors
    public function getFormattedActiveAttribute() {
        if(isset($this->active)) {
            return $this->active == 1 ? "Sim" : "Não";
        }
        return "";
    }	
}