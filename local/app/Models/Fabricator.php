<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;

class Fabricator extends Model {
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'fabricators';
    protected $fillable = ['name','active', 'company_id'];
    
    //Acessors
    public function getFormattedActiveAttribute() {
        if(isset($this->active)) {
            return $this->active == 1 ? "Sim" : "Não";
        }
        return "";
    }    
}
