<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Family extends Model {
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'families';
    protected $fillable = ['name','active', 'company_id'];
    
    //Acessors
    public function getFormattedActiveAttribute() {
        if(isset($this->active)) {
            return $this->active == 1 ? "Sim" : "Não";
        }
        return "";
    }
}
