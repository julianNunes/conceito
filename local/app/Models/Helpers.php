<?php
namespace App\Models;

class Helpers {

	static public function showFilters($data)
	{
		if(is_array($data) && count($data)>0)
		{	
			$filters = "";
			foreach ($data as $key => $value) {
				
				if($key == (count($data)-1)) {
					$filters = 	$filters.$data[$key];
				} else {
					$filters = 	$filters.$data[$key].' / ';
				}
			}
		} else {
			echo("<script>console.log('PHP: ".$data."');</script>");
		}
		return $filters;
	}

	static public function readMore($text, $length) {
		if(strlen($text) >= $length) {
			return substr($text, 0, $length).'...';
		} 
		return $text;
	}

	static public function sizeString($text) {
		if($text !=null) {
			return strlen($text);
		} 
		return 0;
	}
}
