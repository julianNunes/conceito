<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model {
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'locations';
    protected $fillable = ['name','active', 'description', 'company_id'];

    //Acessors
    public function getFormattedActiveAttribute() {
        if(isset($this->active)) {
            return $this->active == 1 ? "Sim" : "Não";
        }
        return "";
    }

    public function getFormattedName() {
        return $this->id.' - '.$this->name;
    }
 
    public function location() {
        return $this->hasOne('App\Models\Location', 'id', 'location_id');
    }  
}
