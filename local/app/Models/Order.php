<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model {
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'orders';
    protected $fillable = ['activity', 'observation', 'date_open', 'date_start_order', 'date_close', 'urgent', 'status', 'commentary', 'type_os_id', 'property_id', 'provider_id', 'company_id', 'user_id'];

    // Relationships
    public function typeOs() {
        return $this->hasOne('App\Models\TypeOs', 'id', 'type_os_id');
    }

    public function property() {
        return $this->hasOne('App\Models\Property', 'id', 'property_id');
    }    

    public function provider() {
        return $this->hasOne('App\Models\Provider', 'id', 'provider_id');
    }     

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }     

    //Mutators
    public function getFormattedDateOpenAttribute() {
        if(!empty($this->attributes['date_open'])) {
            return date('d/m/Y', strtotime($this->attributes['date_open']) );
        }
        return "";
    }

    public function getFormattedDateStartOrderAttribute() {
        if(!empty($this->attributes['date_start_order'])) {
            return date('d/m/Y', strtotime($this->attributes['date_start_order']) );
        }
        return "";
    }

    public function getFormattedDateCloseAttribute() {
        if(!empty($this->attributes['date_close'])) {
            return date('d/m/Y h:i', strtotime($this->attributes['date_close']) );
        }
        return "";
    }

    public function getFormattedStatusAttribute() {
        if(!empty($this->status)) {
            if($this->status== 'pending')   { return 'Pendente'; }
            if($this->status== 'execution') { return 'Execução'; }
            if($this->status== 'done')      { return 'Encerrada'; }
            if($this->status== 'canceled')  { return 'Canceleda'; }
        }
        return "";
    }

	public function getFormattedUrgentAttribute() {
		return ($this['urgent'] == 1 ? 'Sim' : 'Não') ;
	}


    //Mutators
    public function setDateOpenAttribute($value) {
        $this->attributes['date_open'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value) : null; 
    }  

    public function setDateStartOrderAttribute($value) {
        $this->attributes['date_start_order'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value) : null; 
    }   

    public function setTotalAttribute($value) {
        $this->attributes['total'] = !empty($value) ? str_replace(",",".", str_replace(".","", $value)) : null;
    }
}
