<?php

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model {
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'property';
    protected $fillable = ['model', 'description', 'code','date_purchase', 'last_maintenance', 'nf', 'total', 'active', 'fabricator_id', 'family_id', 'provider_id', 'location_id', 'company_id'];

    // Relationships
    public function fabricator() {
        return $this->hasOne('App\Models\Fabricator', 'id', 'fabricator_id');
    }

    public function family() {
        return $this->hasOne('App\Models\Family', 'id', 'family_id');
    }     

    public function provider() {
        return $this->hasOne('App\Models\Provider', 'id', 'provider_id');
    }     

    public function location() {
        return $this->hasOne('App\Models\Location', 'id', 'location_id');
    }     

    //Mutators
    public function getFormattedTotalAttribute() {
        if($this->attributes['total'] > 0) {
           return number_format($this->attributes['total'], 2, ',', '.');
        } 
        return "0,00";
    }

    public function getFormattedDatePurchaseAttribute() {
        if(!empty($this->attributes['date_purchase'])) {
            return date('d/m/Y', strtotime($this->attributes['date_purchase']) );
        }
        return "";
    }

    public function getFormattedLastMaintenanceAttribute() {
        if(!empty($this->attributes['last_maintenance'])) {
            return date('d/m/Y', strtotime($this->attributes['last_maintenance']) );
        }
        return "";
    }
    
    public function getFormattedActiveAttribute() {
        if(isset($this->active)) {
            return $this->active == 1 ? "Sim" : "Não";
        }
        return "";
    }    

    //Mutators
    public function setDatePurchaseAttribute($value) {
        $this->attributes['date_purchase'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value) : null; 
    }  

    public function setLastMaintenanceAttribute($value) {
        $this->attributes['last_maintenance'] = !empty($value) ? Carbon::createFromFormat('d/m/Y', $value) : null; 
    }   

    public function setTotalAttribute($value) {
        $this->attributes['total'] = !empty($value) ? str_replace(",",".", str_replace(".","", $value)) : null;
    }

    //Methods
    public function getNameSelect() {
        return $this->model. ' ('.$this->description.')';
    }
}
