<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model 
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $table = 'providers';
	protected $fillable = ['name', 'type', 'cpf_cnpj',  'email', 'phone_number','active', 'company_id'];

    //Acessors
    public function getFormattedActiveAttribute() {
        if(isset($this->active)) {
            return $this->active == 1 ? "Sim" : "Não";
        }
        return "";
    }
    public function getFormattedTypeAttribute() {
        if(isset($this->type)) {
            return $this->active == 1 ? "Sim" : "Não";
        }
        return "";
    }

    //Mutators
    public function setTotalAttribute($value) {
        $this->attributes['total'] = !empty($value) ? str_replace(",",".", str_replace(".","", $value)) : null;
    }

    public function address() {
        return $this->morphOne('App\Models\Address', 'addressable');
    }   
    
}