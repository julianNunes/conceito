<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class State extends Model {

	protected $table = 'states';
	protected $fillable = ['initials', 'name'];
}
