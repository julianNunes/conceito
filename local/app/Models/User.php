<?php

namespace App\Models;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $fillable = ['name', 'nickname', 'administrator', 'email', 'password', 'company_id', 'active'];

	protected $hidden = [
		'password', 'remember_token',
	];

	//Mutators
	public function getFirstNameAttribute() {
		return explode(" ", $this['name'])[0];
	}

	public function getFormattedActiveAttribute() {
		return ($this['active'] == 1 ? 'Sim' : 'Não') ;
	}

	public function getFormattedAdministratorAttribute() {
		return ($this['active'] == 1 ? 'Sim' : 'Não') ;
	}

	public function getIsAdminAttribute() {
		return $this['administrator'] == 1 ? true: false;
	}

	// Relationships
    public function companies(){
        return $this->hasMany('App\Models\Company', 'user_id', 'id');
	}	 
	
    public function company(){
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
	}
	
	//Methodos
	public function getFormattedName() {
		return empty($this->nickname) ? $this->name : $this->nickname;
	}
}