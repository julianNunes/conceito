<?php

namespace App\Providers;

use App\Models\Gip;
use App\Models\Role;
use App\Models\RolePermissionUser;
use App\Models\User;
use App\Policies\GipPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use DB;
use Auth;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [         
       // Gip::class => GipPolicy::class,

    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */



    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate); 
    }
    
}
