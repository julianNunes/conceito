<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adresses', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['apartment', 'house', 'condominium', 'commercial']); //Edificio, Residencia, Condominio e Comercial
            $table->string('street', 120);
            $table->string('number', 120);
            $table->string('comp', 120);
            $table->string('district', 120);
            $table->string('zipcode', 120);
            $table->string('info', 2000);
            $table->string('address_cond', 120);
            $table->string('number_cond', 120);
            $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('city');   
            $table->string('addressable_type');
            $table->integer('addressable_id')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adresses');
    }
}
