<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('cnpj');
            $table->string('email');
            $table->string('phone_number');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        });   
        
		Schema::table('users', function ($table) {
			$table->unsignedInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('users');
        Schema::drop('companies');
    }
}
