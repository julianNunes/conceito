<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('activity');
            $table->text('observation')->nullable();
            $table->date('date_open')->nullable();
            $table->date('date_start_order')->nullable();
            $table->dateTime('date_close')->nullable();
            $table->boolean('urgent')->default(false);
            $table->enum('status', ['pending', 'execution', 'done', 'canceled'])->nullable();
            $table->text('commentary')->nullable();
            $table->integer('type_os_id')->unsigned();
            $table->foreign('type_os_id')->references('id')->on('type_os');
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('id')->on('property');
            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->nullableTimestamps();
            $table->softDeletes();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
