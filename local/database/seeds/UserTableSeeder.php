<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
    		[	'name' 		=> 'admin', 
    			'email' 	=> 'admin@outlook.com',
    			'password' 	=> bcrypt('654321'),
    			'active' 	=> 0,
    			'administrator'	=> 0,
    			// 'admin' => 1
    		]
 		]);

 		// $user_id = DB::table('users')->where('id', 1)->value('id');
   //  	if($user_id > 0) {

	 	// 	DB::table('users_access')->insert([
	  //   		[	'sunday_start' 			=> '00:00',
			// 		'sunday_end' 				=> '23:59',
			// 		'monday_start' 			=> '00:00',
			// 		'monday_end' 				=> '23:59',
			// 		'tuesday_start' 			=> '00:00',
			// 		'tuesday_end' 				=> '23:59',
			// 		'wednesday_start' 		=> '00:00',
			// 		'wednesday_end' 			=> '23:59',
			// 		'thursday_start' 			=> '00:00',
			// 		'thursday_end' 			=> '23:59',
			// 		'friday_start' 			=> '00:00',
			// 		'friday_end' 				=> '23:59',
			// 		'saturday_start' 			=> '00:00',
			// 		'saturday_end' 			=> '23:59',
			// 		'user_id' 					=> $user_id
	  //   		]
	 	// 	]);
   //  	}
   }
}
