@extends('dashboard.layout.admin_template2')
@section('header-extras')


@endsection

@section('page_title','Login')
@section('page_description','Dashboard')

@section('content')
<div class="login-box">
	{{--  <div class="login-logo">
		<img src="{{url('/images/sd.png')}}" style="  width: 360px;
  margin: 7% auto;">
	</div>  --}}

	<div class="login-box-body">
		<p class="login-box-msg">Digite seus dados para conectar-se</p>

		@if ($errors->has('email') || $errors->has('password'))
			<span class="help-block" style="text-align: center; color: red;">
				{{ $errors->has('email') ? $errors->first('email') : '' }}
				{{ $errors->has('password') ? $errors->first('password') : '' }}
			</span>
		@endif

		@if(Session::has('status'))
			<span class="help-block" style="text-align: center; color: red;">
				{!! session('status') !!}
			</span>
		@endif
		<form method="POST" action="{{ url('/login') }}">
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

			<div class="form-group has-feedback">
				<input type="email" name="email" class="form-control" placeholder="Email" required/>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>

			<div class="form-group has-feedback">
				<input type="password" name="password" class="form-control" placeholder="Senha">

				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			
			<div class="row">
				<div class="col-xs-7">
					<div style="margin-top: 8px;"> <a href="{{ url('/password/reset') }}">Esqueceu sua senha?</a></div>
				</div>
				<div class="col-xs-5">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
				</div>
			</div>
		</form>
		<hr style="border: 0; 
			height: 1px; 
			background-image: -webkit-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
			background-image: -moz-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
			background-image: -ms-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
			background-image: -o-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);"> 
		</hr>
	</div>
</div>

@endsection
@section('footer-extras')


@endsection