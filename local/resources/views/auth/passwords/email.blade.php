
@extends('dashboard.layout.admin_template2')
@section('header-extras')


@endsection

@section('page_title','Recuperar')
@section('page_description','Dashboard')

@section('content')
<div class="login-box">
	<div class="login-logo">
		<img src="{{url('/uploads/logo.png')}}">
	</div>

	<div class="login-box-body">
		<p class="login-box-msg">Digite seus dados para conectar-se</p>

		@if(Session::has('status'))
			<div class="box-body">
				<div class="alert alert-success alert-dismissible"> <h4><i class="icon fa fa-check"></i> Atenção!</h4><em> {!! session('status') !!}</em></div>
			</div>
		@endif
		
   	@if ($errors->has('email'))
			<span class="help-block" style="text-align: center; color: red;">
				{{ $errors->first('email') }}
			</span>
		@endif
		<form method="POST" role="form" action="{{ url('/password/email') }}">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			<div class="form-group has-feedback">
				<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required/>

				@if ($errors->has('email'))
				<span class="help-block" style="text-align: center; color: red;">
					{{ $errors->first('email') }}
				</span>
				@endif

				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>

			<div class="row">
				<div class="col-xs-7">
					<div style="margin-top: 8px;"> <a href="{{ url('/login') }}">Voltar</a></div>
				</div>
				<div class="col-xs-5">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Recuperar</button>
				</div>
			</div>
		</form>

		<hr style="border: 0; 
		height: 1px; 
		background-image: -webkit-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
		background-image: -moz-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
		background-image: -ms-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
		background-image: -o-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);"> 
		</hr>

	</div>
</div>

@endsection
@section('footer-extras')

@endsection
