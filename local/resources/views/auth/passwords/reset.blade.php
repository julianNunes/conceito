@extends('dashboard.layout.admin_template2')
@section('header-extras')


<!-- iCheck -->
<link href="{{ asset("/bower_components/admin-lte/plugins/iCheck/square/blue.css") }}" rel="stylesheet" type="text/css"/>
@endsection

@section('page_title','Recuperar')
@section('page_description','Dashboard')


@section('content')

<div class="login-box">
	<div class="login-logo">
		<img src="{{url('/uploads/logo.png')}}">
	</div>

	<div class="login-box-body">
		<p class="login-box-msg">Digite seus dados para conectar-se</p>
		
		<form method="POST" action="{{ url('/password/reset') }}">
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

			<div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
				<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required/>

				@if ($errors->has('email'))
				<span class="help-block" style="text-align: center; color: red;">
					{{ $errors->first('email') }}
				</span>
				@endif

				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>

			<div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
				<input type="password" class="form-control" placeholder="Senha" name="password">

				@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
				@endif
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>

			<div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
				<input type="password" class="form-control" placeholder="Confirmar Senha" name="password_confirmation">

				@if ($errors->has('password_confirmation'))
				<span class="help-block">
					<strong>{{ $errors->first('password_confirmation') }}</strong>
				</span>
				@endif
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>

			<div class="row">
				<div class="col-xs-7">
					<div style="margin-top: 8px;"> <a href="{{ url('/login') }}">Voltar</a></div>
				</div>
				<div class="col-xs-5">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Resetar Senha</button>
				</div>
			</div>
		</form>
		<hr style="border: 0; 
		height: 1px; 
		background-image: -webkit-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
		background-image: -moz-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
		background-image: -ms-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);
		background-image: -o-linear-gradient(left, #f0f0f0, #ccc, #f0f0f0);"> 
	</hr>

</div>
</div>

@endsection

@section('footer-extras')

@endsection
