@if(isset($account))
<div class="row row-account">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Banco</label>
				<input type="text" name="accounts[{{ $index }}][bank]" class="form-control bank-account account" value="{{ $account->bank }}" placeholder="Banco" />
			</div>  
		</div> 		

		<div class="col-md-5">
			<div class="form-group">
				<label class="control-label">Agência</label>
				<input type="text" name="accounts[{{ $index }}][agency]" class="form-control agency-account account" value="{{ $account->agency }}" placeholder="Agência" />
			</div>  
		</div> 			

		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn btn-danger remove-account form-control account"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Conta</label>
				<input type="text" name="accounts[{{ $index }}][account]" class="form-control account-account account" value="{{ $account->account }}" placeholder="Conta" />
			</div>  
		</div> 

		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Tipo de Conta</label>
				<select class="form-control select2 account_type account" name="accounts[{{ $index }}][account_type]"  style="width: 100% !important">
					<option value="">Selecione</option>
					<option value="current" @if($account->account_type == 'current') selected @endif>Corrente</option>
					<option value="saving" @if($account->account_type == 'saving') selected @endif>Poupança</option>
					<option value="work" @if($account->account_type == 'work') selected @endif>Trabalho</option>
				</select>    
			</div>   
		</div>
		<div class="col-md-12">
			<div class="box-header with-border" style="margin-bottom: 20px;"></div>
		</div>
	</div>
@else
	<div class="row row-account">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Banco</label>
				<input type="text" name="accounts[{{ $index }}][bank]" class="form-control bank-account account" placeholder="Banco" />
			</div>  
		</div> 		

		<div class="col-md-5">
			<div class="form-group">
				<label class="control-label">Agência</label>
				<input type="text" name="accounts[{{ $index }}][agency]" class="form-control agency-account account" placeholder="Agência" />
			</div>  
		</div> 			

		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn btn-danger remove-account form-control account"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Conta</label>
				<input type="text" name="accounts[{{ $index }}][account]" class="form-control account-account account" placeholder="Conta" />
			</div>  
		</div> 

		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Tipo de Conta</label>
				<select class="form-control select2 account_type account" name="accounts[{{ $index }}][account_type]"  style="width: 100% !important">
					<option value="">Selecione</option>
					<option value="current">Corrente</option>
					<option value="saving">Poupança</option>
					<option value="work">Trabalho</option>
				</select>    
			</div>   
		</div>
		<div class="col-md-12">
			<div class="box-header with-border" style="margin-bottom: 20px;"></div>
		</div>
	</div>
@endif
