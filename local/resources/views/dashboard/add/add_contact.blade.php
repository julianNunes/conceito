@if(isset($contact))

	<div class="row-contact">
		<input type="hidden" class="index" value="{{$index}}" >
		<div class="row row-initial-data">
			<!-- Um nome por bloco de contato adicional -->
			<div class="col-md-11">
				<div class="form-group">
					<label class="control-label">Nome</label>				
					<input type="text" name="contacts[{{ $index }}][name]" class="form-control name" placeholder="Nome" value="{{ $contact->name }}" />			
				</div>  
			</div>		

			<div class="col-md-1"> 
				<div class="form-group">
					<label class="control-label" style="color: #fff;">.</label>
					<a type="button" class="btn  btn-danger remove-contact form-control"><i class="fa fa-minus fa-fw"></i></a>             
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Empresa</label>				
					<input type="text" name="contacts[{{ $index }}][company]" class="form-control" placeholder="Empresa" value="{{ $contact->company }}" />			
				</div>  
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Cargo</label>				
					<input type="text" name="contacts[{{ $index }}][office]" class="form-control" placeholder="Cargo" value="{{ $contact->office }}" />			
				</div>  
			</div>			

		</div>
		
		<!-- Vários telefones -->
		<div class="row">
			<div class="col-md-2 col-xs-12 col-sm-12">
				<h4 class="box-title">Telefones</h4>
			</div>
			<div class="col-md-1 col-xs-12 col-sm-12">
				<button type="button" class="btn btn-success form-control add_contact_phone"><i class="fa fa-plus fa-fw"></i></button>
			</div>
		</div>

		<div class="contact-phone">
			@if(count($contact->phones) > 0)
				@foreach($contact->phones as $index_phone => $phone)
					@include('dashboard.add.add_contact_phone', ['index' => $index, 'index_phone' => $index_phone, 'phone' => $phone])	
				@endforeach	
			@endif	
		</div>

		<!-- Vários e-mails -->
		<div class="row">
			<div class="col-md-2 col-xs-12 col-sm-12">
				<h4 class="box-title">E-mails</h4>
			</div>
			<div class="col-md-1 col-xs-12 col-sm-12">
				<button type="button" class="btn btn-success form-control add_contact_email"><i class="fa fa-plus fa-fw"></i></button>
			</div>
		</div>

		<div class="contact-email">	
			@if(count($contact->emails) > 0)
				@foreach($contact->emails as $index_email => $email)
					@include('dashboard.add.add_contact_email', ['index' => $index, 'index_email' => $index_email, 'email' => $email])
				@endforeach	
			@endif	
			
		</div>	

		<div class="col-md-12">
			<div class="box-header with-border" style="margin-bottom: 20px;"></div>
		</div>

	</div>

@else
	<div class="row-contact">
		<input type="hidden" class="index" value="{{$index}}" >
		<div class="row">
			<!-- Um nome por bloco de contato adicional -->
			<div class="col-md-11">
				<div class="form-group">
					<label class="control-label">Nome</label>				
					<input type="text" name="contacts[{{ $index }}][name]" class="form-control name" placeholder="Nome" />			
				</div>  
			</div>

			<div class="col-md-1"> 
				<div class="form-group">
					<label class="control-label" style="color: #fff;">.</label>
					<a type="button" class="btn  btn-danger remove-contact form-control"><i class="fa fa-minus fa-fw"></i></a>             
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Empresa</label>				
					<input type="text" name="contacts[{{ $index }}][company]" class="form-control" placeholder="Empresa" />			
				</div>  
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Cargo</label>				
					<input type="text" name="contacts[{{ $index }}][office]" class="form-control" placeholder="Cargo" />			
				</div>  
			</div>				
		</div>
		
		<!-- Vários telefones -->
		<div class="row">
			<div class="col-md-2 col-xs-12 col-sm-12">
				<h4 class="box-title">Telefones</h4>
			</div>
			<div class="col-md-1 col-xs-12 col-sm-12">
				<button type="button" class="btn btn-success form-control add_contact_phone"><i class="fa fa-plus fa-fw"></i></button>
			</div>
		</div>

		<div class="contact-phone">
			@include('dashboard.add.add_contact_phone', ['index' => $index, 'index_phone' => $index_phone])		
		</div>				

		<!-- Vários e-mails -->
		<div class="row">
			<div class="col-md-2 col-xs-12 col-sm-12">
				<h4 class="box-title">E-mails</h4>
			</div>
			<div class="col-md-1 col-xs-12 col-sm-12">
				<button type="button" class="btn btn-success form-control add_contact_email"><i class="fa fa-plus fa-fw"></i></button>
			</div>
		</div>

		<div class="contact-email">		
			@include('dashboard.add.add_contact_email', ['index' => $index, 'index_email' => $index_email])
		</div>	

		<div class="col-md-12">
			<div class="box-header with-border" style="margin-bottom: 20px;"></div>
		</div>
	</div>
@endif	