@if(isset($email))

	<div class="row-contact-email row">
		<input type="hidden" class="index" value="{{$index_email}}" >
				
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">E-mail</label>
				<input type="text" name="contacts[{{ $index }}][email][{{ $index_email }}][email]" class="form-control email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Digite seu E-mail" value="{{ $email->email }}" />
			</div>  
		</div>

		<div class="col-md-5"> 
			<div class="form-group">
				<label class="control-label">Tipo do E-mail</label>
				<select name="contacts[{{ $index }}][email][{{ $index_email }}][type]" class="form-control type-email select2" style="width: 100% !important" >					
					<option value="personal" @if($email->type == 'personal') selected @endif>Pessoal</option>
					<option value="commercial" @if($email->type == 'commercial') selected @endif>Comercial</option>
					<option value="others" @if($email->type == 'others') selected @endif>Outros</option>
				</select>              
			</div>
		</div>

		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn btn-danger remove-contact-email form-control"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>	
	</div>
@else

	<div class="row-contact-email row">
		<input type="hidden" class="index" value="{{$index_email}}" >
				
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">E-mail</label>
				<input type="text" name="contacts[{{ $index }}][email][{{ $index_email }}][email]" class="form-control email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Digite seu E-mail" />
			</div>  
		</div>
		<div class="col-md-5"> 
			<div class="form-group">
				<label class="control-label">Tipo do E-mail</label>
				<select name="contacts[{{ $index }}][email][{{ $index_email }}][type]" class="form-control type-email select2" style="width: 100% !important" >
					<option value="">Selecione</option>
					<option value="personal">Pessoal</option>
					<option value="commercial">Comercial</option>
					<option value="others">Outros</option>
				</select>              
			</div>
		</div>
		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn btn-danger remove-contact-email form-control"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>	
	</div>	
@endif	