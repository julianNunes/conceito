@if(isset($email))
	<div class="row row-email">
		<div class="col-md-9">
			<div class="form-group">
				<label class="control-label">E-mail</label>				
				<input type="text" name="emails[{{ $index }}][email]" class="form-control email-email"  value="{{ $email->email }}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Digite seu email" />
			</div>  
		</div>
		<div class="col-md-2"> 
			<div class="form-group">
				<label class="control-label">Tipo</label>
				<select name="emails[{{ $index }}][type]" class="form-control type-email select2" style="width: 100% !important" >
					<option value="">Selecione</option>
					<option value="personal" @if($email->type == 'personal') selected @endif>Pessoal</option>
					<option value="commercial" @if($email->type == 'commercial') selected @endif>Comercial</option>
					<option value="others" @if($email->type == 'others') selected @endif>Outros</option>
				</select>              
			</div>
		</div>
		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn  btn-danger remove-email form-control"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>
	</div>
@else
	<div class="row row-email">
		<div class="col-md-9">
			<div class="form-group">
				<label class="control-label">E-mail</label>
				<input type="text" name="emails[{{ $index }}][email]" class="form-control email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Digite seu email" />
			</div>  
		</div>
		<div class="col-md-2"> 
			<div class="form-group">
				<label class="control-label">Tipo do E-mail</label>
				<select name="emails[{{ $index }}][type]" class="form-control type-email select2" style="width: 100% !important" >
					<option value="">Selecione</option>
					<option value="personal">Pessoal</option>
					<option value="commercial">Comercial</option>
					<option value="others">Outros</option>
				</select>              
			</div>
		</div>
		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn  btn-danger remove-email form-control"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>
	</div>
@endif
