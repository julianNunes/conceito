@if(isset($file))
	<input type="hidden" class="id" value="{{ $file->id }}" readonly>
	<div class="row row-file">
		<div class="col-md-6">
			<div class="form-group"> 
				<label class="control-label">Nome</label> 
				<input type="text" name="files_name[{{ $index }}]" class="form-control" placeholder="Nome" value="{{ $file->name }}">
			</div> 
		</div> 	
		<div class="col-md-5">
			<div class="form-group"> 
				<label class="control-label">Arquivo</label> 
				<input type="text" class="form-control" value="{{ $file->file }}" readonly>
			</div> 
		</div> 
		<div class="col-md-1">
			<div class="form-group"> 
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn btn-danger remove-file form-control"><i class="fa fa-minus fa-fw"></i></a>
			</div>
		</div>
	</div>
@else
	<div class="row row-file">
		<div class="col-md-6">
			<div class="form-group"> 
				<label class="control-label">Nome</label> 
				<input type="text" name="files_name[{{ $index }}]" class="form-control file-name" placeholder="Nome">
			</div> 
		</div> 	
		<div class="col-md-5">
			<div class="form-group"> 
				<label class="control-label">Arquivo (2MB)</label> 
				<input type="file" name="files[{{ $index }}]" class="form-control file" accept=".jpg, .png, .jpeg, .doc, .docx, .pdf, .txt, .xls, .xlsx">
			</div> 
		</div> 
		<div class="col-md-1">
			<div class="form-group"> 
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn  btn-danger remove-file form-control"><i class="fa fa-minus fa-fw"></i></a>
			</div>
		</div>
	</div>
@endif
