@if(isset($phone))
	<div class="row row-phone">
		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label">Número</label>
				<input type="text" name="phones[{{ $index }}][number]" class="form-control number-phone" value="{{ $phone->number }}" placeholder="(00) 00000-0000" />
			</div>  
		</div>
		<div class="col-md-7">
			<div class="form-group">
				<label class="control-label">Nome</label>
				<input type="text" name="phones[{{ $index }}][name]" class="form-control name-phone" value="{{ $phone->name }}" placeholder="Nome do contato"/>
			</div>  
		</div>
		<div class="col-md-2"> 
			<div class="form-group">
				<label class="control-label">Tipo</label>
				<select name="phones[{{ $index }}][type]" class="form-control type-phone select2" style="width: 100% !important">
					<option value="">Selecione</option>
					<option value="residential" @if($phone->type == 'residential') selected @endif>Residencial</option>
					<option value="commercial" @if($phone->type == 'commercial') selected @endif>Comercial</option>
					<option value="cellphone" @if($phone->type == 'cellphone') selected @endif>Celular</option>
				</select>              
			</div>	
		</div>
		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn  btn-danger remove-phone form-control"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>	
	</div>
@else
	<div class="row row-phone">
		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label">Número</label>
				<input type="text" name="phones[{{ $index }}][number]" class="form-control number-phone" placeholder="(00) 00000-0000" />
			</div>  
		</div>
		<div class="col-md-7">
			<div class="form-group">
				<label class="control-label">Nome</label>
				<input type="text" name="phones[{{ $index }}][name]" class="form-control name-phone"  placeholder="Nome do contato"/>
			</div>  
		</div>
		<div class="col-md-2"> 
			<div class="form-group">
				<label class="control-label">Tipo</label>
				<select name="phones[{{ $index }}][type]" class="form-control type-phone select2" style="width: 100% !important">
					<option value="">Selecione</option>
					<option value="residential">Residencial</option>
					<option value="commercial">Comercial</option>
					<option value="cellphone">Celular</option>
				</select>              
			</div>	
		</div>	
		<div class="col-md-1"> 
			<div class="form-group">
				<label class="control-label" style="color: #fff;">.</label>
				<a type="button" class="btn  btn-danger remove-phone form-control"><i class="fa fa-minus fa-fw"></i></a>             
			</div>
		</div>
	</div>
@endif


