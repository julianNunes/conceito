@extends('dashboard.layout.admin_template')
@section('header-extras')
<script src="{{ asset("/bower_components/admin-lte/dist/js/validadorcpf.js")}}"></script>
<script src="{{ asset("/js/validaIE.js")}}"></script>
@endsection

@section('page_title','Empresa')
@section('page_description','Editar')

@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

@if(!isset($show))
	<form method="post" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
		<input type="hidden" name="_token" value="{{csrf_token()}}">	
@else
	<form method="get" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
@endif	

	{{-- Dados da Companhia --}}
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title">Dados Iniciais</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
			</div>
		</div>

		<div class="box-body">
			<div class="row">
				{{--  Nome  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Nome Fantasia   @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif  </label>
						<input type="text" @if(!isset($show)) name="name"  @endif class="form-control" placeholder="Nome" @if(isset($company)) value="{{ $company->name }}" @endif   @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>  
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">CNPJ  @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" id="cnpj" @if(!isset($show)) name="cnpj"  @endif class="form-control"  @if(isset($company)) value="{{ $company->cnpj }}" @endif   @if(isset($show)) readonly @else required @endif />
					</div>   
				</div>
				{{--  Nome Fantasia  --}}
				{{--  <div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Razão Social@if(!isset($show)) <span style="color: red;"><sup>&bull;</sup></label>
						<input type="text" name="company_name" class="form-control  text-uppercase" placeholder="Nome " @if(isset($company)) value="{{ $company->company_name }}" @endif  @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>  --}}
				{{--  Email  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">E-mail  @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" @if(!isset($show)) name="email"  @endif class="form-control email " pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Digite seu email" @if(isset($company)) value="{{ $company->email }}" @endif   @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>	
				{{--  Numero de Telefone  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Número de Telefone  @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" id='number-phone' @if(!isset($show)) name="phone_number"  @endif class="form-control" placeholder="(00) 00000-0000" @if(isset($company)) value="{{ $company->phone_number }}" @endif   @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>
				{{--  Ativo  --}}
				<div class="col-md-6">					
					<div class="form-group">
						<label class="control-label">Ativo  @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input type="text" class="form-control" value="{{ $company->formatted_active }}" readonly />
						@else
							<select @if(!isset($show)) name="active"  @endif class="form-control" style="width: 100%"  @if(isset($show)) readonly @else required @endif>
								<option value="">Selecione</option>
								<option value="0" @if(isset($company) && $company->active == '0') selected @endif>Não</option>
								<option value="1" @if((isset($company) && $company->active == '1')  || !isset($company)) selected @endif>Sim</option>		
							</select>
						@endif
					</div> 
				</div> 
			</div>
		</div>
	</div>	

@if(!isset($show))
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" onclick="abrir()" class="btn btn-sm btn-success"><i class="fa fa-save fa-fw"></i> Salvar</button>
			</div>
		</div>
	</form>
@else
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">		
			<button type="submit" onclick="abrir()" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i> Editar</button>
			</div>
		</div>
	</form>
@endif

@endsection

@section('footer-extras')

{{-- Mask --}}
<script src="{{ asset ("/bower_components/jquery-mask-plugin/dist/jquery.mask.js") }}" type="text/javascript"></script>
{{-- FastClick --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>

<script>
	$(document).ready(function() {
		
		//Inicializa FastClick
		FastClick.attach(document.body);
		

		//Máscara de telefone direto da documentação do mask plugin
		var maskPhone = function (val) {
		  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		phone_typing = {
		  onKeyPress: function(val, e, field, options) {
		      field.mask(maskPhone.apply({}, arguments), options);
		    }
		};

		//Set da máscara do telefone
		$('#number-phone').mask(maskPhone, phone_typing);		

		//Responsavel por validar o CNPJ
		function validaCNPJ(cnpj) {
			cnpj = cnpj.replace(/[^\d]+/g,'');

			if(cnpj == '') return false;

			if (cnpj.length != 14)
				return false;

			// Elimina CNPJs invalidos conhecidos
			if (cnpj == "00000000000000" || 
				cnpj == "11111111111111" || 
				cnpj == "22222222222222" || 
				cnpj == "33333333333333" || 
				cnpj == "44444444444444" || 
				cnpj == "55555555555555" || 
				cnpj == "66666666666666" || 
				cnpj == "77777777777777" || 
				cnpj == "88888888888888" || 
				cnpj == "99999999999999")
				return false;

			// Valida DVs
			tamanho = cnpj.length - 2
			numeros = cnpj.substring(0,tamanho);
			digitos = cnpj.substring(tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0))
				return false;

			tamanho = tamanho + 1;
			numeros = cnpj.substring(0,tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1))
				return false;

			return true;
		}


		//Metodo para chamar a validação do CNPJ
		function vCNPJ(evt){
			evt.preventDefault();
			if(validaCNPJ($(this).val())) {
				this.setCustomValidity('');
			} else {
				this.setCustomValidity('CNPJ inválido!');
			}
		}

		//Chamada para Eventos
		$('#cnpj').mask('00.000.000/0000-00', { placeholder: '__.___.___/____-__'});
		$('#cnpj').on('input', vCNPJ);

	}); 
</script>

@endsection