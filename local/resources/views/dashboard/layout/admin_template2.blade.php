<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
	@include('dashboard.layout.header-scripts')

</head>
<body class="hold-transition login-page">

	<section class="content">
		<!-- Your Page Content Here -->
		@yield('content')
	</section><!-- /.content -->
	
	@yield('footer-extras')
</body>
</html>