<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
 @include('dashboard.layout-admin.header-scripts')
 @yield('header-extras')
</head>
<body class="skin-blue sidebar-mini hold-transition login-page">


    
  

 
      

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    



@include('dashboard.layout-admin.footer-scripts')

@yield('footer-extras')
</body>
</html>