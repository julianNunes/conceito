@extends('dashboard.layout.admin_template')

@section('header-extras')
@endsection

@section('page_title',$page_title)
@section('page_description','Excluir')

@section('content')

<div id="div-main">
	<form method="post" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Excluir Registro</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
				</div>
			</div>

			<div class="box-body">
				<div class="row">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					@if(count($fields) > 0)
						@foreach($fields as $item)
							<div class="col-md-{{$item['col']}}">
								<div class="form-group">
									<label class="control-label">{{$item['title']}}</label>
									<input type="text" class="form-control" value="{{$item['value']}}" readonly/> 
								</div>             
							</div>
						@endforeach
					@endif
				</div>
			</div>
		</div>

		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" onclick="abrir()" class="btn btn-sm btn-danger"><i class="fa fa-close fa-fw"></i> Excluir</button>
			</div>
		</div>
	</form>
</div>

@endsection

@section('footer-extras')
@endsection