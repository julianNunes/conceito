<!-- jQuery 2.2.0 -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.2.0.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>

<script>
	//Abre todas as box da tela antes de enviar o form
	function abrir() {

      $('.box-body', '#div-main').css('display', 'block');
      $('.bt-plus', '#div-main').removeClass('fa-plus').addClass('fa-minus bt-plus');
      $('.box', '#div-main').removeClass('collapsed-box');   
   }

   //Desabilita o botão que envia dados do form
	$('form').submit(function(evt){		
	   $(this).find('button[type=submit]').prop('disabled', true);	    
	});

	//Alteração do filtro :contains do Jquery. Aqui esta sendo feito com que a comparação seja sempre CASE INSENSITIVE.
	$.expr[":"].contains = $.expr.createPseudo(function(arg) {
		return function( elem ) {
			return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
		};
	});
</script>
