<meta charset="UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('page_title')</title>

<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- Bootstrap 3.3.2 -->
<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
<!-- Font Awesome Icons -->
<link href="{{ asset("/bower_components/admin-lte/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />

{{-- <link href="{{ asset("/bower_components/admin-lte/plugins/select2/select2.min.css") }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset("/css/select2.min.css") }}" rel="stylesheet" type="text/css" />

<!-- DataTables -->
<link href="{{ asset("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css") }}" rel="stylesheet" type="text/css" />

<!-- Theme style -->
<link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />

<link href="{{ asset("/bower_components/admin-lte/dist/css/skins/_all-skins.min.css") }}" rel="stylesheet" type="text/css" />

<link href="{{ asset("/css/index.css") }}" rel="stylesheet" type="text/css" />