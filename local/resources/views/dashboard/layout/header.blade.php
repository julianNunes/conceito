<!-- Main Header -->
<header class="main-header">

	<!-- Logo -->
	<a href="{{url('/')}}/dashboard" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini">CT</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg">Conceito</span>
	</a>

	<!-- Header Navbar -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown notifications-menu ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						@if(isset(Auth::user()->company))
							<span class="hidden-xs"> {{ Auth::user()->company->name }}</span>
						@else
							<span class="hidden-xs">
								<bold>Nenhuma Empresa Selecionada</bold>
							</span>
						@endif
					</a>
					<ul class="dropdown-menu">
						{{--  <li class="user-header" style='height: 80px!important;'>
							<p>
								{{ Auth::user()->name }}
								<small> {{ Auth::user()->nickname }}</small>
							</p>
						</li>  --}}

						<li>
							<!-- Inner Menu: contains the notifications -->
							<ul class="menu">
								<li>
									<!-- start notification -->
									@if(count(Auth::user()->companies) > 0)
										@foreach(Auth::user()->companies as $company) 
											@if(!isset( Auth::user()->company) || (isset( Auth::user()->company) &&  Auth::user()->company->id != $company->id))
												<a href="{{ route('company.selection', $company->id) }}"> <i class="fa fa-users text-aqua"></i> {{ $company->name }} </a>
											@endif
										@endforeach 
									@else 
										<a href="#"> <i class="fa fa-users text-aqua"></i> Nenhuma Empresa Cadastrada</a>
									@endif
								</li>
								<!-- end notification -->
							</ul>
						</li>
					</ul>
				</li>
			</ul>

			<ul class="nav navbar-nav">

				<!-- User Account Menu -->
				<li class="dropdown user user-menu">
					<!-- Menu Toggle Button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!-- The user image in the navbar-->
						<img src="{{ asset('/images/user-icon.png') }}" class="user-image" alt="User Image">
						<!-- hidden-xs hides the username on small devices so only the image appears. -->
						<span class="hidden-xs"> {{ Auth::user()->first_name }}</span>
					</a>
					<ul class="dropdown-menu">
						<!-- The user image in the menu -->
						<li class="user-header" style='height: 80px!important;'>
							<p>
								{{ Auth::user()->name }}
								<small> {{ Auth::user()->nickname }}</small>
							</p>
						</li>

						<li class="user-footer">
							<div class="pull-left">
								<a href="{{ route('admin.config') }}" class="btn btn-default btn-flat">Perfil</a>
							</div>

							<div class="pull-right">
								<a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sair</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>