<!DOCTYPE html>

<html>
<head>
	@include('dashboard.layout.header-scripts')
	@yield('header-extras')
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<!-- Header -->
		@include('dashboard.layout.header') 

		<!-- Sidebar -->
		@include('dashboard.layout.sidebar')

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper skin-green">
			<!-- Main content -->
			<section class="content">
				<!-- Your Page Content Here -->
				@yield('content')
			</section><!-- /.content -->
		</div><!-- /.content-wrapper -->

		{{-- Dialogs --}}
		@yield('modal_dialogs')
		<!-- Footer -->
		@include('dashboard.layout.footer')

	</div><!-- ./wrapper -->

	<!-- REQUIRED JS SCRIPTS -->
	@include('dashboard.layout.footer-scripts')

	@yield('footer-extras')
</body>
</html>