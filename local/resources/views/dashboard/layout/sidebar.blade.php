<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar ">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar brand-success">

		<!-- Sidebar Menu -->
		<ul class="sidebar-menu">
		<div style="width: 100%; height: 1px; border-top: 1px solid #1A2226;"></div>

		<li class="{{ Ekko::isActiveRoute('home.index') }}"><a href="{{ route('home.index') }}"><i class="fa fa-home"></i><span>Home</span></a></li>
		<li class="{{ Ekko::isActiveRoute('schedule.index') }}"><a href="{{ route('schedule.index') }}"><i class="fa fa-calendar"></i><span>Agenda</span></a></li>
		
		@if(Auth::user()->administrator == 1 )
			@if(Auth::user()->companies->count()>0)

				@if(isset(Auth::user()->company))
					<li class="{{ Ekko::isActiveRoute('order.index', 'order.create', 'order.edit', 'order.confirm-cancel', 'order.confirm-done', 'order.delete') }}"><a href="{{ route('order.index') }}"><i class="fa fa-book"></i><span>Ordem de Serviço</span></a></li>
					
					<li class="{{ Ekko::areActiveRoutes(['company.index','company.edit','company.create','company.delete','company.show','type-os.index','type-os.edit','type-os.create','type-os.delete','type-os.show', 'property.index','property.edit','property.create','property.delete','property.show', 'fabricator.index','fabricator.edit','fabricator.create','fabricator.delete','fabricator.show', 'family.index','family.edit','family.create','family.delete','family.show', 'provider.index','provider.edit','provider.create','provider.delete','provider.show', 'location.index','location.edit','location.create','location.delete','location.show', 'company.index','company.edit','company.create','company.delete','company.show', 'user.index','user.edit','user.create','user.delete','user.show'])}} treeview">
						<a href="#"><i class="fa fa-plus"></i> <span>Cadastros</span> <i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li class="{{ Ekko::areActiveRoutes(['type-os.index','type-os.edit','type-os.create','type-os.delete','type-os.show'])}} treeview"><a href="{{ route('type-os.index') }}"><span>Tipos de OS</span></a></li>
							<li class="{{ Ekko::areActiveRoutes(['property.index','property.edit','property.create','property.delete','property.show'])}} treeview"><a href="{{ route('property.index') }}"><span>Bens</span></a></li>
							<li class="{{ Ekko::areActiveRoutes(['fabricator.index','fabricator.edit','fabricator.create','fabricator.delete','fabricator.show'])}} treeview"><a href="{{ route('fabricator.index') }}"><span>Fabricantes</span></a></li>
							<li class="{{ Ekko::areActiveRoutes(['family.index','family.edit','family.create','family.delete','family.show'])}} treeview"><a href="{{ route('family.index') }}"><span>Famílias</span></a></li>
							<li class="{{ Ekko::areActiveRoutes(['provider.index','provider.edit','provider.create','provider.delete','provider.show'])}} treeview"><a href="{{ route('provider.index') }}"><span>Fornecedores</span></a></li>
							<li class="{{ Ekko::areActiveRoutes(['location.index','location.edit','location.create','location.delete','location.show'])}} treeview"><a href="{{ route('location.index') }}"><span>Localizações</span></a></li>
							<li class="{{ Ekko::areActiveRoutes(['company.index','company.edit','company.create','company.delete','company.show'])}} treeview"><a href="{{ route('company.index') }}"><span>Empresas</span></a></li>
							<li class="{{ Ekko::areActiveRoutes(['user.index','user.edit','user.create','user.delete','user.show'])}} treeview"><a href="{{ route('user.index') }}"><span>Usuário</span></a></li>
						</ul>
					</li>
				@else
					<li class="{{ Ekko::areActiveRoutes(['company.index','company.edit','company.create','company.delete','company.show'])}} treeview">
						<a href="#"><i class="fa fa-users"></i> <span>Cadastros</span> <i class="fa fa-angle-left pull-right"></i></a>
						<ul class="treeview-menu">
							<li class="{{ Ekko::areActiveRoutes(['company.index','company.edit','company.create','company.delete','company.show'])}} treeview"><a href="{{ route('company.index') }}"><span>Empresas</span></a></li>
						</ul>
					</li>		
				@endif
			@else
				<li class="{{ Ekko::areActiveRoutes(['company.index','company.edit','company.create','company.delete','company.show'])}} treeview">
					<a href="#"><i class="fa fa-users"></i> <span>Cadastros</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li class="{{ Ekko::areActiveRoutes(['company.index','company.edit','company.create','company.delete','company.show'])}} treeview"><a href="{{ route('company.index') }}"><span>Empresas</span></a></li>
					</ul>
				</li>		
			@endif
		@else
			<li class="{{ Ekko::isActiveRoute('order.index', 'order.create', 'order.edit', 'order.confirm-cancel', 'order.confirm-done', 'order.delete') }}"><a href="{{ route('order.index') }}"><i class="fa fa-book"></i><span>Ordem de Serviço</span></a></li>
		@endif

		<li><a href="{{ url('/logout') }}"><i class="fa fa-power-off"></i> <span>Sair</span></a></li>
	</ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>