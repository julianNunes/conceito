@if(Session::has('success_message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <i class="icon fa fa-check"></i>
        <em> {!! session('success_message') !!}</em>
    </div>
@endif