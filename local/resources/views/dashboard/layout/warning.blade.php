@if(Session::has('warning_message'))
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <i class="icon fa fa-warning"></i>
        <em> {!! session('warning_message') !!}</em>
    </div>		
@endif