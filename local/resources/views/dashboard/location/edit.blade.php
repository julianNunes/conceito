@extends('dashboard.layout.admin_template')
@section('header-extras')
@endsection

@section('page_title','Localiação')
@section('page_description', $page_description)

@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

<div id="div-main">
@if(!isset($show))
	<form method="post" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
		<input type="hidden" name="_token" value="{{csrf_token()}}">	
@else
	<form method="get" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
@endif		
		<div class="box box-default">
			{{-- Dados Iniciais --}}
			<div class="box-header with-border">
				<h3 class="box-title">Dados Cadastrais</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
				</div>
			</div>

			<div class="box-body">
				<div class="row">
					{{-- Nome --}}	
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Nome  @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif  </label>
							<input type="text" class="form-control" @if(!isset($show)) name="name" @endif @if(isset($location)) value="{{ $location->name }}" @endif @if(isset($show)) readonly @else required @endif/>
						</div>  
					</div>
					{{-- Descrição --}}	
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Descrição </label>
							<input type="text" class="form-control" @if(!isset($show)) name="description" @endif @if(isset($location)) value="{{ $location->description }}" @endif @if(isset($show)) readonly @endif/>
						</div>  
					</div>
					{{--  Active  --}}
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Ativo  @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif  </label>
							@if(isset($show)) 
								<input type="text" class="form-control" value="{{ $location->formatted_active }}" readonly />
							@else
								<select @if(!isset($show)) name="active" @endif class="form-control" required >
									<option value="">Selecione</option>
									<option value="0" @if(isset($location) && $location->active == '0') selected @endif>Não</option>
									<option value="1" @if((isset($location) && $location->active == '1' )  || !isset($location)) selected @endif>Sim</option>
								</select>
							@endif
						</div> 
					</div>	
				</div>	
			</div>	
		</div>			

@if(!isset($show))
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" onclick="abrir()" class="btn btn-sm btn-success"><i class="fa fa-save fa-fw"></i> Salvar</button>
			</div>
		</div>
	</form>
@else
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">		
			<button type="submit" onclick="abrir()" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i> Editar</button>
			</div>
		</div>
	</form>
@endif
</div>

@endsection

@section('footer-extras')

<!-- FastClick -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>

<script>
	$(document).ready(function() {
		//Inicializa FastClick
		FastClick.attach(document.body);	
	}); 
</script>

@endsection