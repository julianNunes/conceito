@extends('dashboard.layout.admin_template')
@section('header-extras')
<!-- date-picker -->
<link href="{{ asset ("/css/datepicker/bootstrap-datepicker3.standalone.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_title','Ordem de Serviço')
@section('page_description', $page_description)

@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

@if(!isset($show) || isset($done) || isset($cancel))
	<form method="post" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
		<input type="hidden" name="_token" value="{{csrf_token()}}">	
@else
	<form method="get" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
@endif	

	{{-- Dados da Iniciais --}}
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title">Dados Iniciais</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
			</div>
		</div>

		<div class="box-body">
			<div class="row">
				{{--  Tipo da OS  --}}
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label">Tipo da OS @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $order->typeOs->name }}" readonly/>
						@else
							<select  @if(!isset($show)) name="type_os_id" @endif class="form-control select2" tyle="width: 100%" required >
								<option value="">Selecione</option>
								@if(isset($order)) 
									<option value="{{$order->typeOs->id}}" selected>{{$order->typeOs->name}}</option>
								@endif										
								@if(count($types) > 0) 
									@foreach($types as $type)
										<option value="{{$type->id}}">{{$type->name}}</option>
									@endforeach
								@endif										
							</select> 
						@endif 
					</div>   
				</div>
				{{--  Data de Abertur  --}}
				<div class="col-md-3">
					<div class="form-group">
						<label id="label-name" class="control-label">Data de Abertura @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datepicker" @if(!isset($show)) name="date_open" @endif data-mask="00/00/0000"  placeholder="__/__/____" data-mask-selectonfocus="true"  data-mask-clearifnotmatch="true"  @if(isset($order)) value="{{ $order->formatted_date_open }}" @endif  @if(isset($show)) readonly @else required @endif />
						</div>
					</div>   
				</div>
				{{--  Data de Inicio da OS  --}}
				<div class="col-md-3">
					<div class="form-group">
						<label id="label-name" class="control-label">Data de Inicio da OS </label>
						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datepicker" @if(!isset($show)) name="date_start_order" @endif data-mask="00/00/0000"  placeholder="__/__/____" data-mask-selectonfocus="true"  data-mask-clearifnotmatch="true"  @if(isset($order)) value="{{ $order->formatted_date_start_order }}" @endif  @if(isset($show)) readonly @endif />
						</div>
					</div>   
				</div>
				{{--  Equipamento  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Equipamento @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $order->property->model }}" readonly/>
						@else
							<select @if(!isset($show)) name="property_id" @endif class="form-control select2" tyle="width: 100%" required >
								<option value="">Selecione</option>
								@if(isset($order)) 
									<option value="{{$order->property->id}}" selected>{{$order->property->getNameSelect()}}</option>
								@endif										
								@if(count($property) > 0) 
									@foreach($property as $prop)
										<option value="{{$prop->id}}">{{$prop->getNameSelect()}}</option>
									@endforeach
								@endif										
							</select> 
						@endif 
					</div>   
				</div> 
				{{--  Atividade  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label id="label-name" class="control-label">Atividade @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" @if(!isset($show)) name="activity" @endif class="form-control" @if(isset($order)) value="{{ $order->activity }}" @endif  @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>  
				{{--  Fornecedor  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Fornecedor @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $order->provider->name }}" readonly/>
						@else
							<select @if(!isset($show)) name="provider_id" @endif class="form-control select2" tyle="width: 100%" required >
								<option value="">Selecione</option>
								@if(isset($order)) 
									<option value="{{$order->provider->id}}" selected>{{$order->provider->name}}</option>
								@endif										
								@if(count($providers) > 0) 
									@foreach($providers as $provider)
										<option value="{{$provider->id}}">{{$provider->name}}</option>
									@endforeach
								@endif										
							</select> 
						@endif 
					</div>   
				</div>
				{{--  Urgente  --}}
				<div class="col-md-6">					
					<div class="form-group">
						<label class="control-label">Urgente @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup>@endif </label>
						@if(isset($show))
							<input type="text" class="form-control" value="{{ $order->formatted_urgent }}" readonly />
						@else
							<select @if(!isset($show)) name="urgent" @endif class="form-control" style="width: 100%"  @if(isset($show)) readonly @else required @endif>
								<option value="0" @if(isset($order) && $order->urgent == '0') selected @endif>Não</option>
								<option value="1" @if(isset($order) && $order->urgent == '1') selected @endif>Sim</option>		
							</select>
						@endif
					</div> 
				</div> 

				@if(isset($order)) 
					@if(isset($show))
						{{--  Status  --}}
						<div class="col-md-6">
							<div class="form-group">
								<label id="label-name" class="control-label">Status</label>
								<input type="text" class="form-control" value="{{ $order->formatted_status }}" readonly/>
							</div>  
						</div>  
					@else
						{{--  Status  --}}
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Status  @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif  </label>
								<select @if(!isset($show)) name="status" @endif class="form-control" @if(isset($show)) readonly @else required @endif>
									<option value="">Selecione</option>
									<option value="pending" @if(isset($order) && $order->status == 'pending') selected @endif>Pendente</option>
									<option value="execution" @if(isset($order) && $order->status == 'execution') selected @endif>Em Execução</option>
									{{--  <option value="done" @if(isset($order) && $order->status == 'done') selected @endif>Encerrado</option>  --}}
									{{--  <option value="canceled" @if(isset($order) && $order->status == 'canceled') selected @endif>Canceleado</option>  --}}
								</select>
							</div> 
						</div>	
					@endif						
				@endif
				@if(Auth::user()->administrator == 1 )
					{{--  Usuário  --}}
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Usuário @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
							@if(isset($show))
								<input class="form-control" value="{{ $order->user->getFormattedName() }}" readonly/>
							@else
								<select @if(!isset($show)) name="user_id" @endif class="form-control select2" tyle="width: 100%" required >
									<option value="">Selecione</option>
									@if(isset($order)) 
										<option value="{{$order->user->id}}" selected>{{$order->user->name}}</option>
									@endif										
									@if(count($users) > 0) 
										@foreach($users as $user)
											<option value="{{$user->id}}">{{$user->getFormattedName()}}</option>
										@endforeach
									@endif										
								</select> 
							@endif 
						</div>   
					</div>
				@endif
				{{--  Observação  --}}
				<div class="col-md-12">
					<div class="form-group">						
						<label class="control-label">Observação</label>
						<textarea id="editor1" class="text-style" @if(!isset($show)) name="observation" @endif rows="10" cols="80" style="width:100%;" @if(isset($show))  disabled @endif >@if(isset($order)){{ $order->observation }}@endif</textarea>   
					</div>
				</div>

				@if(isset($order) && (isset($done) || isset($cancel) || !empty($order->commentary)))
					{{--  Comentario  --}}
					<div class="col-md-12">
						<div class="form-group">						
							<label class="control-label">Comentario @if(isset($done) || isset($cancel)) <span style="color: red;"><sup>&bull;</sup> @endif  </label>
							<textarea id="editor1" class="text-style" name="commentary" rows="10" cols="80" style="width:100%;" @if(!isset($done) && !isset($cancel)) disabled @endif >@if(isset($order) && !empty($order->commentary)){{ $order->commentary }}@endif</textarea>   
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>	
@if(!isset($show) || isset($done) || isset($cancel))
	{{-- Botão --}}
	<div class="box box-default">
		<div class="box-footer">      
			<button type="submit" onclick="abrir()" class="btn btn-sm btn-success"><i class="fa fa-save fa-fw"></i> Salvar</button>
		</div>
	</div>
</form>
@else
	{{-- Botão --}}
	<div class="box box-default">
		<div class="box-footer">		
		<button type="submit" onclick="abrir()" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i> Editar</button>
		</div>
	</div>
</form>
@endif

@endsection

@section('footer-extras')
<!-- Jquery Mask -->
<script src="{{ asset ("/bower_components/jquery-mask-plugin/dist/jquery.mask.js") }}" type="text/javascript"></script>
{{-- Select2 --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/i18n/pt-BR.js") }}" type="text/javascript"></script>
{{-- DatePicker --}}
<script src="{{ asset ("/js/bootstrap-datepicker.min.js") }}"></script>
<script src="{{ asset ("/js/bootstrap-datepicker.pt-BR.js") }}"></script>
{{-- Mascara.js --}}
<script src="{{ asset("/bower_components/admin-lte/dist/js/mascara.js")}}"></script>
{{-- FastClick --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>

<script>
	$(document).ready(function() {
		//Inicializa FastClick
		FastClick.attach(document.body);

		//Inicializa o Datepicker
		$('.datepicker').datepicker({
			language: 'pt-BR',
			format: 'dd/mm/yyyy',
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		});
	}); 
</script>

@endsection