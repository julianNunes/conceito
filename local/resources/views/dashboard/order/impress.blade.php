
<style type="text/css">
h4, h5, td {
	padding-left: 5px;
	padding-top: 5px;
	font-family: Verdana;
}

.label-title {
	font-weight: bold;
}

/*Utilizado para retirar o header e o footer que são gerados na pagina de impressao*/
@page 
{
  size: auto;   /* auto is the initial value */
  margin: 0mm;  /* this affects the margin in the printer settings */
}
</style>

<div id="div-impress" style="margin: 15px 20px 15px 20px;">
	
	<h4>Ordem de Serviço</h4>
	<table  cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td class="label-title"> Numero</td>
			<td> {{$order->id}}</td>
		</tr>				
		<tr>
			<td class="label-title"> Tipo da OS  </td>
			<td> {{$order->typeOs->name}}</td>
		</tr>				
		<tr>
			<td class="label-title">Data de Abertura</td>
			<td> {{ $order->formatted_date_open }} </td>
		</tr>
		<tr>
			<td class="label-title"> Data de Inicio da OS </td>
			<td> {{ $order->formatted_date_start_order }} </td>
		</tr>

		<tr>
			<td class="label-title">Data de Entrega</td>
			<td>{{ $order->formatted_date_close }}</td>
		</tr>

		<tr>
			<td class="label-title">Equipamento</td>
			<td>{{ $order->property->getNameSelect() }}</td>
		</tr>

		<tr>
			<td class="label-title">Atividade</td>
			<td>{{ $order->activity }}</td>
		</tr>

		<tr>
			<td class="label-title">Urgente</td>
			<td>{{ $order->formatted_urgent }}</td>
		</tr>

		<tr>
			<td class="label-title">Status</td>
			<td>{{ $order->formatted_status }}</td>
		</tr>

		@if(Auth::user()->administrator == 1)
			<tr>
				<td class="label-title">Usuário</td>
				<td>{{ $order->user->getFormattedName() }}</td>
			</tr>
		@endif
    </table>
	
    <br>
    <br>
    <table  cellspacing="0" cellpadding="0" border="0">
        <tr>
			<td class="label-title">Observação</td>
		</tr>
		<tr>
			<td>{{ $order->observation }}</td>
		</tr>
    </table>
    
    <br>
    <br>
    <table  cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td class="label-title">Comentario</td>
		</tr>
		<tr>
			<td>{{ $order->commentary }}</td>
		</tr>
    </table>
</div>
