@extends('dashboard.layout.admin_template')
@section('header-extras')
<!-- date-picker -->
<link href="{{ asset ("/css/datepicker/bootstrap-datepicker3.standalone.css") }}" rel="stylesheet" type="text/css" />

<style type="text/css">
/*Para o Data Table*/
.dataTables_scrollHead{ overflow: visible !important; }
/*Para mostrar o scroll horizontalmente*/
.scroll-x {  overflow-x:auto; margin-right: 10px;  }
/*Alinha todos os conteudos no meio*/
.table>tbody>tr>td, 
.table>tbody>tr>th, 
.table>tfoot>tr>td, 
.table>tfoot>tr>th, 
.table>thead>tr>td, 
.table>thead>tr>th {
	vertical-align: middle;
}
/*Para arrumar onde não houver sort na coluna da tabela*/
.sorting_disabled { padding: 0px !important; }
</style>
@endsection

@section('page_title','Ordem de Serviço')
@section('page_description','Painel de Controle')


<!-- Register Button -->
@section('top-button')
<div class="breadcrumb">
	<div class="row">
		<form method="get" action="{{ route('order.index') }}" enctype="multipart/form-data">			
			<div class="col-md-3 {{ Auth::user()->administrator == 0 ? 'col-md-offset-3' : '' }} " style="padding-bottom: 5px;">
				<input type="text" class="form-control datepicker" data-mask="00/00/0000" data-mask-selectonfocus="true" data-mask-clearifnotmatch="true" placeholder="__/__/____" value="{{$start}}"/> 
				<input id="start" type="hidden" name="start" value="{{str_replace('/', '-', $start)}}"/> 
			</div>
			<div class="col-md-3" style="padding-bottom: 5px;">
				<input type="text" class="form-control datepicker" data-mask="00/00/0000" data-mask-selectonfocus="true" data-mask-clearifnotmatch="true" placeholder="__/__/____" value="{{$end}}"/> 
				<input id="end" type="hidden" name="end" value="{{str_replace('/', '-', $end)}}" /> 
			</div>
			<div class="col-md-3" style="padding-bottom: 5px;">
				<button type="submit" class="btn btn-block btn btn-default"><i class="fa fa-search fa-fw"></i>Pesquisar</button>
			</div>
		</form>
		@if(Auth::user()->administrator == 1 )   
			<div class="col-md-3">
				<a href="{{ action('OrderController@create') }}"> <button class="btn btn-block btn-success"><i class="fa fa-plus fa-fw"></i> Cadastrar</button> </a>
			</div>
		@endif
	</div>
</div>
@endsection


<!-- Content -->
@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

<div id="div-main" class='row'> 
	<div class="col-xs-12">
		<div class="box">
			<div id="div-scroll-x" class="box-body">
				<table id="tbAll" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th class="text-center">Código</th>
							<th>Tipo de OS</th>
							<th>Equipamento</th>
							<th>Data de Abertura</th>            
							<th>Data de Entrega</th>            
							<th>Status</th> 
							@if(Auth::user()->administrator == 1 )       
								<th>Usuário</th>        
							@endif
							<th class="text-center">Ação</th>            
						</tr>
					</thead>
					<tbody>
					@if(count($orders) >0)
						@foreach($orders as $order)
							<tr @if($order->urgent == 1) class="warning" @endif >
								<input type="hidden" class="id" value="{{$order->id}}">
								<td class="text-center">{{ $order->id }}</td>
								<td>{{ $order->typeOs->name }}</td> 
								<td>{{ $order->property->getNameSelect() }}</td> 
								<td data-order="{{$order->date_open}}">{{ $order->formatted_date_open }}</td> 
								<td data-order="{{$order->date_close}}21">{{ $order->formatted_date_close }}</td> 
								<td>{{ $order->formatted_status }}</td> 
								@if(Auth::user()->administrator == 1 )
									<td>{{ $order->user->getFormattedName() }}</td> 
								@endif
								<td class="action-group-button text-center" style="width: 10% !important">          
									<div class="btn-group">
										<button type="button" class="btn btn-default">Ação</button>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" style="min-width: 100%!important">
											<li class="dropdown-primary"><a href="{{ route('order.show', ['id' => $order->id]) }}">Ver</a></li>
											<li class="dropdown-info"><a class="btn-impress" href="javascript:void(0);">Imprimir</a></li>
											@if($order->status == 'pending' || $order->status == 'execution')
												@if(Auth::user()->administrator == 1 ) 
													<li class="dropdown-warning"><a href="{{ route('order.edit', ['id' => $order->id]) }}">Editar</a></li>
												@endif
												<li  class="dropdown-success"><a href="{{ route('order.confirm-done', ['id' => $order->id]) }}">Encerrar</a></li>
												<li  class="dropdown-warning"><a href="{{ route('order.confirm-cancel', ['id' => $order->id]) }}">Cancelar</a></li>
												<li class="divider"></li>
												@if(Auth::user()->administrator == 1 ) 
													<li class="dropdown-danger"><a href="{{ route('order.delete', ['id' => $order->id]) }}">Excluir</a></li>
												@endif
											@endif
										</ul>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
					</tbody>
					<tfoot>
						<tr>
							<th class="text-center">Código</th>
							<th>Tipo de OS</th>
							<th>Equipamento</th>
							<th>Data de Abertura</th>            
							<th>Data de Entrega</th>                  
							<th>Status</th>  
							@if(Auth::user()->administrator == 1 )          
								<th>Usuário</th>  
							@endif          
							<th class="text-center">Ação</th>           
						</tr>
					</tfoot>
				</table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

<!-- Footer Extra -->
@section('footer-extras')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
{{-- FastClick --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>
{{-- DatePicker --}}
<script src="{{ asset ("/js/bootstrap-datepicker.min.js") }}"></script>
<script src="{{ asset ("/js/bootstrap-datepicker.pt-BR.js") }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.min.js") }}"></script>
<script>
	$(function () {

		var _token = '{{csrf_token()}}'; 

		@if(Auth::user()->administrator == 1 )    
			//Inicializa Data Table
			$("#tbAll").DataTable({
				"aaSorting": [],	
				"columnDefs": [
					{ "orderable": false, "targets": [7] }
				]
			});	

		@else
			//Inicializa Data Table
			$("#tbAll").DataTable({
				"aaSorting": [],	
				"columnDefs": [
					{ "orderable": false, "targets": [6] }
				]
			});	

		@endif

		//Inicializa o Datepicker
		$('.datepicker').datepicker({
			language: 'pt-BR',
			format: 'dd/mm/yyyy',
			todayHighlight: true,
			autoclose: true,
			clearBtn: true
		}).on('changeDate', function(e) {
			$(this).next().val(moment(e.date).format('DD-MM-YYYY'));
    	});		

		//Inicializa FastClick
		FastClick.attach(document.body);

		//Ajax para obter o html da Impressão do Atendimento
		function impress(evt) {
			var id = $(this).closest('tr').find(".id").val();	
			if(id) {

				var jqxhr = $.ajax({
					url: "{{ route('order.impress') }}",
					data: {'id': id},
					dataType: 'JSON',
					type: 'GET',
					beforeSend: function(request) {
						return request.setRequestHeader('X-CSRF-Token', _token);
					}
				});
				
				jqxhr.done(function(data) {
					if(data && data.success && data.html) {
						w=window.open();
						w.document.write(data.html);
						w.print();
						w.close();

					} else {

						if(data.error) { console.log(data.error); }
					}
				});
			}
		}

		//Redesenha o datatable
		function screenResize(evt) {
			evt.preventDefault();

			//Verificar tamanho da tela
			var width = $(window).width();				

			//se a tela menor que 1024 o datatable fica com scroll horizontal
			if(width < 1024){		
				$('.action-group-button').removeAttr("style").css('min-width','100px');			
				$('#div-scroll-x').addClass('scroll-x');								
				
			} else {
				$('.action-group-button').removeAttr("style").css('width','10%');
				$('#div-scroll-x').removeClass('scroll-x');								
			}
		}				

		//Eventos
		$(document).delegate('.btn-impress','click', impress);			
		$(window).resize(screenResize);
		$(window).trigger('resize');
	});

</script>

@endsection