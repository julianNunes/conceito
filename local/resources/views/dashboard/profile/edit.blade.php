@extends('dashboard.layout.admin_template')


@section('header-extras')
<script type="text/javascript">

	function validaSenha (input) { 
		if (input.value != document.getElementById('txtPassword').value) {
			input.setCustomValidity('Repita a senha corretamente');
		} else {
			input.setCustomValidity('');
		}
	}

</script>

<style type="text/css">
	.checkbox label, .radio label { margin-bottom: 5px; }
</style>
@endsection


@section('page_title','Perfil')

@section('page_description','Editar')

@section('content')

<div id="div-main">
	<form id="formUser" method="post" action="{{ route('user.profile.update') }}" enctype="multipart/form-data">    
		<div class="box box-default">

			@if(Session::has('flash_message'))
				<div class="box-body">
					<div class="alert alert-warning alert-dismissible"> <h4><i class="icon fa fa-warning"></i> Atenção!</h4><em> {!! session('flash_message') !!}</em></div>
				</div>
			@endif
			
			@if(Session::has('flash_message2'))
				<div class="box-body">
					<div class="alert alert-success alert-dismissible"> <h4><i class="icon fa fa-warning"></i> Atenção!</h4><em> {!! session('flash_message2') !!}</em></div>
				</div>
			@endif
			<div class="box-header with-border">
				<h3 class="box-title">Dados Cadastrais</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
				</div>
			</div>

			<div class="box-body">
				<div class="row">
					<input type="hidden" name="_token" value="{{csrf_token()}}">				

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Nome<span style="color: red;"><sup>&bull;</sup></span></label>
							<input type="text" name="name" class="form-control" value="{{$user->name}}" placeholder="Nome" required/> 
						</div>   
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Apelido</label>
							<input type="text" name="nickname" class="form-control" value="{{$user->nickname}}" placeholder="Apelido"/>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Senha</label>
							<input type="text" style="display:none;"> 
							<input type="password" id="txtPassword" name="password" placeholder="Somente preencher caso queira mudar a senha" class="form-control"/>
						</div>           
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Confirmar Senha</label>
							<input type="password"  id="txtPasswordConfirmation"  name="password_confirmation" placeholder="Somente preencher caso queira mudar a senha" class="form-control" oninput="validaSenha(this)"/>
						</div>     
					</div>
				</div>
			</div>
		</div>

		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" class="btn btn-sm btn-success" onclick="abrir()"><i class="fa fa-save fa-fw"></i> Salvar</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('footer-extras')

<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/i18n/pt-BR.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/local/vendor/igorescobar/jquery-mask-plugin/dist/jquery.mask.min.js") }}" type="text/javascript"></script>

<script>
	$(document).ready(function() {

		function changePassword (evt) {
			var result = $(this).val().length;
			if(result >0) {
				
				$(this).prop('required', true);  				
				$('#txtPasswordConfirmation').prop('required', true);  				
			} else {
				
				$(this).prop('required', false);  				
				$('#txtPasswordConfirmation').prop('required', false);  
			}
		}

	 // Eventos
	 $('#txtPassword').on('input', changePassword);
	}); 
</script>

@endsection