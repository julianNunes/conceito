@extends('dashboard.layout.admin_template')
@section('header-extras')
<script src="{{ asset("/bower_components/admin-lte/dist/js/validadorcpf.js")}}"></script>
<script src="{{ asset("/js/validaIE.js")}}"></script>
<!-- date-picker -->
<link href="{{ asset ("/css/datepicker/bootstrap-datepicker3.standalone.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_title','Bens')
@section('page_description', $page_description)

@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

@if(!isset($show))
	<form method="post" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
		<input type="hidden" name="_token" value="{{csrf_token()}}">	
@else
	<form method="get" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
@endif	

	{{-- Dados da Iniciais --}}
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title">Dados Iniciais</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
			</div>
		</div>

		<div class="box-body">
			<div class="row">
				{{--  Fabricante  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Fabricante @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $property->fabricator->name }}" readonly/>
						@else
							<select @if(!isset($show)) name="fabricator_id" @endif class="form-control select2" tyle="width: 100%" required >
								<option value="">Selecione</option>
								@if(isset($property)) 
									<option value="{{$property->fabricator->id}}" selected>{{$property->fabricator->name}}</option>
								@endif										
								@if(count($fabricators) > 0) 
									@foreach($fabricators as $fabricator)
										<option value="{{$fabricator->id}}">{{$fabricator->name}}</option>
									@endforeach
								@endif										
							</select> 
						@endif 
					</div>   
				</div>
				{{--  Modelo  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label id="label-name" class="control-label">Modelo @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" @if(!isset($show)) name="model" @endif class="form-control" @if(isset($property)) value="{{ $property->model }}" @endif  @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>  
				{{--  Familia  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Família @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $property->fabricator->name }}" readonly/>
						@else
							<select @if(!isset($show)) name="family_id" @endif class="form-control select2" tyle="width: 100%" required >
								<option value="">Selecione</option>
								@if(isset($property)) 
									<option value="{{$property->family->id}}" selected>{{$property->family->name}}</option>
								@endif										
								@if(count($families) > 0) 
									@foreach($families as $family)
										<option value="{{$family->id}}">{{$family->name}}</option>
									@endforeach
								@endif										
							</select> 
						@endif 
					</div>   
				</div>
				{{--  Descrição  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label id="label-name" class="control-label">Descrição @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" @if(!isset($show)) name="description" @endif class="form-control" @if(isset($property)) value="{{ $property->description }}" @endif  @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>  
				{{--  Plaqueta  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label id="label-name" class="control-label">Plaqueta @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" @if(!isset($show)) name="code" @endif class="form-control" @if(isset($property)) value="{{ $property->code }}" @endif  @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>  
				{{--  Fornecedor  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Fornecedor @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $property->provider->name }}" readonly/>
						@else
							<select @if(!isset($show)) name="provider_id" @endif class="form-control select2" tyle="width: 100%" required >
								<option value="">Selecione</option>
								@if(isset($property)) 
									<option value="{{$property->provider->id}}" selected>{{$property->provider->name}}</option>
								@endif										
								@if(count($providers) > 0) 
									@foreach($providers as $provider)
										<option value="{{$provider->id}}">{{$provider->name}}</option>
									@endforeach
								@endif										
							</select> 
						@endif 
					</div>   
				</div>
				{{--  Data de Aquisição  --}}
				<div class="col-md-3">
					<div class="form-group">
						<label id="label-name" class="control-label">Data de Aquisição @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datepicker" @if(!isset($show)) name="date_purchase" @endif data-mask="00/00/0000"  placeholder="__/__/____" data-mask-selectonfocus="true"  data-mask-clearifnotmatch="true"  @if(isset($property)) value="{{ $property->formatted_date_purchase }}" @endif  @if(isset($show)) readonly @else required @endif />
						</div>
					</div>   
				</div>
				{{--  Ultima Manutenção  --}}
				<div class="col-md-3">
					<div class="form-group">
						<label id="label-name" class="control-label">Ultima Manutenção </label>
						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control datepicker" @if(!isset($show)) name="last_maintenance" @endif data-mask="00/00/0000"  placeholder="__/__/____" data-mask-selectonfocus="true"  data-mask-clearifnotmatch="true"  @if(isset($property)) value="{{ $property->formatted_last_maintenance }}" @endif  @if(isset($show)) readonly @endif />
						</div>
					</div>   
				</div>
				{{--  Localização  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Localização @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $property->location->name }}" readonly/>
						@else
							<select @if(!isset($show)) name="location_id" @endif class="form-control select2" tyle="width: 100%" required >
								<option value="">Selecione</option>
								@if(isset($property)) 
									<option value="{{$property->location->id}}" selected>{{$property->location->getFormattedName()}}</option>
								@endif										
								@if(count($locations) > 0) 
									@foreach($locations as $location)
										<option value="{{$location->id}}">{{$location->getFormattedName()}}</option>
									@endforeach
								@endif										
							</select> 
						@endif 
					</div>   
				</div>
				{{--  Nota Fiscal  --}}
				<div class="col-md-6">
					<div class="form-group">
						<label id="label-name" class="control-label">Nota Fiscal @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" @if(!isset($show)) name="nf" @endif class="form-control" @if(isset($property)) value="{{ $property->nf }}" @endif  @if(isset($show)) readonly @else required @endif/>
					</div>  
				</div>  
				{{--  Valor  --}}
				<div class="col-md-6"> 
					<div class="form-group">
						<label class="control-label">Valor @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<div class="input-group">
							<span class="input-group-addon">R$</span>
							<input type="text" @if(!isset($show)) name="total" @endif placeholder="0,00" class="form-control money" @if(isset($property)) value="{{ $property->formatted_total }}" @endif  @if(isset($show)) readonly @else required @endif/>
						</div>
					</div>
				</div>
				{{--  Ativo  --}}
				<div class="col-md-6">					
					<div class="form-group">
						<label class="control-label">Ativo @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup>@endif </label>
						@if(isset($show))
							<input type="text" class="form-control" value="{{ $property->formatted_active }}" readonly />
						@else
							<select @if(!isset($show)) name="active" @endif class="form-control" style="width: 100%"  @if(isset($show)) readonly @else required @endif>
								<option value="">Selecione</option>
								<option value="0" @if(isset($property) && $property->active == '0') selected @endif>Não</option>
								<option value="1" @if((isset($property) && $property->active == '1')  || !isset($property)) selected @endif>Sim</option>		
							</select>
						@endif
					</div> 
				</div> 
			</div>
		</div>
	</div>	
@if(!isset($show))
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" onclick="abrir()" class="btn btn-sm btn-success"><i class="fa fa-save fa-fw"></i> Salvar</button>
			</div>
		</div>
	</form>
@else
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">		
			<button type="submit" onclick="abrir()" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i> Editar</button>
			</div>
		</div>
	</form>
@endif

@endsection

@section('footer-extras')
<!-- Jquery Mask -->
<script src="{{ asset ("/bower_components/jquery-mask-plugin/dist/jquery.mask.js") }}" type="text/javascript"></script>
{{-- Select2 --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/i18n/pt-BR.js") }}" type="text/javascript"></script>
{{-- DatePicker --}}
<script src="{{ asset ("/js/bootstrap-datepicker.min.js") }}"></script>
<script src="{{ asset ("/js/bootstrap-datepicker.pt-BR.js") }}"></script>
{{-- Mascara.js --}}
<script src="{{ asset("/bower_components/admin-lte/dist/js/mascara.js")}}"></script>
{{-- FastClick --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>

<script>
	$(document).ready(function() {
			//Inicializa FastClick
			FastClick.attach(document.body);
		
		@if(!isset($show))
			//Set da linguagem do select2
			$.fn.select2.defaults.set('language', 'pt-BR');

			//Inicia os plugins nos componentes
			$('.select2', '#div-main').select2();

			//Inicializa o Datepicker
			$('.datepicker').datepicker({
				language: 'pt-BR',
				format: 'dd/mm/yyyy',
				todayHighlight: true,
				autoclose: true,
				clearBtn: true
			});

			//Set da máscara do telefone
			$('.money').mask("#.##0,00", {reverse: true});
		@endif
	}); 
</script>

@endsection