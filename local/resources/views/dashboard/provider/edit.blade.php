@extends('dashboard.layout.admin_template')
@section('header-extras')
<!-- date-picker -->
<link href="{{ asset ("/css/datepicker/bootstrap-datepicker3.standalone.css") }}" rel="stylesheet" type="text/css" />
<script src="{{ asset("/bower_components/admin-lte/dist/js/validadorcpf.js")}}"></script>
@endsection

@section('page_title','Fornecedor')
@section('page_description','Editar')

@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

@if(!isset($show))
	<form method="post" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
		<input type="hidden" name="_token" value="{{csrf_token()}}">	
@else
	<form method="get" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
@endif	

	{{-- Dados da Iniciais --}}
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title">Dados Iniciais</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
			</div>
		</div>

		<div class="box-body">
			<div class="row">
				<div class="col-md-6"> 
					<div class="form-group">
						<label class="control-label">Tipo de Pessoa @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input type="text" class="form-control" value="{{ $provider->formatted_type }}" readonly />
						@else
							<select id="type-client" @if(!isset($show)) name="type" @endif class="form-control" required style="width: 100%">
								<option value="">Selecione</option>
								<option value="pf" @if(isset($provider) && $provider->type == 'pf') selected @endif> Pessoa Física</option>
								<option value="pj" @if(isset($provider) && $provider->type == 'pj') selected @endif> Pessoa Jurídica </option>
							</select> 
						@endif             
					</div>
				</div>
				<div class="hidden-type" @if(!isset($provider) && !isset($show)) style="display:none;" @endif>
					{{--  Nome / Nome Fantasia  --}}
					<div class="col-md-6">
						<div class="form-group">
							@if(isset($provider))
								<label  class="control-label">{{ $provider->type == 'pj' ? "Pessoa Física" : "Pessoa Jurídica" }}@if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif</label>
							@else
								<label id="label-name" class="control-label"><span style="color: red;"><sup>&bull;</sup></label>
							@endif						
							<input type="text" @if(!isset($show)) name="name" @endif class="form-control" placeholder="Nome" @if(isset($provider)) value="{{ $provider->name }}" @endif  @if(isset($show)) readonly @else required @endif/>
						</div>  
					</div>  
					{{--  CPF / CNPJ  --}}
					<div class="col-md-6">
						<div class="form-group">
							@if(isset($provider))
								<label  class="control-label">{{ $provider->type == 'pj' ? "CPF" : "CNPJ" }}@if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif</label>
							@else
								<label  class="control-label"><span style="color: red;"><sup>&bull;</sup></label>
							@endif
							<input id="cpf_cnpj" type="text" @if(!isset($show)) name="cpf_cnpj" @endif class="form-control" @if(isset($provider)) value="{{ $provider->cpf_cnpj }}" @endif   @if(isset($show)) readonly @else required @endif/>
						</div>  
					</div>  
					{{--  Numero de Telefone  --}}
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Número de Telefone @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
							<input type="text" @if(!isset($show)) name="phone_number" @endif class="form-control number-phone" placeholder="(00) 00000-0000" @if(isset($provider)) value="{{ $provider->phone_number }}" @endif @if(isset($show)) readonly @else required @endif/>
						</div>  
					</div>
					{{--  Email  --}}
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">E-mail @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
							<input type="text" @if(!isset($show)) name="email" @endif class="form-control email " pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Digite seu email" @if(isset($provider)) value="{{ $provider->email }}" @endif   @if(isset($show)) readonly @else required @endif/>
						</div>  
					</div>	
					{{--  Ativo  --}}
					<div class="col-md-6">					
						<div class="form-group">
							<label class="control-label">Ativo @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
							@if(isset($show))
								<input type="text" class="form-control" value="{{ $provider->formatted_active }}" readonly />
							@else
								<select @if(!isset($show)) name="active" @endif class="form-control" style="width: 100%"  @if(isset($show)) readonly @else required @endif>
									<option value="">Selecione</option>
									<option value="0" @if(isset($provider) && $provider->active == '0') selected @endif>Não</option>
									<option value="1" @if((isset($provider) && $provider->active == '1')  || !isset($provider)) selected @endif>Sim</option>		
								</select>
							@endif
						</div> 
					</div> 
				</div>
			</div>
		</div>
	</div>	

	{{-- Endereço --}}
	<div id="div-address" class="box box-default hidden-type" @if(!isset($provider) && !isset($show)) style="display:none;" @endif>
		<div class="box-header with-border">
			<h3 class="box-title">Dados do Endereço</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">CEP @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" id="zipcode" @if(!isset($show)) name="zipcode" @endif class="form-control" data-mask="00000-000" placeholder="00000-000" @if(isset($provider) && isset($provider->address)) value="{{ $provider->address->zipcode }}" @endif  @if(isset($show)) readonly @else required @endif/>
					</div>   
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Logradouro @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" id="street"  @if(!isset($show)) name="street" @endif class="form-control" @if(isset($provider) && isset($provider->address)) value="{{ $provider->address->street }}" @endif @if(isset($show)) readonly @else required @endif/>
					</div>   
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Número @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" id="number"  @if(!isset($show)) name="number" @endif class="form-control" @if(isset($provider) && isset($provider->address)) value="{{ $provider->address->number }}" @endif @if(isset($show)) readonly @else required @endif/>
					</div>   
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Complemento</label>
						<input type="text"  id="comp"  @if(!isset($show)) name="comp" @endif class="form-control" @if(isset($provider) && isset($provider->address)) value="{{ $provider->address->comp }}" @endif  @if(isset($show)) readonly  @endif/>
					</div>   
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Bairro @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						<input type="text" id="district" @if(!isset($show)) name="district" @endif class="form-control" @if(isset($provider) && isset($provider->address)) value="{{ $provider->address->district }}" @endif @if(isset($show)) readonly @else required @endif/>
					</div>   
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Ponto de Referência</label>
						<input type="text"  @if(!isset($show)) name="info" @endif class="form-control" @if(isset($provider) && isset($provider->address)) value="{{ $provider->address->info }}" @endif @if(isset($show)) readonly  @endif/>
					</div>   
				</div>							

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">UF @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input class="form-control" value="{{ $provider->address->city->state->initials }}" readonly/>
						@else
							<select id="state" class="form-control select2" tyle="width: 100%" required>
								@if(isset($provider) && isset($provider->address)) 
									<option value="{{$provider->address->city->state->id}}" selected>{{$provider->address->city->state->initials}}</option>
								@endif										
								<option value="">Selecione</option>		
								@foreach($states as $state)
									<option value="{{$state->id}}">{{$state->initials}}</option>
								@endforeach
							</select> 
						@endif 
					</div>   
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Cidade @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
						@if(isset($show))
							<input  class="form-control" value="{{ $provider->address->city->name }}" readonly/>
						@else
							<select id="city" @if(!isset($show)) name="city_id" @endif class="form-control select2" tyle="width: 100%"  @if(!isset($provider)) disabled @else required @endif>
								@if(isset($provider) && isset($provider->address))  
									<option value="{{$provider->address->city->id}}" selected>{{$provider->address->city->name}}</option>
									<option value="">Selecione</option>		
									@foreach($cities as $city)
										<option value="{{$city->id}}">{{$city->name}}</option>
									@endforeach
								@endif										
							</select> 
						@endif
					</div>   
				</div>
			</div>
		</div>
	</div>	

@if(!isset($show))
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" onclick="abrir()" class="btn btn-sm btn-success"><i class="fa fa-save fa-fw"></i> Salvar</button>
			</div>
		</div>
	</form>
@else
		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">		
			<button type="submit" onclick="abrir()" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i> Editar</button>
			</div>
		</div>
	</form>
@endif

@endsection

@section('footer-extras')

{{-- Mask --}}
<script src="{{ asset ("/bower_components/jquery-mask-plugin/dist/jquery.mask.js") }}" type="text/javascript"></script>
{{-- Select2 --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/i18n/pt-BR.js") }}" type="text/javascript"></script>
{{-- FastClick --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>

<script>
	$(document).ready(function() {
		
		var city_search = '';
		
		//Inicializa FastClick
		FastClick.attach(document.body);
	
	@if(!isset($show))		
		//Máscara de cep
		$('#zipcode', '#div-main').mask('00000-000', { placeholder: '00000-000'});

		//Máscara de telefone direto da documentação do mask plugin
		var maskPhone = function (val) {
		  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		phone_typing = {
		  onKeyPress: function(val, e, field, options) {
		      field.mask(maskPhone.apply({}, arguments), options);
		    }
		};

		//Set da máscara do telefone
		$('.number-phone').mask(maskPhone, phone_typing);		

		//Set da linguagem do select2
		$.fn.select2.defaults.set('language', 'pt-BR');

		//Inicia os plugins nos componentes
		$('.select2', '#div-main').select2();

		//Responsavel por validar o CNPJ
		function validaCNPJ(cnpj) {
			cnpj = cnpj.replace(/[^\d]+/g,'');

			if(cnpj == '') return false;

			if (cnpj.length != 14)
				return false;

			// Elimina CNPJs invalidos conhecidos
			if (cnpj == "00000000000000" || 
				cnpj == "11111111111111" || 
				cnpj == "22222222222222" || 
				cnpj == "33333333333333" || 
				cnpj == "44444444444444" || 
				cnpj == "55555555555555" || 
				cnpj == "66666666666666" || 
				cnpj == "77777777777777" || 
				cnpj == "88888888888888" || 
				cnpj == "99999999999999")
				return false;

			// Valida DVs
			tamanho = cnpj.length - 2
			numeros = cnpj.substring(0,tamanho);
			digitos = cnpj.substring(tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0))
				return false;

			tamanho = tamanho + 1;
			numeros = cnpj.substring(0,tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1))
				return false;

			return true;
		}

		//Metodo para chamar a validação do CNPJ
		function vCNPJ(evt){
			evt.preventDefault();
			if(validaCNPJ($(this).val())) {
				this.setCustomValidity('');
			} else {
				this.setCustomValidity('CNPJ inválido!');
			}
		}

		//Metodo para chamar a validação do CPF
		function vCPF(evt){
			evt.preventDefault();
			this.setCustomValidity(validaCPF($(this).val())?'':'CPF inválido!');
		}


		//Faz a busca via ajax na Api para os correios
		function searchZipcode(evt) {
			evt.preventDefault();
			var $address = $('#div-address');
			var cep = $(this).val().replace('-','');
			
			if(cep.length == 8) {
				city_search = "";
				$.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
					if (!("erro" in dados)) {
						if(dados.uf) {							
							
							//Guarda o nome da cidade para utilizar
							city_search = dados.localidade;
							//SET da UF do estado e dispara evento para buscar cidade
							var filter = "#state option:contains('"+dados.uf+"')";
							$address.find(filter).prop('selected', true).trigger('change');								
							
							$address.find("#street").val(dados.logradouro);
							$address.find("#district").val(dados.bairro);
							$address.find("#number").val('');
							$address.find("#comp").val('');
						}

					} else {						
					
						$address.find("#state").val('').trigger('change');

						$address.find("#street").val('');
						$address.find("#district").val('');
						$address.find("#number").val('');
						$address.find("#comp").val('');
					}
				});
			}
		}

		//Metodo para alterar os componentes em tela ao alterar o tipo de cliente
		function changeTypeClient(evt) {
			evt.preventDefault();

			var value =  this.value;

			if(value) {
				$('.hidden-type').show(300);
				var $cpf_cnpj = $('#cpf_cnpj');
				var $label_name = $('#label-name');
				
				if(value == 'pj') {
					$label_name.html('Nome Fantasia<span style="color: red;"><sup>&bull;</sup></span>');

					$cpf_cnpj.off('input').on('input', vCNPJ).val('').prev().html('CNPJ<span style="color: red;"><sup>&bull;</sup></span>');
					$cpf_cnpj.unmask('');
					$cpf_cnpj.mask('00.000.000/0000-00', { placeholder: '__.___.___/____-__'});

				} else {

					$label_name.html('Nome<span style="color: red;"><sup>&bull;</sup></span>');
					$cpf_cnpj.off('input').on('input', vCPF).val('').prev().html('CPF<span style="color: red;"><sup>&bull;</sup></span>');
					$cpf_cnpj.unmask('');
					$cpf_cnpj.mask('000.000.000-00', { placeholder: '___.___.___-__'});
				} 

			} else{

				$('.hidden-type').hide(300);
			}
		}
		
		//Metodo que busca as cidades ao mudar o estado
		function changeState(evt) {
			evt.preventDefault();

			var $inputToken = $("input[name='_token']");
			var jqxhr = $.ajax({
				url: 			'{{ route('city.state') }}',
				dataType: 	'JSON',
				data: 		{ state_id : $(this).val() },
				beforeSend: function(request) {
					return request.setRequestHeader('X-CSRF-Token',  $inputToken.val());
				}
			});

			//Limpa o componente
			var $city = $('#city'); 
			$city.find('option').remove();
			$city.prop('disabled', true);
			$city.append($('<option>', {value: '', text: 'Selecione'}));
			$city.val('').trigger('change');
			
			jqxhr.done(function(data) {
				if(data && data.success && data.cities) {
					var item = '';
					for (var i = 0; i < data.cities.length; i++) {
						item += '<option value="'+data.cities[i].id+'">'+data.cities[i].name+'</option>';
					}	
					$city.append(item);
					$city.prop('disabled', false);

					//Caso tenha sido buscado por CEP a variavel ja vai estar preenchida para buscar a cidade
					if(city_search) {	
						var filter = 'option:contains("'+city_search+'")';
						$city.find(filter).prop('selected', true).change();
					}
				} else if(data.error){
					console.log(data);
				}
			}); 
		}

		//Chamada para Eventos
		$(document).delegate('#state','change', changeState);
		$(document).delegate('#type-client','change', changeTypeClient);
		$(document).delegate('#zipcode','change', searchZipcode);

		//Ativando a mascara conforme o tipo de cliente
		@if(isset($provider))
			@if($provider->type == 'pj')
				$('#cpf_cnpj').mask('00.000.000/0000-00', { placeholder: '__.___.___/____-__'});
				$('#cpf_cnpj').on('input', vCNPJ);
			@else
				$('#cpf_cnpj').mask('000.000.000-00', { placeholder: '___.___.___-__'});
				$('#cpf_cnpj').on('input', vCPF);
			@endif
		@endif
	@endif
	
	}); 
</script>

@endsection