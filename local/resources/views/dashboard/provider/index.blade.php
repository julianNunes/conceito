@extends('dashboard.layout.admin_template')
@section('header-extras')
<style type="text/css">
/*Para o Data Table*/
.dataTables_scrollHead{ overflow: visible !important; }
/*Para mostrar o scroll horizontalmente*/
.scroll-x {  overflow-x:auto; margin-right: 10px;  }
/*Alinha todos os conteudos no meio*/
.table>tbody>tr>td, 
.table>tbody>tr>th, 
.table>tfoot>tr>td, 
.table>tfoot>tr>th, 
.table>thead>tr>td, 
.table>thead>tr>th {
	vertical-align: middle;
}
/*Para arrumar onde não houver sort na coluna da tabela*/
.sorting_disabled { padding: 0px !important; }
</style>
@endsection

@section('page_title','Fornecedores')
@section('page_description','Painel de Controle')


<!-- Register Button -->
@section('top-button')
<div class="breadcrumb">
	<div class="row">
		<div class="col-md-6" style="margin-bottom: 5px;">
			<a href="{{ action('ProviderController@index') }}" > <button class="btn btn-block btn btn-default"><i class="fa fa-repeat fa-fw"></i> Atualizar</button> </a>
		</div>
		<div class="col-md-6">
			<a href="{{ action('ProviderController@create') }}"> <button class="btn btn-block btn-success"><i class="fa fa-plus fa-fw"></i> Cadastrar</button> </a>
		</div>
	</div>
</div>
@endsection


<!-- Content -->
@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

<div id="div-main" class='row'> 
	<div class="col-xs-12">
		<div class="box">
			<div id="div-scroll-x" class="box-body">
				<table id="tbAll" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Nome Fantasia</th>
							<th>CNPJ</th>
							<th>Telefone</th>            
							<th>Email</th>           
							<th class="text-center">Ação</th>            
						</tr>
					</thead>
					<tbody>
					@if(count($providers) >0)
						@foreach($providers as $provider)
							<tr @if($provider->active == 0) class="warning" @endif >
								<td>{{ $provider->name }}</td>
								<td>{{ $provider->cpf_cnpj }}</td> 
								<td>{{ $provider->phone_number }}</td> 
								<td>{{ $provider->email }}</td> 
								<td class="action-group-button text-center" style="width: 10% !important">          
									<div class="btn-group">
										<button type="button" class="btn btn-default">Ação</button>
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" style="min-width: 100%!important">
											<li class="dropdown-primary"><a href="{{ route('provider.show', ['id' => $provider->id]) }}">Ver</a></li>
											<li class="dropdown-warning"><a href="{{ route('provider.edit', ['id' => $provider->id]) }}">Editar</a></li>
											<li class="divider"></li>
											<li class="dropdown-danger"><a href="{{ route('provider.delete', ['id' => $provider->id]) }}">Excluir</a></li>
										</ul>
									</div>
								</td>
							</tr>
						@endforeach
					@endif
					</tbody>
					<tfoot>
						<tr>
							<th>Nome Fantasia</th>
							<th>CNPJ</th>
							<th>Telefone</th>            
							<th>Email</th>            
							<th class="text-center">Ação</th>           
						</tr>
					</tfoot>
				</table>
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

<!-- Footer Extra -->
@section('footer-extras')
<!-- DataTables -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
{{-- FastClick --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>

<script>
	$(function () {

		//Inicializa Data Table
		$("#tbAll").DataTable({
			"aaSorting": [],	
			"columnDefs": [
				{ "orderable": false, "targets": [4] }
			]
		});			

		//Inicializa FastClick
		FastClick.attach(document.body);

		//Redesenha o datatable
		function screenResize(evt) {
			evt.preventDefault();

			//Verificar tamanho da tela
			var width = $(window).width();				

			//se a tela menor que 1024 o datatable fica com scroll horizontal
			if(width < 1024){		
				$('.action-group-button').removeAttr("style").css('min-width','100px');			
				$('#div-scroll-x').addClass('scroll-x');								
				
			} else {
				$('.action-group-button').removeAttr("style").css('width','10%');
				$('#div-scroll-x').removeClass('scroll-x');								
			}
		}				

		//eventos		
		$(window).resize(screenResize);
		$(window).trigger('resize');
	});

</script>

@endsection
