@extends('dashboard.layout.admin_template')
@section('header-extras')

{{-- Full Calendar --}}
<link href="{{ asset ("/js/fullcalendar/fullcalendar.min.css") }}" rel="stylesheet" type="text/css" />
<link href="{{ asset ("/js/fullcalendar/fullcalendar.print.css") }}" rel='stylesheet' media='print' />

<style type="text/css">
.fc-event-container{
	cursor:pointer; 
}

/* UTILIZADO PARA QUE O TITULO DO EVENTO QUEBRE NA LINHA*/
span.fc-title {
	padding: 5px !important;
	text-overflow: ellipsis;
	white-space: pre-line;	
}

/* Título Central do Calendário */
.fc-center>h2 {
	font-size: 23px;
}

{{-- CSS Textarea --}}
.text-style{ width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px; } 
}
</style>

@endsection

@section('page_title','Agenda')
@section('page_description','Painel de Controle')


<!-- Register Button -->
@section('top-button')

<div class="breadcrumb">
	<div class="row">
		<div class="col-md-6" style="margin-bottom: 5px;">
			<a class="btn btn-default" href="{{ action('ScheduleController@index') }}" > <i class="fa fa-repeat fa-fw"></i> Atualizar</a>
		</div>
	</div>
</div>
@endsection

<!-- Content -->
@section('content')

<div id="div-main" >
	<div class='row'> 
		<div class="col-xs-12">
			<div class="box box-default">
				<div class="box-body">
					<div id='calendar'></div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>

@endsection

<!-- Footer Extra -->
@section('footer-extras')
<!-- Jquery UI -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js") }}"></script>
<!-- Moment.js -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/daterangepicker/moment.min.js") }}"></script>
<!-- Full Calenddar -->
<script src="{{ asset ("/js/fullcalendar/fullcalendar.min.js") }}"></script>
<script src="{{ asset ("/js/fullcalendar/locale/pt-br.js") }}"></script>

<script>
	$(function () {
		var _token = '{{csrf_token()}}'; 

        var $calendar = $('#calendar', '#div-main');
		
		//Inicializa o Calendario
		$calendar.fullCalendar({
			lazyFetching: false,
			listDayFormat: 'dddd',
			listDayAltFormat: 'D/M/YYYY',
			header: {
	        	left: 'prev,next, today',
	        	center: 'title',
	        	right: 'month,listWeek'
      		},
			timezone: 'local',			
			defaultView: 'month',
			events: function(start, end, timezone, callback) {
				$.ajax({
					url: '{{ route('schedule.get-by-day')}}',
					dataType: 'JSON',
					method: 'GET',
					cache: true,
					beforeSend: function(request) {
						return request.setRequestHeader('X-CSRF-Token',  _token);
					},
					data: {
					   	start: start.format('YYYY-MM-DD'),
					   	end: end.format('YYYY-MM-DD'),
					},
					success: function(doc) {
					   	callback(doc.events);
					}
				});
			},
			eventRender: function(event, element, view ) {
				if(view.name == "listWeek") {
					element.find('.fc-list-item-title').find('a').html(event.title_2);
				}
        	},
			eventClick: function(event, jsEvent, view ) {
				window.open({!! json_encode(url('/')) !!}+'/dashboard/ordem-servico/'+event.id+'/ver','_blank');
			}
		});

	});
</script>

@endsection