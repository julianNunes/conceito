@extends('dashboard.layout.admin_template')


@section('header-extras')
@endsection

@section('page_title','Meus Dados')

@section('page_description','Editar')


@section('content')

<div id="div-main">
	<form method="post" action="{{ route('admin.update-config') }}" enctype="multipart/form-data">   
		<input type="hidden" name="_token" value="{{csrf_token()}}">

		@if(Session::has('flash_message'))
			<div class="alert alert-warning alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
				<h4><i class="icon fa fa-warning"></i>Atenção!</h4>
				<em> {!! session('flash_message') !!}</em>
			</div>
		@endif

		@if(Session::has('flash_message2'))
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<i class="icon fa fa-check"></i>
				<em> {!! session('flash_message2') !!}</em>
			</div>
		@endif
	
		{{-- Dados Iniciais --}}
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Dados Iniciais</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
				</div>
			</div>

			<div class="box-body">
				<div class="row">	
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Nome<span style="color: red;"><sup>&bull;</sup></span></label>
							<input type="text" name="name" placeholder="Nome" class="form-control" value="{{Auth::user()->name}}" required/>
						</div>  
					</div>  

					<div class="col-md-6 password">
						<div class="form-group">
							<label class="control-label">Apelido</label>
							<input type="text" name="nickname" placeholder="Apelido" value="{{Auth::user()->nickname}}" class="form-control"/>
						</div>					   
					</div>	

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Senha</label>
							<input type="text" id="password-required" style="display:none;"> 
							<input id="txtPassword" type="password" name="password" class="form-control" placeholder="Senha" autocomplete="off" />
						</div>          
					</div>				

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Confirmar Senha</label>
							<input type="password" id="txtPasswordConfirmation" class="form-control" name="password_confirmation" placeholder="Confirmar Senha" autocomplete="off" />
						</div>  
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">E-mail<span style="color: red;"><sup>&bull;</sup></span></label>
							<input type="text" name="email" placeholder="Email" class="form-control" value="{{Auth::user()->email}}"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required/>
						</div>  
					</div> 					
				</div>
			</div>
		</div>

		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" onclick="abrir()" class="btn btn-sm btn-success"><i class="fa fa-save fa-fw"></i> Salvar</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('footer-extras')
<!-- Jquery Mask -->
<script src="{{ asset ("/bower_components/jquery-mask-plugin/dist/jquery.mask.js") }}" type="text/javascript"></script>
<!-- Mascara.js -->
<script src="{{ asset("/bower_components/admin-lte/dist/js/mascara.js")}}"></script>
{{-- Select2 --}}
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/select2.full.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/select2/i18n/pt-BR.js") }}" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		
		var error = @if(Session::has('flash_message')) true @else false @endif;

		//Set da linguagem do select2
		$.fn.select2.defaults.set('language', 'pt-BR');

		//Inicia os plugins nos componentes
		$('.select2', '#div-main').select2();
		//Inicializa as mascaras
		$(".hour", '#div-main').mask('00:00', { placeholder: '__:__'});

		//Limpa os campos de senha
		$('#txtPassword').val('');
		$('#txtPasswordConfirmation').val('');

		//Altera os inputs de Horario referente ao dia escolhido
		function changeDay (evt) {
			var result = $(this).val();
			if(result == '1') {
				$(this).closest('.divDay').find('.hour').prop('readonly', false).prop('required', true);  
			} else {
				$(this).closest('.divDay').find('.hour').val('').prop('readonly', true).prop('required', false);  
			}
		}

		//Metodo para validar os inputs com a senha
		function changePassword (evt) {
			var result = $(this).val().length;
			if(result >0) {
				
				$(this).prop('required', true);  				
				$('#txtPasswordConfirmation').prop('required', true);  	
				$('#password-required').prev().html('Senha<span style="color: red;"><sup>&bull;</sup></span>');				
				$('#txtPasswordConfirmation').prev().html('Confirmar Senha<span style="color: red;"><sup>&bull;</sup></span>'); 			
			} else {
				
				$(this).prop('required', false);  				
				$('#txtPasswordConfirmation').prop('required', false);  
				$('#password-required').prev().html('Senha');				
				$('#txtPasswordConfirmation').prev().html('Confirmar Senha');
			}

			var $this = $(this);
			var psw = $this.val();
			var c_psw = $('#txtPasswordConfirmation').val();		

			if(c_psw != psw) {
				$('#txtPasswordConfirmation').get(0).setCustomValidity('Repita a senha corretamente');
			} else {
				$('#txtPasswordConfirmation').get(0).setCustomValidity('');
			}	
		}	

		//Metodo para validar os inputs com a senha
		function checkPassword(){
			var $this = $(this);
			var c_psw = $this.val();
			var psw = $('#txtPassword').val();	

			if(c_psw != psw) {
				$this.get(0).setCustomValidity('Repita a senha corretamente');
			} else {
				$this.get(0).setCustomValidity('');
			}		
		}
		
		//Eventos		
		$(document).delegate('.day','change', changeDay);
		$('#txtPassword').on('input', changePassword);
		$('#txtPasswordConfirmation').on('input', checkPassword);

		//Chamado para ativar os selects dos dias e habilitar os horarios
		//no caso de haver erro e voltar 
		if(error) { $('.day', '#div-main').trigger('change'); }
	}); 
</script>

@endsection