@extends('dashboard.layout.admin_template')

@section('header-extras')

	{{-- TIMEPICKE (WICKEDPICKER) --}}
	<link href="{{ asset ("/bower_components/wickedpicker/stylesheets/wickedpicker.css") }}" rel="stylesheet" type="text/css" />

	<!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{ asset ("/bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.min.css") }}">

@endsection

@section('page_title','Usuário')
@section('page_description', $page_description)

@section('content')

@include('dashboard.layout.success')
@include('dashboard.layout.warning')

<div id="div-main">

@if(!isset($show))
	<form method="post" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
		<input type="hidden" name="_token" value="{{csrf_token()}}">	
@else
	<form method="get" action="{{ route($route['name'], $route['params']) }}" enctype="multipart/form-data"> 
@endif	

		<div class="box box-default">
			{{-- Dados Iniciais --}}
			<div class="box-header with-border">
				<h3 class="box-title">Dados Iniciais</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus bt-plus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">	
					{{-- Nome --}}	
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Nome </label>
							<input type="text" class="form-control" @if(!isset($show)) name="name" @endif @if(isset($user)) value="{{ $user->name }}" @endif @if(isset($show)) readonly @endif/>
						</div>  
					</div>
					{{-- Apelido --}}	
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Apelido </label>
							<input type="text" class="form-control" @if(!isset($show)) name="nickname" @endif @if(isset($user)) value="{{ $user->nickname }}" @endif @if(isset($show)) readonly @endif/>
						</div>  
					</div>
					{{-- Senha --}}	
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Senha </label>
							<input id="txtPassword" type="password"  class="form-control" placeholder="Senha" autocomplete="off" @if(!isset($show)) name="password" @endif  @if(isset($show)) readonly @endif/>
						</div>  
					</div>
					{{-- Confirmar Senha --}}	
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Confirmar Senha </label>
							<input type="password" id="txtPasswordConfirmation" class="form-control" placeholder="Confirmar Senha" autocomplete="off" @if(!isset($show)) name="password_confirmation" @endif  @if(isset($show)) readonly @endif/>
						</div>  
					</div>
					{{-- E-mail --}}	
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label">E-mail </label>
							<input type="text" class="form-control" @if(!isset($show)) name="email" @endif @if(isset($user)) value="{{ $user->email }}" @endif @if(isset($show)) readonly @endif/>
						</div>  
					</div>
					{{--  Administrador  --}}
					<div class="col-md-6">					
						<div class="form-group">
							<label class="control-label">Administrador @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
							@if(isset($show))
								<input type="text" class="form-control" value="{{ $provider->formatted_administrator }}" readonly />
							@else
								<select @if(!isset($show)) name="administrator" @endif class="form-control" style="width: 100%"  @if(isset($show)) readonly @else required @endif>
									<option value="">Selecione</option>
									<option value="0" @if((isset($user) && $user->administrator == '0')   || !isset($user)) selected @endif>Não</option>
									<option value="1" @if(isset($user) && $user->administrator == '1') selected @endif>Sim</option>		
								</select>
							@endif
						</div> 
					</div> 
					{{--  Ativo  --}}
					<div class="col-md-6">					
						<div class="form-group">
							<label class="control-label">Ativo @if(!isset($show)) <span style="color: red;"><sup>&bull;</sup> @endif </label>
							@if(isset($show))
								<input type="text" class="form-control" value="{{ $user->formatted_active }}" readonly />
							@else
								<select @if(!isset($show)) name="active" @endif class="form-control" style="width: 100%"  @if(isset($show)) readonly @else required @endif>
									<option value="">Selecione</option>
									<option value="0" @if(isset($user) && $user->active == '0') selected @endif>Não</option>
									<option value="1" @if((isset($user) && $user->active == '1')  || !isset($user)) selected @endif>Sim</option>		
								</select>
							@endif
						</div> 
					</div> 
				</div>
			</div>
		</div>

		{{-- Botão --}}
		<div class="box box-default">
			<div class="box-footer">      
				<button type="submit" onclick="abrir()" class="btn btn-sm btn-success"><i class="fa fa-save fa-fw"></i> Salvar</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('footer-extras')

<!-- Jquery Mask -->
<script src="{{ asset ("/bower_components/jquery-mask-plugin/dist/jquery.mask.js") }}" type="text/javascript"></script>
<!-- Mascara.js -->
<script src="{{ asset("/bower_components/admin-lte/dist/js/mascara.js")}}"></script>
<!-- FastClick -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>
<!-- DatePicker-->
<script src="{{ asset ("/js/bootstrap-datepicker.min.js") }}"></script>
<script src="{{ asset ("/js/bootstrap-datepicker.pt-BR.js") }}"></script>

{{-- Moment. js --}}
<script src="{{ asset ("/bower_components/admin-lte/dist/js/moment.js") }}"></script>
<!-- InputMask -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset ("/bower_components/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js") }}"></script>
<!-- bootstrap time picker -->
<script src="{{ asset ("/bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.min.js") }}"></script>

<script type="text/javascript">
	var error = @if(Session::has('flash_message')) true @else false @endif;

	//Timepicker
	$(".timepicker").timepicker({
		showInputs: false,
		showMeridian: false
	});

   // $('.hour').val('');
		
</script>

<script>
	$(document).ready(function() {

		$('.select-day').each(function(){
			if($(this).val()  == 0){
	     	   $(this).closest('.divDay').find('.hour').val('')
	    	}
	    });
		
		//Inicializa FastClick
		FastClick.attach(document.body);	

		//Limpa os campos de senha
		$('#txtPassword').val('');
		$('#txtPasswordConfirmation').val('');

		function changeDay (evt) {
			var result = $(this).val();
			if(result == '1') {
				$(this).closest('.divDay').find('.hour').prop('disabled', false).prop('required', true);  
			} else {
				$(this).closest('.divDay').find('.hour').val('').prop('disabled', true).prop('required', false);  
			}
		}

		function changePassword (evt) {
			var result = $(this).val().length;
			if(result >0) {
				
				$(this).prop('required', true);  				
				$('#txtPasswordConfirmation').prop('required', true);  	
				$('#password-required').prev().html('Senha<span style="color: red;"><sup>&bull;</sup></span>');				
				$('#txtPasswordConfirmation').prev().html('Confirmar Senha<span style="color: red;"><sup>&bull;</sup></span>'); 			
			} else {
				
				$(this).prop('required', false);  				
				$('#txtPasswordConfirmation').prop('required', false);  
				$('#password-required').prev().html('Senha');				
				$('#txtPasswordConfirmation').prev().html('Confirmar Senha');
			}

			var $this = $(this);
			var psw = $this.val();
			var c_psw = $('#txtPasswordConfirmation').val();		

			if(c_psw != psw) {
				$('#txtPasswordConfirmation').get(0).setCustomValidity('Repita a senha corretamente');
			} else {
				$('#txtPasswordConfirmation').get(0).setCustomValidity('');
			}	
		}		

		function checkPassword(){
			var $this = $(this);
			var c_psw = $this.val();
			var psw = $('#txtPassword').val();	

			if(c_psw != psw) {
				$this.get(0).setCustomValidity('Repita a senha corretamente');
			} else {
				$this.get(0).setCustomValidity('');
			}		
		}
		
		//Eventos		
		$(document).delegate('.select-day','change', changeDay);
		$('#txtPassword').on('input', changePassword);
		$('#txtPasswordConfirmation').on('input', checkPassword);
		//Chamado para ativar os selects dos dias e habilitar os horarios
		//no caso de haver erro e voltar 
		if(error) { $('.day', '#div-main').trigger('change'); }
	}); 
</script>

@endsection