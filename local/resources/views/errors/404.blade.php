<!DOCTYPE html>
<html>
    <head>
        <title>Página não encontrada</title>

       
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
     <link href="{{ asset("/bower_components/admin-lte/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    
  <link href="{{ asset("/bower_components/admin-lte/plugins/select2/select2.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="{{ asset("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />

    </head>
    <body>
       <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow" style="margin-top: -14px;"> 404</h2>

        <div class="error-content" style="margin-top: 200px;">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Página não encontrada.</h3>

          <p>
            Não foi possível encontrar a página que você estava procurando. <br/>
            Porém, você pode <a href="{{url('/')}}">retornar a página inicial</a>.
          </p>

         
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
       </body>
</html>
